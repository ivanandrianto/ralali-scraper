var express = require('express');
var router = express.Router();

var AlibabaProductQuery = require('../../modules/scraper/website/alibaba/queries/alibabaProduct');

/* GET users listing. */
router.post('/', function (req, res) {
  var options = {};

  var setProperty = function (property, value) {
    if (value) {
      options[property] = value;
    }
  };

  // Set options property
  setProperty('title', new RegExp(req.body.title, 'g'));
  setProperty('category', new RegExp(req.body.category, 'g'));
  setProperty('subCategory', new RegExp(req.body.subCategory, 'g'));
  setProperty('seller', new RegExp(req.body.seller, 'g'));

  var minOrderQuantity = req.body.minOrderQuantity;
  if (minOrderQuantity) {
    setProperty('minOrderQuantity', { $gte: minOrderQuantity });
  }

  var maxPriceGreaterThan = req.body.maxPriceGreaterThan;
  if (maxPriceGreaterThan) {
    setProperty('maxPrice', { $gte: maxPriceGreaterThan });
  }

  var maxPriceLessThan = req.body.maxPriceLessThan;
  if (maxPriceLessThan) {
    setProperty('maxPrice', { $lte: maxPriceLessThan });
  }

  console.log(options);

  return AlibabaProductQuery.getProduct(options)
    .then(function (result) {
      res.contentType('application/json');
      res.send(result);
    })
    .catch(function (err) {
      if (err) {
        console.log(err);
      }
    });
});

module.exports = router;

var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var CompanyQuery = require('../../modules/scraper/queries/company');

/* GET users listing. */
router.get('/', function (req, res) {
  res.send('company list');
});

router.get('/all', function (req, res) {
  return CompanyQuery.findCompaniesAll()
    .then(function (result) {
      console.log(result);
      res.contentType('application/json');
      res.send(result);
    })
    .catch(function (err) {
      if (err) {
        console.log(err);
      }
    });
});

router.get('/id/:id', function (req, res) {
  return CompanyQuery.findCompanyById(req.params.id)
    .then(function (result) {
      console.log(result);
      res.contentType('application/json');
      res.send(result);
    })
    .catch(function (err) {
      if (err) {
        console.log(err);
      }
    });
});

router.get('/web/:sourceWebsite', function (req, res) {
  return CompanyQuery.findCompaniesBySourceWebsite(req.params.sourceWebsite)
   .then(function (result) {
      console.log(result);
      res.contentType('application/json');
      res.send(result);
    })
   .catch(function (err) {
      if (err) {
        console.log(err);
      }
    });
});

router.get('/url/:sourceUrl', function (req, res) {
  return CompanyQuery.findCompaniesBySourceUrl(req.params.sourceUrl)
    .then(function (result) {
      console.log(result);
      res.contentType('application/json');
      res.send(result);
    })
    .catch(function (err) {
      if (err) {
        console.log(err);
      }
    });
});

router.get('/address-like/:addressLike', function (req, res) {
  return CompanyQuery.findCompaniesWithAddressLike(req.params.addressLike)
    .then(function (result) {
      console.log(result);
      res.contentType('application/json');
      res.send(result);
    })
    .catch(function (err) {
      console.log(err);
    });
});

router.get('/verified/:status', function (req, res) {
  return CompanyQuery.findCompaniesByVerifiedStatus(req.params.status)
    .then(function (result) {
      console.log(result);
      res.contentType('application/json');
      res.send(result);
    })
    .catch(function (err) {
      console.log(err);
    });
});

router.get('/csv', function (req, res) {
  return CompanyQuery.getCsvFilePath()
    .then(function (result) {
      console.log('Got result from database');
      res.contentType('text/csv');
      res.setHeader('Content-disposition', 'attachment; filename=companies.csv');
      res.send(result);
    })
    .catch(function (err) {
      console.log(err);
    });
});

router.get('/run-converter', function (req, res) {
  return CompanyQuery.runConverter()
    .then(function (result) {
      console.log('Converter run');
      res.contentType('application/json');
      res.send(result);
    })
    .catch(function (err) {
      console.log(err);
    });
});

router.post('/insert', function (req, res, next) {
  return CompanyQuery.createCompany(req.body)
    .then(function (result) {
      console.log(result);
      res.contentType('application/json');
      res.send(result);
    })
    .catch(function (err) {
      console.log(err);
    });
});

module.exports = router;

/**
 * This script is used to test convert currency with google finance
 * To run:
 * node tests/currencyUSDtoIDR.js
 * @author: gazandi
 */

var cc = require('../../modules/rbiz/helpers/currencyConverter');

var Promise = require('bluebird');
var chai = require('chai');
var expect = chai.expect;

describe('test getCurrency USD to IDR', function () {
  context('given a USD', function () {
    var cur = 0;
    it('should return less than 15000 and greater than 13000 ', function (done) {
      return new Promise(function (resolve) {
        var cur = cc.getCurrency('IDR');
        console.log(cur);
        expect(cur).to.be.within(13000, 15000);
        resolve();
      }).then(done);
    });
  });
});

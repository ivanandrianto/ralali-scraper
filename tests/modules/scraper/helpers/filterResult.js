var dbHelper = require('../../../../helpers/common/database');
dbHelper.connect(dbHelper.databases.test);
var chai = require('chai');
var expect = chai.expect;
var filterResult = require('../../../../modules/scraper/helpers/filterResult');

describe('test filter company result', function () {
  context('given a list of companies', function () {
    var companies = [];

    before(function () {
      var company = { name: 'company1', category: 'categoryA', sourceUrl: 'http://company.website.com' };
      var company2 = { name: 'company2', category: 'categoryB', sourceUrl: 'http://company.website.com' };
      var company3 = { name: 'company3', category: 'categoryC', sourceUrl: 'http://company3.website.com' };
      companies.push(company);
      companies.push(company2);
      companies.push(company3);

      return filterResult.filterCompanies(companies)
        .then(function (res) {
          console.log(res);
          companies = res;
        });
    });

    it('should return array of companies with length of 2', function () {
      console.log('b');
      return expect(companies.length).to.equals(2);
    });
  });

});

describe('test filter product result', function () {
  context('given a list of products', function () {
    var products = [];

    before(function () {
      var product = { name: 'product1', category: 'categoryA', sourceUrl: 'http://product.website.com' };
      var product2 = { name: 'product2', category: 'categoryB', sourceUrl: 'http://product.website.com' };
      var product3 = { name: 'product3', category: 'categoryC', sourceUrl: 'http://product3.website.com' };
      products.push(product);
      products.push(product2);
      products.push(product3);
      return filterResult.filterProducts(products)
        .then(function (res) {
          console.log(res);
          products = res;
        });
    });

    it('should return array of products with length of 2', function () {
      return expect(products.length).to.equals(2);
    });
  });

});

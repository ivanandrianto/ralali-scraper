'use strict';

var dbHelper = require('../../../../../helpers/common/database');
dbHelper.connect(dbHelper.databases.test);
var chai = require('chai');
var expect = chai.expect;
var companyScraper = require('../../../../../modules/scraper/website/indonetwork/company');
var companyQuery = require('../../../../../modules/scraper/queries/company');

describe('test scraping a company page', function () {
  this.timeout(60000);

  context('given a valid company url page', function () {

    before(function () {
      return companyQuery.removeCompany({})
        .then(function () {
          var company = {
            name: 'company1',
            category: 'categoryA',
            subCategory: 'subCategoryA',
            sourceUrl: 'http://cv-safacitramandiri.indonetwork.co.id',
            sourceWebsite: 'indonetwork'
          };

          return companyScraper.getCompanyDetails(company);
        });
    });

    after(function () {
      return companyQuery.removeCompany({});
    });

    it('should store the company details correctly in database', function () {
      return companyQuery.findCompaniesAll()
        .then(function (companies) {
          var company = companies[0].toJSON();

          expect(companies.length).to.equal(1);
          expect(company.name).to.equal('company1');
          expect(company.description).to.equal('cv.safacitramandiri\n\n                    ' +
            'Kami adalah Produsen alat peraga pendidikan terlengkap mulai dari TK-SD-SMP-SMA-SLB. ' +
            'Kami siap mensuply alat peraga untuk proyek( DAK dan proyek lainnya ) ke seluruh wilayah di Indonesia. ' +
            'Harga Miring, Mutu Bersaing. Selain Itu Kami Juga menerima DESAIN KHUSUS untuk produk Glass Ware Dan ' +
            'Plastic Ware Dalam menjalankan usahanya, perusahaan kami selalu mengedepankan mutu, kualitas serta ' +
            'metode kerja yang professional demi kepuasan pelanggan. Kami didukung oleh tenaga-tenaga kerja yang ahli ' +
            'dibidangnya masing-masing. CV. SAFA CITRA MANDIRI, KAMI MEMPRODUKSI, MEMBUAT , MEREPARASI, MEMODIFIKASI, ' +
            'MENJUAL ALAT LABORATORIUM GLASSWARE, ALAT laboratorium, PERLENGKAPAN LAB, PERALATAN LAB, ALAT-ALAT LAB, ' +
            'DISTIBUTOR ALAT LABORATORIUM, JUAL ALAT LABORATORIUM MURAH, ALAT UJI KUALITAS AIR, ALAT UJI BIOTEKNOLOGY , ' +
            'ALAT LAB KESEHATAN, ALAT LABOR, ALAT LAB, ALAT KESEHATAN Dan ALAT PERAGA MULAI DARI SMP, SMA, dan ' +
            'IPBA MURAH. Peralatan Laboratorium Glassware, Porselen ware, Plastick ware, Tool dan Eqipment. ' +
            'Kami merupakan perusahaan home industri alat laboratorium ( glass ware) , alat alat gelas kimia, ' +
            'alat laboratorium kimia, gelas laboratorium, alat lab, peralatan laboratorium, gelas kimia, ' +
            'alat destilasi, alat peraga, kit, Yang memproduksi/ membuat sendiri. Produk kami didukung oleh ' +
            'tenaga-tenaga ahli yang berpengalaman dan juga menggunakan mesin penunjang serta alat penunjang ' +
            'lainnya.Bahan yang kami gunakan adalah dari bahan pyrek, duran, cina dan lokal. Dalam menjalankan ' +
            'usahanya, perusahaan kami selalu mengedepankan mutu, kualitas serta metode kerja yang professional ' +
            'demi kepuasan pelanggan. produk kami dibuat dengan kalibarasi , sehingga kualitas kami diutamakan. ' +
            'harga kamipun terjangkau, murah dan bersaing. ( silahkan buktikan) .kami juga menjual produk jadi ' +
            'merk pyrek, duran , dan RRC. CV. SAFA CITRA MANDIRI Kami menyediakan berbagai alat-alat laboratorium, ' +
            'alat distilasi. CV. SAFA CITRA MANDIRI Beralamat komp. Griya Winaya Blok. B2 No. 22 Ujung Berung, ' +
            'Bandung Tlp. no( 081320992894/ 022-87885505) PIN : 28844142 / 24F3350C Email. bagus_ 3tomo@ yahoo.co.id ' +
            '/ safacitra_ mandiri@ yahoo.co.i'
          );
          expect(company.sourceUrl).to.equal('http://cv-safacitramandiri.indonetwork.co.id');
          expect(company.sourceWebsite).to.equal('indonetwork');
          expect(company.verified).to.equal(false);
          expect(company.categories.length).equals(1);
          expect(company.categories[0]).equals('categoryA');
          expect(company.subCategories.length).equals(1);
          expect(company.subCategories[0]).equals('subCategoryA');

        });
    });

  });

  context('given a valid company url page', function () {

    before(function () {
      return companyQuery.removeCompany({})
        .then(function () {
          var company = {
            name: 'company1',
            category: 'categoryA',
            subCategory: 'subCategoryA',
            sourceUrl: 'http://zzz-cv-safacitramandiri.indonetwork.co.id',
            sourceWebsite: 'indonetwork'
          };

          return companyScraper.getCompanyDetails(company);
        });
    });

    after(function () {
      return companyQuery.removeCompany({});
    });

    it('should store the company details correctly in database', function () {
      return companyQuery.findCompaniesAll()
        .then(function (companies) {
          var company = companies[0].toJSON();

          expect(companies.length).to.equal(1);
          expect(company.name).to.equal('company1');
          expect(company.description).to.equal(undefined);
          expect(company.sourceUrl).to.equal('http://zzz-cv-safacitramandiri.indonetwork.co.id');
          expect(company.sourceWebsite).to.equal('indonetwork');
          expect(company.verified).to.equal(false);
          expect(company.categories.length).equals(1);
          expect(company.categories[0]).equals('categoryA');
          expect(company.subCategories.length).equals(1);
          expect(company.subCategories[0]).equals('subCategoryA');

        });
    });

  });

});

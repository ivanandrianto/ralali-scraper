'use strict';

var dbHelper = require('../../../../../helpers/common/database');
dbHelper.connect(dbHelper.databases.test);
var chai = require('chai');
var expect = chai.expect;
var companyScraper = require('../../../../../modules/scraper/website/indotrading/company');
var companyQuery = require('../../../../../modules/scraper/queries/company');

describe('test scraping a company page', function () {
  this.timeout(60000);

  context('given a valid company url page', function () {

    before(function () {
      return companyQuery.removeCompany({})
        .then(function () {
          var company = {
            name: 'company1',
            category: 'categoryA',
            subCategory: 'subCategoryA',
            sourceUrl: 'https://www.indotrading.com/company/pt-karya-baru-indonesia.aspx',
            sourceWebsite: 'indotrading'
          };

          return companyScraper.getCompanyDetails(company);
        });
    });

    after(function () {
      return companyQuery.removeCompany({});
    });

    it('should store the company details correctly in database', function () {
      return companyQuery.findCompaniesAll()
        .then(function (companies) {
          var company = companies[0].toJSON();

          expect(companies.length).to.equal(1);
          expect(company.name).to.equal('company1');
          expect(company.description).to.equal('PT Karya Baru Indonesia\nPT. Karya Baru IndonesiaAgen, ' +
            'Distributor, Dan Supplier Untuk Berbagai Pangan Hasil Bumi :Agen beras,Beras Merah,' +
            'Ketan Putih,Ketan Hitam,Kacang Hijau,Kacang Tanah, Wijen, Kacang Hijau Kupas, ' +
            'Kacang Tanah Kupas, Dsb. Berlokasi Strategis Di Pusat Perdagangan Palawija Jakarta.' +
            'Kami Berkomitmen Untuk Memberikan Hanya Produk Segar Pilihan Terbaik Yang Dihasilkan Oleh ' +
            'Alam Untuk Anda.Hubungi Kami : PT. Karya Baru IndonesiaPhone : 021 - 4718167 , ' +
            '4718183Email : Info@Karyabaruindonesia.ComWeb : Http://Www.Karyabaruindonesia.Com'
          );
          expect(company.sourceUrl).to.equal('https://www.indotrading.com/company/pt-karya-baru-indonesia.aspx');
          expect(company.sourceWebsite).to.equal('indotrading');
          expect(company.verified).to.equal(false);
          expect(company.categories.length).equals(1);
          expect(company.categories[0]).equals('categoryA');
          expect(company.subCategories.length).equals(1);
          expect(company.subCategories[0]).equals('subCategoryA');
          expect(company.phones.length).equals(4);
          expect(company.phones[0]).equals('(+62) 214718167');
          expect(company.phones[1]).equals('(+62) 214718183');
          expect(company.phones[2]).equals('(+62) 4718167');
          expect(company.phones[3]).equals('(+62) 214718183');

        });
    });

  });

  context('given an invalid company url page', function () {

    before(function () {
      return companyQuery.removeCompany({})
        .then(function () {
          var company = {
            name: 'company1',
            category: 'categoryA',
            subCategory: 'subCategoryA',
            sourceUrl: 'https://www.indotrading.com/company/zzz-pt-karya-baru-indonesia.aspx',
            sourceWebsite: 'indotrading'
          };

          return companyScraper.getCompanyDetails(company);
        });

    });

    after(function () {
      return companyQuery.removeCompany({});
    });

    it('should ............. ', function () {
      return companyQuery.findCompaniesAll()
        .then(function (companies) {
          var company = companies[0].toJSON();

          expect(companies.length).to.equal(1);
          expect(company.name).to.equal('company1');
          expect(company.description).to.equal(undefined);
          expect(company.sourceUrl).to.equal('https://www.indotrading.com/company/zzz-pt-karya-baru-indonesia.aspx');
          expect(company.sourceWebsite).to.equal('indotrading');
          expect(company.verified).to.equal(false);
          expect(company.categories.length).equals(1);
          expect(company.categories[0]).equals('categoryA');
          expect(company.subCategories.length).equals(1);
          expect(company.subCategories[0]).equals('subCategoryA');
          expect(company.phones.length).equals(0);

        });
    });

  });

});

'use strict';

var dbHelper = require('../../../../helpers/common/database');
dbHelper.connect(dbHelper.databases.test);
var chai = require('chai');
var expect = chai.expect;
var getDetailsScript = require('../../../../modules/rbiz/resellerAlibaba/getDetails');
var RbizProduct = require('../../../../modules/rbiz/models/rbizProduct');
var rbizProductQuery = require('../../../../modules/rbiz/queries/rbizProduct');

describe('test get details of a product', function () {
  this.timeout(60000);

  context('given a valid product url page without wholesale price', function () {
    before(function () {
      console.log('aaa');
      return rbizProductQuery.removeProduct({})
        .then(function () {
          var product = {
            Url: 'http://reseller.alibaba.com/product-detail/Fantastic-Mini-Windmill-Shape-Electric-Mini_60029907126.html',
            'Product Name': 'Fantastic Mini Windmill Shape Electric Mini Battery Fan USB Fan DIY Gift Series Electrical Gadget',
            type: 'reseller_alibaba',
            sellerId: '10068'
          };
          return rbizProductQuery.saveProduct(new RbizProduct(product))
            .then(function (id) {
              return rbizProductQuery.getOneProduct({ _id: id });
            })
            .then(function (product) {
              return getDetailsScript.getDetails(product);
            });

        });

    });

    after(function () {
      return rbizProductQuery.removeProduct({});
    });

    it('should store the company details correctly in database', function () {
      return rbizProductQuery.getProducts()
        .then(function (products) {
          var product = products[0].toJSON();
          console.log(products.length);
          expect(products.length).to.equal(1);
          expect(product.isMultiVariant).equal(false);
          expect(product.detailAdded).equal(true);
          expect(product.Category).equal('Home Appliances');
          expect(product['Sub Category']).equal('Air Conditioning Appliances');
          expect(product['Sub Sub Category']).equal('Fans');
          expect(product['Minimum Order']).equal('240');
          expect(product.Model).equal('22932');
          expect(product['Retail Normal Price']).equal('11.0');
          expect(product['Shipping Height']).equal('84');
          expect(product['Shipping Length']).equal('69');
          expect(product['Shipping Weight']).equal('23.0 KG');
          expect(product['Shipping Width']).equal('49');
          expect(product['Short Description']).equal('Power Source: Battery\nType: Air Cooling Fan' +
            '\nInstallation: Table\nMaterial: Plastic\nCertification: CB, CE, EMC\nPower (W): 0.75' +
            '\nVoltage (V): 3\nModel Number: 22932\nPlace of Origin: Guangdong China (Mainland)' +
            '\nModel: 22932\nClassification: Portable mini fan\nColor: Red, White, Black' +
            '\nPower Source: Battery\nInstallation: Table\nMaterial: ABS and components' +
            '\nPrint: Piano vanishing surface treatment\nCategory: USB port fan' +
            '\nPacking style: Trapezoidal box\nWeight: 256g\n');
          expect(product['Wholesale Price 1']).equal('4.30');
          expect(product['WP1 Start Quantity']).equal('240');
          expect(product['WP1 End Quantity']).to.not.exist;
          expect(product['Wholesale Price 2']).to.not.exist;
          expect(product['WP2 Start Quantity']).to.not.exist;
          expect(product['WP2 End Quantity']).to.not.exist;
          expect(product['Wholesale Price 3']).to.not.exist;
          expect(product['WP3 Start Quantity']).to.not.exist;
          expect(product['WP3 End Quantity']).to.not.exist;
          expect(product['Wholesale Price 4']).to.not.exist;
          expect(product['WP4 Start Quantity']).to.not.exist;
          expect(product['WP4 End Quantity']).to.not.exist;
        });
    });
  });

  context('given a valid product url page with wholesale price', function () {
    before(function () {
      console.log('aaa');
      return rbizProductQuery.removeProduct({})
        .then(function () {
          var product = {
            Url: 'https://wholesaler.alibaba.com/product-detail/Home-Security-Alarm-Puch-p2p-cam_60379589127.html?spm=a2700.7724838.0.0.nFyFrS',
            'Product Name': 'Home Security Alarm Puch p2p cam viewer',
            type: 'reseller_alibaba',
            sellerId: '10068'
          };
          return rbizProductQuery.saveProduct(new RbizProduct(product))
            .then(function (id) {
              return rbizProductQuery.getOneProduct({ _id: id });
            })
            .then(function (product) {
              return getDetailsScript.getDetails(product);
            });

        });

    });

    after(function () {
      return rbizProductQuery.removeProduct({});
    });

    it('should store the company details correctly in database', function () {
      return rbizProductQuery.getProducts()
        .then(function (products) {
          var product = products[0].toJSON();
          console.log(products.length);
          expect(products.length).to.equal(1);
          expect(product.isMultiVariant).equal(false);
          expect(product.detailAdded).equal(true);
          expect(product.Brand).equal('Daytech');
          expect(product.Category).equal('Security & Protection');
          expect(product['Sub Category']).equal('CCTV Products');
          expect(product['Sub Sub Category']).equal('CCTV Camera');
          expect(product['Minimum Order']).equal('5');
          expect(product.Model).equal('DT-C8815');
          expect(product['Retail Normal Price']).to.not.exist;
          expect(product['Shipping Height']).equal('10');
          expect(product['Shipping Length']).equal('21');
          expect(product['Shipping Weight']).equal('0.9 KG');
          expect(product['Shipping Width']).equal('16');
          expect(product['Short Description']).equal('Place of Origin: Fujian China (Mainland)' +
            '\nBrand Name: Daytech\nModel Number: DT-C8815\nType: IP Camera\nStyle: Dome Camera' +
            '\nSensor: CMOS\nTechnology: Pan / Tilt / Zoom\nSpecial Features: Vandal-proof\nPower: DC5V 2A' +
            '\nSensor: CMOS\nLens: f: 3.6mm, F: 2.0\nVideo Compressing Format: H.264' +
            '\nPTZ Rotation: Horizontal: 360 ° & Vertical: 110 °\nEffective Pixel: 1280(H)* 720(V)(1 million pixels)' +
            '\nNight Vision: 12 pcs Infrared light ,IR Distance : 10m\nVideo Storage: 128G\nCamera Style: Dome' +
            '\nCamera type: IP camera\n');
          expect(product['Wholesale Price 1']).equal('27.42');
          expect(product['WP1 Start Quantity']).equal('5');
          expect(product['WP1 End Quantity']).equal('10');
          expect(product['Wholesale Price 2']).to.equal('26.61');
          expect(product['WP2 Start Quantity']).to.equal('11');
          expect(product['WP2 End Quantity']).to.equal('49');
          expect(product['Wholesale Price 3']).to.equal('25.60');
          expect(product['WP3 Start Quantity']).to.equal('50');
          expect(product['WP3 End Quantity']).to.not.exist;
          expect(product['Wholesale Price 4']).to.not.exist;
          expect(product['WP4 Start Quantity']).to.not.exist;
          expect(product['WP4 End Quantity']).to.not.exist;
        });
    });
  });

});

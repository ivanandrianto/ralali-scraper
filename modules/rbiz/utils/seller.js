'use strict';

var sellers = [
  { name: 'Sports Port',
    id: 11188,
    email: 'sports.port@raksasa.biz' },
  { name: 'OTORALI CARE',
    id: 11094,
    email: 'otorali.care@raksasa.biz' },
  { name: 'Beauty Shop',
    id: 11048,
    email: 'susilawati@raksasa.biz' },
  { name: 'CELLALI TECH',
    id: 10861,
    email: 'purchasing@raksasa.biz' },
  { name: 'Stationery Station',
    id: 10860,
    email: 'harnum.muliawaty@raksasa.biz' },
  { name: 'shopping garden',
    id: 10858,
    email: 'cs05@raksasa.biz' },
  { name: 'Raksasa Elektronik',
    id: 10857,
    email: 'taufik.ismail@raksasa.biz' },
  { name: 'berlian improvement',
    id: 10856,
    email: 'cs04@raksasa.biz' },
  { name: 'Tour Toys',
    id: 10855,
    email: 'cs09@raksasa.biz' },
  { name: 'Raksasa Bisnis Indonesia',
    id: 10089,
    email: 'ozy@raksasa.biz' }
];

exports.sellers = sellers;

/**
 * This script is used to get convert product url to image url
 * To run:
 * node modules/rbiz/utils/convertToImageUrl.js
 * @author: ivan
 * @author: gazandi
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var csv = require('csv-parser');
var fs = require('fs');
var json2csv = require('json2csv');
var Promise = require('bluebird');
var prompt = require('prompt');
var url = require('url');
var validUrl = require('valid-url');

var downloader = require('../../../helpers/file/downloader');
var request = require('../../../helpers/common/request');
var s3Helper = require('../../../helpers/common/s3');
var s3Client = s3Helper.getS3Client();
var seller = require('./seller');
var sellerHelper = require('./../helpers/seller');

var imageDirectory = '~/images/aliexpress/products/';
var s3Directory = 'rbiz/';

const util = require('util');
const REQUEST_TIMEOUT = 60000;
const MAX_ATTEMPT = 5;

var fields = [
  'No.',
  'Brand*',
  'Product Name*',
  'Model*',
  'Reference SKU',
  'Category*',
  'Sub Category*',
  'Sub Sub Category*',
  'Sub Sub Sub Category*',
  'Short Description*',
  'Long Description*',
  'Height*',
  'Width*',
  'Length*',
  'Weight*',
  'Shipping Height*',
  'Shipping Width*',
  'Shipping Length*',
  'Shipping Weight*',
  'Stock*',
  'Minimum Order*',
  'Lead Time ID*',
  'Images*',
  'Url*',
  'Manual Books',
  'Retail Normal Price*',
  'Retail Discounted Price',
  'Disc. Start Date',
  'Disc. End Date',
  'Wholesale Price 1',
  'WP1 Start Quantity',
  'WP1 End Quantity',
  'Wholesale Price 2',
  'WP2 Start Quantity',
  'WP2 End Quantity',
  'Wholesale Price 3',
  'WP3 Start Quantity',
  'WP3 End Quantity',
  'Wholesale Price 4',
  'WP4 Start Quantity',
  'WP4 End Quantity'
];

prompt.start();
prompt.get(['csvPath', 'sellerName'], function (err, input) {

  var sellerId = sellerHelper.getSellerId(input.sellerName);
  if (sellerId === -1) {
    console.log('Seller name not valid');
    return;
  }

  var inputFile = _.trim(input.csvPath);
  return read(inputFile)
    .then(function (products) {
      console.log(products.length);
      return Promise.each(products, function (product) {
        return getImageUrls(product, sellerId);
      })
        .then(function (res) {
          var res = _.flatten(res);
          var csv = json2csv({ data: res, fields: fields }, function (err, csv) {
            if (err) throw err;
            var output = input.sellerName + '_' + Date.now() + '.csv';
            fs.writeFile(output, csv, function (err) {
              if (err) throw err;
              console.log('file saved');
            });
          });
          return res;
        });
    })
    .catch(function (err) {
      console.log(err);
    });
});

function read(csvPath) {
  var products = [];

  return new Promise(function (resolve) {
      fs.createReadStream(csvPath)
      .pipe(csv())
      .on('data', function (data) {
        products.push(data);
      })
      .on('end', function () {
        resolve(products);
      });
    });
}

function getImageUrls(product, sellerId) {
  var productUrl = product['Images*'];
  if (!validUrl.isUri(productUrl)) return product;
  var parsedUrl = url.parse(productUrl);
  product['Images*'] = undefined;
  product['Url*'] = productUrl;

  if (parsedUrl.host && parsedUrl.protocol && parsedUrl.path) {

    var requestOptions = {
      url: productUrl,
      method: 'GET',
      timeout: REQUEST_TIMEOUT
    };

    return request.makeRequest(requestOptions, MAX_ATTEMPT)
      .then(function (html) {
        var body = html.body;
        if (body) {
          var host = parsedUrl.host;
          switch (host) {
            case 'www.gearbest.com':
              console.log('Gearbest');
              product['Images*'] = getImageFromGearbest(body);
              break;
            case 'www.alibaba.com':
              console.log('Alibaba');
              product['Images*'] = getImageFromAlibaba(body);
              break;
            case 'www.aliexpress.com':
              console.log('Aliexpress');
              product['Images*'] = getImageFromAliexpress(body);
              break;
          }
          return uploadImages(product, sellerId);
        }
        return product;
      })
      .catch(function (err) {
        console.log(err);
        return product;
      });
  } else {
    return Promise.resolve(product);
  }
}

function getImageFromGearbest(body) {
  var $ = cheerio.load(body);
  var imageUrls = [];
  $('.n_thumbImg_item img').each(function () {
    var url = $(this).attr('data-big-img');
    imageUrls.push(url);
  });
  imageUrls = imageUrls.toString();
  return imageUrls;
}

function getImageFromAlibaba(body) {
  var $ = cheerio.load(body);
  var imageUrls = [];
  $('.thumb img').each(function () {
    var url = $(this).attr('src').replace('.jpg_50x50', '');
    imageUrls.push(url);
  });

  if (imageUrls.length === 0) {
    $('#J-image-icontent img').each(function () {
      var url = $(this).attr('src').replace('.jpg_350x350', '');
      imageUrls.push(url);
    });
  }

  imageUrls = imageUrls.toString();
  return imageUrls;
}

function getImageFromAliexpress(body) {
  var $ = cheerio.load(body);
  var imageUrls = [];
  $('.image-thumb-list img').each(function () {
    var url = $(this).attr('src').replace('.jpg_50x50', '');
    imageUrls.push(url);
  });
  imageUrls = imageUrls.toString();
  return imageUrls;
}

function uploadImages(product, sellerId) {

  var imageUrls = product['Images*'].split(',');

  return Promise.mapSeries(imageUrls, function (imageUrl) {
    return downloadAndUploadToS3(product['Brand*'], product['Product Name*'], sellerId, imageUrl);
  })
    .then(function (res) {
      var res = _.flatten(res);
      product['Images*'] = res;
      return product;
    });
}

function downloadAndUploadToS3(brand, productName, sellerId, imageUrl) {

  var parsedUrl = url.parse(imageUrl);
  if (parsedUrl.host && parsedUrl.protocol && parsedUrl.path) {

    // Get file format
    var lastIndexOfDot = imageUrl.lastIndexOf('.');
    var fileFormat = imageUrl.substr(lastIndexOfDot);

    // Set filename
    brand = brand.toLowerCase().replace(new RegExp('[ /]', 'g'), '_').replace(new RegExp('_$'), '');
    productName = productName.toLowerCase().replace(new RegExp('[ /]', 'g'), '_').replace(new RegExp('_$'), '');
    var fileName = sellerId + '_' + brand + '_' + productName.substr(0, 30);
    fileName = fileName;
    return getFileName(fileName, fileFormat)
      .then(function (fileName) {
        var localFileLocation = imageDirectory.replace('~', process.env.HOME) + fileName;
        return downloader.download(imageUrl, localFileLocation)
          .then(function (res) {
            if (res === true) {
              var s3FileLocation = s3Directory + fileName;

              return new Promise(function (resolve, reject) {
                s3Client.putFile(localFileLocation, s3FileLocation, function (err, res) {
                  if (res) {
                    console.log('success: ' + fileName);
                    resolve(fileName);
                  }
                  if (err) {
                    console.log(err);
                    resolve(imageUrl);
                  }
                });
              });
            }
          });
      })
      .catch(function (err) {
        console.log(err);
        return imageUrl;
      });
  } else {
    return Promise.resolve(imageUrl);
  }
}

function getFileName(fileName, fileFormat) {

  var number = 1;

  var getUniqueName = function (name) {
    return new Promise(function (resolve, reject) {
      s3Client.get(s3Directory + name + fileFormat).on('response', function (res) {
        if (res.statusCode === 404) {
          resolve(name + fileFormat);
        } else if (res.statusCode === 200) {
          number++;
          resolve(getUniqueName(fileName + '_' + number, fileFormat));
        } else { // error
          if (number > 1) {
            return getUniqueName(fileName + '_' + number, fileFormat);
          } else {
            return getUniqueName(fileName, fileFormat);
          }
        }
      }).end();
    });
  };

  return getUniqueName(fileName, fileFormat);
}

/**
 * This script is used to save product from csv to database
 * To run:
 * node modules/rbiz/utils/importToDb.js
 * @author: ivan
 * @author: gazandi
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var csv = require('csv-parser');
var fs = require('fs');
var json2csv = require('json2csv');
var Promise = require('bluebird');
var prompt = require('prompt');
var url = require('url');
var seller = require('./seller');
var rbizProductQuery = require('./../queries/rbizProduct');
var RbizProduct = require('./../models/rbizProduct');
var sellerHelper = require('./../helpers/seller');

prompt.start();
prompt.get(['csvPath', 'sellerName'], function (err, input) {

  var sellerId = sellerHelper.getSellerId(input.sellerName);
  if (sellerId === -1) {
    console.log('Seller name not valid');
    return;
  }

  var inputFile = _.trim(input.csvPath);

  return read(inputFile)
    .then(function (products) {
      console.log(products.length);
      return Promise.each(products, function (product) {
        return saveToDatabase(product, sellerId);
      })
        .then(function () {
          console.log('Done save to database');
        });
    })
    .catch(function (err) {
      console.log(err);
    });

});

function read(csvPath) {
  var products = [];

  return new Promise(function (resolve) {
    fs.createReadStream(csvPath)
      .pipe(csv())
      .on('data', function (data) {
        products.push(data);
      })
      .on('end', function () {
        resolve(products);
      });
  });
}

function saveToDatabase(product, sellerId) {
  var rbizProduct = {};
  _.forOwn(product, function (value, key) {
    var keyWithoutStar = key.replace(new RegExp('[*]$'), '');
    rbizProduct[keyWithoutStar] = product[key];
  });

  rbizProduct.Url = rbizProduct.Images ? rbizProduct.Images : rbizProduct['Images*'];
  rbizProduct.Url = rbizProduct.Url.replace(/\\$/, '');
  rbizProduct.Images = null;
  rbizProduct.sellerId = sellerId;

  // var numberPattern = /\d+/g;
  // rbizProduct.Unit = _.trim(rbizProduct['Minimum Order'].replace(/\d/g, ''));
  // rbizProduct['Minimum Order'] = rbizProduct['Minimum Order'].match(numberPattern);

  return rbizProductQuery.saveProduct(new RbizProduct(rbizProduct));

  // return rbizProductQuery.getProducts({ Url: rbizProduct.Url, 'No.': product['No.']})
  //   .then(function (res) {
  //     if (res.length === 0) {
  //       // return rbizProductQuery.saveProduct(new RbizProduct(rbizProduct));
  //       console.log(product['No.']);
  //       return Promise.resolve();
  //     } else {
  //       return Promise.resolve();
  //     }
  //   });
}

/**
 * This script is used to save get Images, upload to S3, and save to database
 * To run:
 * node modules/rbiz/utils/getDetails.js
 * @author: ivan
 * @author: gazandi
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var csv = require('csv-parser');
var fs = require('fs');
var json2csv = require('json2csv');
var Promise = require('bluebird');
var prompt = require('prompt');
var url = require('url');
var validUrl = require('valid-url');

var downloader = require('../../../helpers/file/downloader');
var rbizProductQuery = require('./../queries/rbizProduct');
var request = require('../../../helpers/common/request');
var s3Helper = require('../../../helpers/common/s3');
var s3Client = s3Helper.getS3Client();
var seller = require('./seller');
var sellerHelper = require('./../helpers/seller');

var imageDirectory = '~/images/aliexpress/products/';
var s3Directory = 'rbiz/';
var akamizedUrl = 'https://ralali.akamaized.net/rbiz/';
var oneImageOnly = true;

const REQUEST_TIMEOUT = 60000;
const MAX_ATTEMPT = 5;

prompt.start();
prompt.get(['sellerName'], function (err, input) {

  var sellerId = sellerHelper.getSellerId(input.sellerName);
  if (sellerId === -1) {
    console.log('Seller name not valid');
    return;
  }

  return getProducts(sellerId)
    .then(function (products) {
      console.log('Products: ' + products.length);
      return Promise.each(products, function (product) {
        return getDetails(product);
      });
    })
    .then(function () {
      console.log('Done');
      process.exit();
    });

});

/**
 * @param sellerId
 * @returns {Promise<Product>}
 */
function getProducts(sellerId) {
  var options = {};
  options.sellerId = sellerId;
  options.uploaded = false;

  // options.$or = [];
  // options.$or.push({ $or: [{ 'Category': '' }, { 'Category': null }] });
  // options.$or.push({ $or: [{ 'Sub Category': '' }, { 'Sub Category': null }] });
  // options.$or.push({ $or: [{ 'Short Description': '' }, { 'Short Description': null }] });
  // options.$or.push({ $or: [{ 'Long Description': '' }, { 'Long Description': null }] });
  // options.$or.push({ $or: [{ 'Height': '' }, { 'Height': null }] });
  // options.$or.push({ $or: [{ 'Width': '' }, { 'Width': null }] });
  // options.$or.push({ $or: [{ 'Length': '' }, { 'Length': null }] });
  // options.$or.push({ $or: [{ 'Weight': '' }, { 'Weight': null }] });
  // options.$or.push({ $or: [{ 'Shipping Height': '' }, { 'Shipping Height': null }] });
  // options.$or.push({ $or: [{ 'Shipping Width': '' }, { 'Shipping Width': null }] });
  // options.$or.push({ $or: [{ 'Shipping Length': '' }, { 'Shipping Length': null }] });
  // options.$or.push({ $or: [{ 'Shipping Weight': '' }, { 'Shipping Weight': null }] });
  return rbizProductQuery.getProducts(options);
}

function getDetails(product) {

  var productJSON = product.toJSON();
  var productUrl = product.Url ? product.Url : productJSON['Url*'];
  if (productUrl) {
    productUrl = productUrl.replace(/\\$/, '');
  }

  if (!validUrl.isUri(productUrl)) return product;
  var parsedUrl = url.parse(productUrl);
  var sellerId = product.sellerId;
  if (parsedUrl.host && parsedUrl.protocol && parsedUrl.path) {
    console.log('b');
    var requestOptions = {
      url: productUrl,
      method: 'GET',
      timeout: REQUEST_TIMEOUT,
      headers: {
        'cookie': ['ali_apache_id=118.97.158.23.1476717570130.480089.6; ' +
        'ali_apache_track=mt=1|mid=id1176413561cerl; ' +
        'xman_us_f=x_l=0&x_locale=en_US&no_popup_today=n' +
        '&x_user=ID|Gazandi|Cahyadarma|ifm|795362067&last_popup_time=1476717827007; ' +
        'xman_f=Gpy8qQWrGlYoyx3WOkvCIlHnUdaemxVRzkNr1ZfvL1PtK1iJZwZdE8ukdcwCSL+' +
        'fmNH7jIgVDM/xG5cRsO9aazoJDy7gRPObWi0b+enRQI7FAL8ziXNkG/HCZdOx6M/A7amz5KKv/' +
        'p43B0yykz5lBUblgT51eJaoqougAR+shf3gVpzzi/docDDis1ERcrhWPEVmP/' +
        '3D8eGjSvoAdnkdGX7hrqchol079NYuE8Ezb+R8Brl5X/oX5oeN0u+' +
        '1NFoxFKMVAzyJsK+owAgPb++Mi5bMgJM4LWJrGsGdi8Lu0tCoHzF74XNtuc/' +
        'mkVRNZjzKim7U9qonuxCxggHsWX99APgvdRWTbO/SI72e9oHd7FPi/nItDpzsfyazxuln+PXP; ' +
        'aep_common_f=gUAdn3n4Rhw2IDdGx+mVfF0RFbNJdlLcVztn0j1SJMdQHZ6kdeHgmQ==; ' +
        'aep_usuc_f=site=glo&region=ID&b_locale=en_US&isb=y&isfm=y&x_alimid=795362067&c_tp=USD; ' +
        'intl_common_forever=KVXawlT0dEJfsEtLnxfVrOZPX5bMQ89X/x1vaVtFC8DUiDd+sk6pvw==; ' +
        'aep_history=keywords%5E%0Akeywords%09%0A%0Aproduct_selloffer%5E%0Aproduct_selloffer%' +
        '0932675968104%091218665525%091283053502%0932546175295%0932476601219%0932596587526%' +
        '0932355683700%091371733385; l=An5-jEhfE3PiQznwR5LCJHcRzprA2EIt; ' +
        'isg=AgsLXmmP18cSzguSIaiglmKBm6belB8iBkn8s30Ijcq1nCv-BXCvcqk-UCWH; ' +
        '_ga=GA1.2.242326579.1477725329; ali_beacon_id=180.253.31.204.1476717548599.487219.5; ' +
        'cna=7NmLEH+qUTACAbT9H8y4VOKf; __utma=3375712.242326579.1477725329.1477772753.1477787486.4; ' +
        '__utmz=3375712.1477725415.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); ' +
        'JSESSIONID=4B6BD13235BEDCDF020642D61BBBEB05; ali_apache_tracktmp=; intl_locale=en_US; ' +
        'xman_t=O4cnYGmREmcLW3HY8xPdhBAQ9xFWGp0qHJFYGEdRuXQ1bPCJBQ0kAGmaWHiFTUCK4rMJH7qF1/' +
        'Ub93ebsIgVfGSVxGubco1+3Fp0ODCa+S8=; acs_usuc_t=acs_rt=bdb86ea07bda4b13b168634d5e397727; ' +
        '__utmc=3375712'],
      }
    };

    return request.makeRequest(requestOptions, MAX_ATTEMPT)
      .then(function (html) {
        var body = html.body;

        if (body) {
          var host = parsedUrl.host;
          switch (host) {
            case 'www.gearbest.com':
              console.log('Gearbest');
              product.Images = getImageFromGearbest(body);
              break;
            case 'www.alibaba.com':
            case 'wholesaler.alibaba.com':
              console.log('Alibaba');
              product.Images = getImageFromAlibaba(body);
              break;
            case 'www.aliexpress.com':
              console.log('Aliexpress');
              product.Images = getImageFromAliexpress(body);
              break;
            case 'www.lazada.co.id':
              console.log('Lazada');
              product.Images = getImageFromLazada(body);
              break;
            default:
              console.log('Not supported');
          }
          return uploadImages(product, sellerId)
            .then(function () {
              product.uploaded = true;

              if (_.isArray(product.ImagesAkamized)) {
                product.ImagesAkamized = product.ImagesAkamized[0];
              }

              if (_.isArray(product.Images)) {
                product.Images = product.Images[0];
              }

              return rbizProductQuery.saveProduct(product)
                .then(function () {
                  console.log('Saved: ' + product['Product Name']);
                });
            });
        }
        return product;
      })
      .catch(function (err) {
        console.log(err);
        return product;
      });
  } else {
    return Promise.resolve(product);
  }
}

function getImageFromGearbest(body) {
  var $ = cheerio.load(body);
  var imageUrls = [];
  $('.n_thumbImg_item img').each(function () {
    var url = $(this).attr('data-big-img');
    imageUrls.push(url);
  });

  return _.compact(imageUrls);
}

function getImageFromAlibaba(body) {
  var $ = cheerio.load(body);
  var imageUrls = [];
  $('.thumb img').each(function () {
    var url = $(this).attr('src').replace('.jpg_50x50', '');
    imageUrls.push(url);
  });

  if (imageUrls.length === 0) {
    $('#J-image-icontent img').each(function () {
      var url = $(this).attr('src').replace('.jpg_350x350', '');
      imageUrls.push(url);
    });
  }

  return _.compact(imageUrls);
}

function getImageFromAliexpress(body) {
  console.log(body);
  var $ = cheerio.load(body);
  var imageUrls = [];

  $('.image-thumb-list img').each(function () {
    var url = $(this).attr('src').replace('.jpg_50x50', '');
    imageUrls.push(url);
  });

  if (imageUrls.length == 0) {
    $('.ui-image-viewer-thumb-frame img').each(function () {
      var url = $(this).attr('src').replace('.jpg_640x640', '');
      imageUrls.push(url);
    });
  }

  if (imageUrls.length === 0) {
    $('.image img').each(function () {
      var url = $(this).attr('src');
      imageUrls.push(url);
    });
  }
  console.log(imageUrls);
  return _.compact(imageUrls);
}

function getImageFromLazada(body) {
  var $ = cheerio.load(body);
  var imageUrls = [];
  $('.productImage').each(function () {
    var url = $(this).attr('data-big');
    imageUrls.push(url);
  });

  return _.compact(imageUrls);
}

function uploadImages(product, sellerId) {

  var productJSON = product.toJSON();
  var imageUrls = product.Images;

  if (oneImageOnly) {
    imageUrls = _.dropRight(imageUrls, imageUrls.length - 1);
  }

  return Promise.mapSeries(imageUrls, function (imageUrl) {
    var brand = product.Brand ? product.Brand : productJSON['Brand*'];
    var name = product['Product Name'] ? product['Product Name'] : productJSON['Product Name*'];
    brand = _.trim(brand);
    name = _.trim(name);
    return downloadAndUploadToS3(brand, name, sellerId, imageUrl);
  })
    .then(function (res) {
      var res = _.flatten(res);
      product.Images = res;

      product.ImagesAkamized = [];
      _.forEach(product.Images, function (imageUrl) {
        var imageUrlAkamized = akamizedUrl + imageUrl;
        product.ImagesAkamized.push(imageUrlAkamized);
      });

      return product;
    });
}

function downloadAndUploadToS3(brand, productName, sellerId, imageUrl) {

  var parsedImageUrl = url.parse(imageUrl);
  if (!parsedImageUrl.protocol) {
    parsedImageUrl.protocol = 'http';
    imageUrl = url.format(parsedImageUrl);
  }

  if (validUrl.isUri(imageUrl)) {
    // Get file format
    var lastIndexOfDot = imageUrl.lastIndexOf('.');
    var fileFormat = imageUrl.substr(lastIndexOfDot);

    // Set filename
    brand = brand.toLowerCase().replace(/\W/g, '').replace(new RegExp('[ /]', 'g'), '_').replace(new RegExp('_$'), '');
    productName = productName.toLowerCase().replace(/\W/g, '').replace(new RegExp('[ /]', 'g'), '_').replace(new RegExp('_$'), '');
    var fileName = sellerId + '_' + brand + '_' + productName.substr(0, 30);

    return getFileName(fileName, fileFormat)
      .then(function (fileName) {
        var localFileLocation = imageDirectory.replace('~', process.env.HOME) + fileName;

        return downloader.download(imageUrl, localFileLocation)
          .then(function (res) {
            if (res === true) {
              var s3FileLocation = s3Directory + fileName;

              return new Promise(function (resolve, reject) {
                s3Client.putFile(localFileLocation, s3FileLocation, function (err, res) {
                  if (res) {
                    console.log('success: ' + fileName);
                    resolve(fileName);
                  }
                  if (err) {
                    console.log(err);
                    resolve(imageUrl);
                  }
                });
              });
            }
          });
      })
      .catch(function (err) {
        console.log(err);
        return imageUrl;
      });
  } else {
    return Promise.resolve(imageUrl);
  }
}

function getFileName(fileName, fileFormat) {
  var number = 1;

  var getUniqueName = function (name) {
    return new Promise(function (resolve, reject) {
      s3Client.get(s3Directory + name + fileFormat).on('response', function (res) {
        if (res.statusCode === 404) {
          resolve(name + fileFormat);
        } else if (res.statusCode === 200) {
          number++;
          resolve(getUniqueName(fileName + '_' + number, fileFormat));
        } else { // error
          if (number > 1) {
            return getUniqueName(fileName + '_' + number, fileFormat);
          } else {
            return getUniqueName(fileName, fileFormat);
          }
        }
      }).end();
    });
  };

  return getUniqueName(fileName, fileFormat);
}

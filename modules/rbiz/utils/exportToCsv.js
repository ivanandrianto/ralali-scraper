/**
 * This script is used to get export rbizproduct of a seller from db to csv
 * To run:
 * node modules/rbiz/utils/convertToImageUrl.js
 * @author: ivan
 * @author: gazandi
 */

var _ = require('lodash');
var csv = require('csv-parser');
var fs = require('fs');
var json2csv = require('json2csv');
var prompt = require('prompt');
var sellerHelper = require('./../helpers/seller');

var rbizProductQuery = require('./../queries/rbizProduct');

/**
 * This script is used to export from db to csv
 * To run:
 * node modules/scraper/website/aliexpress/exportToCsv.js
 * @author: ivan
 */

var fields = [
  'No.',
  'Brand',
  'Product Name',
  'Model',
  'Reference SKU',
  'Category',
  'Sub Category',
  'Sub Sub Category',
  'Sub Sub Sub Category',
  'Short Description',
  'Long Description',
  'Height',
  'Width',
  'Length',
  'Weight',
  'Shipping Height',
  'Shipping Width',
  'Shipping Length',
  'Shipping Weight',
  'Stock',
  'Minimum Order',
  'Lead Time ID',
  'Images',
  'ImagesAkamized',
  'Url',
  'Manual Books',
  'Retail Normal Price',
  'Retail Discounted Price',
  'Disc. Start Date',
  'Disc. End Date',
  'Wholesale Price 1',
  'WP1 Start Quantity',
  'WP1 End Quantity',
  'Wholesale Price 2',
  'WP2 Start Quantity',
  'WP2 End Quantity',
  'Wholesale Price 3',
  'WP3 Start Quantity',
  'WP3 End Quantity',
  'Wholesale Price 4',
  'WP4 Start Quantity',
  'WP4 End Quantity',
  'detailAdded',
  'uploaded'
];

prompt.start();
prompt.get(['sellerName', 'csvOutput'], function (err, input) {
  var sellerId = sellerHelper.getSellerId(input.sellerName);
  if (sellerId === -1) {
    console.log('Seller name not valid');
    return;
  }

  var options = { sellerId: sellerId };
  var sortOptions = { createdAt: 1 };
  return rbizProductQuery.getProducts(options, sortOptions)
    .then(function (products) {
      console.log(products.length);
      // _.forEach(products, function (product) {
      //   product.Images = product.Images ? _.join(product.Images, ',') : undefined;
      //   product.ImagesAkamized = product.ImagesAkamized ? _.join(product.ImagesAkamized, ',') : undefined;
      // });

      var productsToExport = [];

      _.forEach(products, function (product) {
        product = product.toJSON();

        if (product.Images && product.Images.length > 0) {
          var image = product.Images[0] ? product.Images[0].replace(new RegExp('[[]"\']'), '') : null;
          product.Images = image;
          console.log(product.Images);
        }

        if (product.ImagesAkamized && product.ImagesAkamized.length > 0) {
          var imageAkamized = product.ImagesAkamized[0] ? product.ImagesAkamized[0]
            .replace(new RegExp('[[]"\']'), '') : null;
          product.ImagesAkamized = imageAkamized;
        }

        productsToExport.push(product);
      });

      var csv = json2csv({ data: productsToExport, fields: fields }, function (err, csv) {
        if (err) throw err;
        var output = input.csvOutput.replace(new RegExp('.csv$'), '') + '.csv';
        fs.writeFile(output, csv, function (err) {
          if (err) throw err;
          console.log('file saved');
        });
      });

    });
});

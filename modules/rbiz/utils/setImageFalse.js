/**
 * This script is used to set uploaded status of products of a seller to false
 * Don't forget to change the number in array on this script
 * To run:
 * node modules/rbiz/utils/setImageFalse.js
 * @author: ivan
 * @author: gazandi
 */

'use strict';

var _ = require('lodash');
var Promise = require('bluebird');
var prompt = require('prompt');

var rbizProductQuery = require('./../queries/rbizProduct');
var seller = require('./seller');
var sellerHelper = require('./../helpers/seller');

prompt.start();
prompt.get(['sellerName'], function (err, input) {

  var sellerId = sellerHelper.getSellerId(input.sellerName);
  if (sellerId === -1) {
    console.log('Seller name not valid');
    return;
  }

  var options = { sellerId: sellerId };
  return rbizProductQuery.getProducts(options)
    .then(function (products) {
      console.log(products.length);
      return Promise.each(products, function (product) {
        return setToFalse(product);
      });
    })
    .then(function () {
      console.log('Done save to database');
    });

});

function setToFalse(product) {
  // Write the numbers of product you want to set the uploaded status to false
  // on the following array
  var numbers = [];
  var productJson = product.toJSON();
  var productJsonNumber = productJson.No;
  var productNumber = -1;
  _.forOwn(productJsonNumber, function (value) {
    productNumber = parseInt(value);
  });
  console.log(productNumber);
  if (_.indexOf(numbers, productNumber) !== -1) {
    product.Images = '';
    product.ImagesAkamized = '';
    product.uploaded = false;
    return rbizProductQuery.saveProduct(product);
  } else {
    return Promise.resolve();
  }
}

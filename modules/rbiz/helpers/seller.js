var _  = require('lodash');
var seller = require('../utils/seller');

exports.getSellerId = function (sellerName) {
  var sellerNameList = [];
  _.forEach(seller.sellers, function (seller) {
    sellerNameList.push(seller.name.toLowerCase());
  });

  var index = _.indexOf(sellerNameList, sellerName.toLowerCase());
  return index === -1 ? -1 : seller.sellers[index].id;
};

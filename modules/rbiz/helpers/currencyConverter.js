/**
 * This script is used to convert currency with google finance
 * To run:
 * node modules/rbiz/helpers/currencyConverter.js
 * @author: gazandi
 */

'use strict';

var _ = require('lodash');
var _ = require('lodash');
var cheerio = require('cheerio');
var prompt = require('prompt');
var url = require('url');
var validUrl = require('valid-url');
var request = require('../../../helpers/common/request');

const REQUEST_TIMEOUT = 60000;
const MAX_ATTEMPT = 5;

exports.getCurrency = function getFromGoogle(currency) {
  var Url = 'https://www.google.com/finance/converter?a=1&from=USD&to=' + currency;
  Url = Url.replace(/\\$/, '');
  var parsedUrl = url.parse(Url);

  if (parsedUrl.host && parsedUrl.protocol && parsedUrl.path) {
    var requestOptions = {
      url: Url,
      method: 'GET',
      timeout: REQUEST_TIMEOUT
    };
    return request.makeRequest(requestOptions, MAX_ATTEMPT)
      .then(function (html) {
        var body = html.body;
        if (body) {
          var $ = cheerio.load(body);
          var unparsed = $('span')[0].children[0].data;
          var curr = unparsed.split(' ');
          console.log(curr[0] * 1);
          return curr[0] * 1;
        }
        return 0;
      });
  }

};

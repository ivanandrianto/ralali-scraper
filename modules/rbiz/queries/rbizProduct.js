var RbizProduct = require('../models/rbizProduct');

exports.saveProduct = function (product) {
  return product.save()
    .then(function (product) {
      return product._id;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.getProducts = function (options, sortOptions) {
  return RbizProduct.find(options).sort(sortOptions)
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.getOneProduct = function (options) {
  return RbizProduct.findOne(options);
};

exports.getProductByUrl = function (url) {
  return RbizProduct.findOne({ Url: url })
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.removeProduct = function (options) {
  return RbizProduct.remove(options).exec();
};

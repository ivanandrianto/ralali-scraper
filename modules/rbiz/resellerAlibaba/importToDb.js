/**
 * This script is used to get import Alibaba scraper data in CSV to database
 * To run:
 * node modules/scraper/website/alibaba/import/productFromCsv.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var csv = require('csv-parser');
var fs = require('fs');
var json2csv = require('json2csv');
var Promise = require('bluebird');
var prompt = require('prompt');
var url = require('url');
var rbizProductQuery = require('../queries/rbizProduct');
var RbizProduct = require('../models/rbizProduct');
var sellerHelper = require('../helpers/seller');

prompt.start();
prompt.get(['csvPath', 'sellerName'], function (err, input) {
  var inputFile = _.trim(input.csvPath);

  var sellerId = sellerHelper.getSellerId(input.sellerName);
  if (sellerId === -1) {
    console.log('Seller name not valid');
    return;
  }

  return read(inputFile)
    .then(function (products) {
      console.log(products.length);
      return Promise.each(products, function (product) {
        return saveToDatabase(product, sellerId);
      })
        .then(function () {
          console.log('Done save to database');
        });
    })
    .catch(function (err) {
      console.log(err);
    })
    .then(function () {
      process.exit();
    });

});

function read(csvPath) {
  var products = [];

  return new Promise(function (resolve) {
    fs.createReadStream(csvPath)
      .pipe(csv())
      .on('data', function (data) {
        products.push(data);
      })
      .on('end', function () {
        resolve(products);
      });
  });
}

function saveToDatabase(product, sellerId) {
  var rbizProduct = {};

  rbizProduct['Product Name'] = product.title;
  // rbizProduct['Retail Normal Price'] = getMaxPrice(product.price);
  // rbizProduct.Unit = getSellingUnit(product['min-order']);
  // rbizProduct['Minimum Order'] = getMinOrder(product['min-order']);
  rbizProduct.Url = getUrl(product.product_url);
  rbizProduct.type = 'reseller_alibaba';
  rbizProduct.sellerId = sellerId;
  console.log(rbizProduct);

  return rbizProductQuery.saveProduct(new RbizProduct(rbizProduct));
}

function getMaxPrice(price) {
  // Get price
  var maxPrice = 0;

  var priceText = _.trim(price);
  var firstDigitIndex = priceText.search(/\d/);

  priceText = priceText.substr(firstDigitIndex, firstDigitIndex.length);
  var prices = priceText.split('-');
  var minPriceStr = prices[0];
  if (!isNaN(minPriceStr)) {
    var minPrice = minPriceStr;
  }
  if (prices.length > 1) {
    var maxPriceStr = prices[1];
    if (!isNaN(maxPriceStr)) {
      maxPrice = maxPriceStr;
    }
  } else {
    maxPrice = minPrice ? minPrice : undefined;
  }

  return maxPrice;
}

function getSellingUnit(priceUnit) {
  if (!priceUnit) {
    return null;
  }
  return _.trim(priceUnit.replace(/\d/g, ''));
}

function getMinOrder(minOrder) {
  if (!minOrder) {
    return null;
  }
  return _.trim(minOrder.replace(/\D/g, ''));
}

function getUrl(url) {
  if (!url) {
    return null;
  }
  return _.trim(url.replace(new RegExp('^//'), 'https://').replace(/\\$/, ''));
}

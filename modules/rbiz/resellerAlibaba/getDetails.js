/**
 * This script is used to get details of resellera alibaba products
 * To run:
 * node modules/rbiz/runner/resellerAlibaba/getDetails.js
 * @author: ivan
 * @author: gazandi
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var csv = require('csv-parser');
var fs = require('fs');
var json2csv = require('json2csv');
var Promise = require('bluebird');
var prompt = require('prompt');
var url = require('url');
var util = require('util');
var validUrl = require('valid-url');

var downloader = require('../../../helpers/file/downloader');
var rbizProductQuery = require('./../queries/rbizProduct');
var request = require('../../../helpers/common/request');
var RbizProduct = require('../models/rbizProduct');

var self = this;

const REQUEST_TIMEOUT = 60000;

const MAX_ATTEMPT = 5;

exports.run = function () {
  return getProducts()
    .then(function (products) {
      console.log('a');
      console.log('Products: ' + products.length);

      // products = _.dropRight(products, 2025);
      return Promise.each(products, function (product) {
        return self.getDetails(product);
      });
    })
    .then(function () {
      console.log('Done');
      process.exit();
    });
  console.log('a');
};

/**
 * @param sellerId
 * @returns {Promise<Product>}
 */
function getProducts() {
  var options = {
    type: 'reseller_alibaba',
    $or: [
      { detailAdded: false },
      { 'Short Description': null }
    ]
  };

  return rbizProductQuery.getProducts(options);
}

exports.getDetails = function (product) {
  console.log('Get Details of: ' + product['Product Name']);
  var productJSON = product.toJSON();
  var productUrl = product.Url ? product.Url : productJSON['Url*'];
  if (productUrl) {
    productUrl = productUrl.replace(/\\$/, '');
  }
  if (!validUrl.isUri(productUrl)) return product;

  var requestOptions = {
    url: productUrl,
    method: 'GET',
    timeout: REQUEST_TIMEOUT,
    headers: {
      'cookie': ['ali_apache_id=118.97.158.23.1476717570130.480089.6; ' +
      'ali_apache_track=mt=1|mid=id1176413561cerl; ' +
      'xman_us_f=x_l=0&x_locale=en_US&no_popup_today=n' +
      '&x_user=ID|Gazandi|Cahyadarma|ifm|795362067&last_popup_time=1476717827007; ' +
      'xman_f=Gpy8qQWrGlYoyx3WOkvCIlHnUdaemxVRzkNr1ZfvL1PtK1iJZwZdE8ukdcwCSL+fmNH7jIgVDM' +
      '/xG5cRsO9aazoJDy7gRPObWi0b+enRQI7FAL8ziXNkG/HCZdOx6M/A7amz5KKv/' +
      'p43B0yykz5lBUblgT51eJaoqougAR+shf3gVpzzi/docDDis1ERcrhWPEVmP/' +
      '3D8eGjSvoAdnkdGX7hrqchol079NYuE8Ezb+R8Brl5X/oX5oeN0u+1NFoxFKMVAzyJsK+owAgPb++' +
      'Mi5bMgJM4LWJrGsGdi8Lu0tCoHzF74XNtuc/mkVRNZjzKim7U9qonuxCxggHsWX99APgvdRWTbO/' +
      'SI72e9oHd7FPi/nItDpzsfyazxuln+PXP; ' +
      'aep_common_f=gUAdn3n4Rhw2IDdGx+mVfF0RFbNJdlLcVztn0j1SJMdQHZ6kdeHgmQ==; ' +
      'aep_usuc_f=site=glo&region=ID&b_locale=en_US&isb=y&isfm=y&x_alimid=795362067&c_tp=USD; ' +
      'intl_common_forever=KVXawlT0dEJfsEtLnxfVrOZPX5bMQ89X/x1vaVtFC8DUiDd+sk6pvw==; ' +
      'aep_history=keywords%5E%0Akeywords%09%0A%0Aproduct_selloffer%5E%0Aproduct_selloffer%' +
      '0932675968104%091218665525%091283053502%0932546175295%0932476601219%0932596587526%0932355683700%091371733385; ' +
      'l=An5-jEhfE3PiQznwR5LCJHcRzprA2EIt; ' +
      'isg=AgsLXmmP18cSzguSIaiglmKBm6belB8iBkn8s30Ijcq1nCv-BXCvcqk-UCWH; ' +
      '_ga=GA1.2.242326579.1477725329; ali_beacon_id=180.253.31.204.1476717548599.487219.5; ' +
      'cna=7NmLEH+qUTACAbT9H8y4VOKf; __utma=3375712.242326579.1477725329.1477772753.1477787486.4; ' +
      '__utmz=3375712.1477725415.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); ' +
      'JSESSIONID=4B6BD13235BEDCDF020642D61BBBEB05; ' +
      'ali_apache_tracktmp=; intl_locale=en_US; ' +
      'xman_t=O4cnYGmREmcLW3HY8xPdhBAQ9xFWGp0qHJFYGEdRuXQ1bPCJBQ0kAGmaWHiFTUCK4rMJH7qF1/Ub93ebsIgVfGSVxGubco1+3Fp0ODCa+S8=; ' +
      'acs_usuc_t=acs_rt=bdb86ea07bda4b13b168634d5e397727; ' +
      '__utmc=3375712'],
    }
  };

  return request.makeRequest(requestOptions, MAX_ATTEMPT)
    .then(function (html) {
      var body = html.body;

      if (!body) {
        return product;
      }

      product.sellByLot = false;
      var $ = cheerio.load(body);

      getQuickDetails(product, $);
      getPackagingSize(product, $);
      getCategory(product, $);
      getWholesalePrice(product, $);
      getResellerPrice(product, $);
      getMinOrder(product, $);
      getLot(product, $);
      getLongDescription(product, $);
      var variants = getVariants(product, $);
      if (!product['Wholesale Price 1']) {
        getRefPrice(product, $);
      }

      // Currently we use short description as an indicator that the page has been scraped successfully
      if (product['Short Description']) {
        product.detailAdded = true;
      }

      console.log(product);

      return rbizProductQuery.saveProduct(product)
        .then(function () {
          console.log('Saved: ' + product['Product Name']);
        });

      // Multi-variant save
      // if (variants.length > 1) {
      //   return Promise.each(variants, function (variant) {
      //     return saveProduct(product, variant);
      //   })
      //     .then(function (res) {
      //       var flatted = _.flatten(res);
      //       return flatted;
      //     });
      // } else {
      //   return rbizProductQuery.saveProduct(product)
      //     .then(function () {
      //       console.log('Saved: ' + product['Product Name']);
      //     });
      // }

    })
    .catch(function (err) {
      console.log(err);
      return product;
    });
};

function saveProduct(product, variant) {
  var product = _.cloneDeep(product.toJSON());
  delete product._id;
  delete product.createdAt;
  delete product.updatedAt;
  product['Short Description'] += '\nVariant: ' + variant.name;
  console.log(product);
  var productMongooseObj = new RbizProduct(product);
  console.log(productMongooseObj);
  return rbizProductQuery.saveProduct(productMongooseObj)
    .then(function () {
      console.log('Saved: ' + product['Product Name']);
    });
}

function getQuickDetails(product, $) {
  var quickDetails = '';
  $('.quick-detail tr').each(function () {
    $(this).children('.J-name').each(function () {
      var name = _.trim($(this).text());
      var value = _.trim($(this).next().text());

      if (name.match(new RegExp('brand', 'i'))) {
        product.Brand = value;
      }

      if (name.match(new RegExp('model', 'i'))) {
        product.Model = value;
      }

      if (name.match(new RegExp('size', 'i'))) {
        var valueSplit = value.split(/[*xX]+/);

        if (valueSplit.length === 3) {
          product.Length = valueSplit[0].replace(/[^0-9$.,]/g, '');
          product.Width = valueSplit[1].replace(/[^0-9$.,]/g, '');
          product.Height = valueSplit[2].replace(/[^0-9$.,]/g, '');
        } else if (valueSplit.length === 2) {
          if (valueSplit[0].match(new RegExp('d', 'i'))) {
            product.Length = valueSplit[0].replace(/[^0-9$.,]/g, '');
            product.Width = valueSplit[0].replace(/[^0-9$.,]/g, '');
            product.Height = valueSplit[1].replace(/[^0-9$.,]/g, '');
          }
        }
      }

      if (name.match(new RegExp('weight', 'i'))) {
        product.Weight = _.trim(value);
      }

      //data-section-title="Product Description"
      quickDetails += name + ' ' + value + '\n';
    });
  });
  product['Short Description'] = quickDetails;
}

function getPackagingSize(product, $) {
  $('#J-product-detail h3').each(function () {
    var detailTitle = _.trim($(this).text());
    if (detailTitle === 'Packaging') {
      $(this).next().children('tr').children('.mk-name').each(function () {
        var detailName = _.trim($(this).text());

        if (detailName.match(new RegExp('size'))) {
          var sizeDetail = _.trim($(this).next().text());
          var sizeDetailSplitByX = sizeDetail.split('X');
          var length = sizeDetailSplitByX[0].replace(/[^0-9$.,]/g, '');
          var width = sizeDetailSplitByX[1].replace(/[^0-9$.,]/g, '');
          var height = sizeDetailSplitByX[2].replace(/[^0-9$.,]/g, '');
          product['Shipping Length'] = length;
          product['Shipping Width'] = width;
          product['Shipping Height'] = height;

          var sizeUnit;
          if (sizeDetail.match(new RegExp('cm', 'i'))) {
            sizeUnit = 'cm';
          } else if (sizeDetail.match(new RegExp('mm', 'i'))) {
            sizeUnit = 'mm';
          }

          if (sizeUnit === 'mm') {
            product['Shipping Length'] = Number(length) / 10;
            product['Shipping Width'] = Number(width) / 10;
            product['Shipping Height'] = Number(height) / 10;
          }

        } else if (detailName.match(new RegExp('weight'))) {
          product['Shipping Weight'] = _.trim($(this).next().text());
        }

      });
    }
  });
}

function getCategory(product, $) {

  var breadcrumbTexts = [];
  $('.ui-breadcrumb span a span').each(function () {
    var text = _.trim($(this).text());
    breadcrumbTexts.push(text);
  });

  breadcrumbTexts = _.drop(breadcrumbTexts, breadcrumbTexts.length - 3);
  product.Category = breadcrumbTexts[0];
  product['Sub Category'] = breadcrumbTexts[1];
  product['Sub Sub Category'] = breadcrumbTexts[2];
}

function getWholesalePrice(product, $) {

  var wholesalePrices = [];
  $('#ladderPrice li').each(function () {
    var wholesalePrice = {};
    var quantityText = _.trim($(this).children('.ma-quantity-range').text());
    var quantityTextSplit = quantityText.split('-');
    if (quantityTextSplit.length === 2) {
      wholesalePrice.startQuantity = quantityTextSplit[0].replace(/\D/g, '');
      wholesalePrice.endQuantity = quantityTextSplit[1].replace(/\D/g, '');
    } else if (quantityTextSplit.length === 1) {
      if (quantityText.match(new RegExp('>'))) {
        wholesalePrice.startQuantity = quantityText.replace(/\D/g, '');
      } else if (quantityText.match(new RegExp('<'))) {
        wholesalePrice.endQuantity = quantityText.replace(/\D/g, '');
      } else {
        wholesalePrice.startQuantity = quantityText.replace(/\D/g, '');
        wholesalePrice.endQuantity = wholesalePrice.startQuantity;
      }
    }

    var priceText = _.trim($(this).children('.ma-spec-price').text().replace('US $', ''));
    wholesalePrice.price = priceText;
    wholesalePrices.push(wholesalePrice);
  });

  var wpTextFormat = 'Wholesale Price %s';
  var wpStartTextFormat = 'WP%s Start Quantity';
  var wpEndTextFormat = 'WP%s End Quantity';
  for (var i = 0; i < wholesalePrices.length && i < 4; i++) {
    var wpText = util.format(wpTextFormat, i + 1);
    var wpStartText = util.format(wpStartTextFormat, i + 1);
    var wpEndText = util.format(wpEndTextFormat, i + 1);
    product[wpText] = wholesalePrices[i].price;
    product[wpStartText] = wholesalePrices[i].startQuantity;
    product[wpEndText] = wholesalePrices[i].endQuantity;
  }

}

function getResellerPrice(product, $) {
  var retailNormalPrice = _.trim($('.reseller-price').text().replace('US $', ''));
  if (retailNormalPrice) {
    product['Retail Normal Price'] = retailNormalPrice;
  }
}

function getMinOrder(product, $) {
  product['Minimum Order'] = _.trim($('.ma-min-order').text().replace(/\D/g, ''));

  if (!product['Minimum Order']) {
    product['Minimum Order'] = $('.quantity-value input').attr('value');
  }
}

function getLot(product, $) {
  $('.ma-brief-item-key').each(function () {
    var text = $(this).text();
    if (text.match(new RegExp('sell by lot', 'i'))) {
      // console.log(_.trim($(this).next().text().replace(/\D/g, '')));
      product['Minimum Order'] = _.trim($(this).next().text().replace(/\D/g, ''));
      product.sellByLot = true;
    }
  });
}

function getLongDescription(product, $) {
  var longDescription = '';
  $('img').remove();
  $('#J-rich-text-description div').each(function () {
    var titleSection = $(this).attr('data-section-title');
    console.log('ts: ' + titleSection);
    if (titleSection && isImportantSection(titleSection)) {
      longDescription += $(this).html().replace(/\s/g, '').replace(/\t/g, '');
    }
  });
  product['Long Description'] = longDescription;
}

function isImportantSection(titleSection) {
  switch (titleSection) {
    case 'Our Services':
      break;
    case 'Remarks':
      break;
    case 'Company Information':
      break;
    default:
      return true;
  }
  return false;
}

function getRefPrice(product, $) {
  var text = _.trim($('.ma-ref-price').text().replace('US$', ''));
  var textSplit = text.split('-');
  var maxPriceIndex = textSplit.length > 1 ? 1 : 0;
  product['Wholesale Price 1'] = textSplit[maxPriceIndex].replace(/[^0-9.,]/g, '');
  product['WP1 Start Quantity'] =  product['Minimum Order'] ? product['Minimum Order'] : undefined;
}

function getVariants(product, $) {
  var variants = [];
  $('.sku-attr-val-item').each(function () {
    var variant = {};
    variant.name = _.trim($(this).children('.sku-attr-val-text').text());
    variant.minQuantity = $(this).children('.quantity-value').children('input').attr('value');
    variants.push(variant);
  });

  if (variants.length > 0) {
    product.isMultiVariant = true;
  }
  return variants;
}

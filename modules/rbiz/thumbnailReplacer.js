/**
 * This script is used to save get Images, upload to S3, and save to database
 * To run:
 * node modules/scraper/website/aliexpress/getImage.js
 * @author: gazandi
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var json2csv = require('json2csv');
var Promise = require('bluebird');
var prompt = require('prompt');
var url = require('url');
var validUrl = require('valid-url');
var imagemagick = require('imagemagick-native');

var downloader = require('../../helpers/file/downloader');
var request = require('../../helpers/common/request');
var s3Helper = require('../../helpers/common/s3');
var s3Client = s3Helper.getS3Client();
var s3Helper2 = require('../../helpers/common/s3copy');
var s3Client2 = s3Helper2.getS3Client();

var imageDirectory = '~/images/aliexpress/products/';
var s3Directory = 'rbiz/';
var akamizedUrl = 'https://ralali.akamaized.net/rbiz/';
var oneImageOnly = true;

prompt.start();
prompt.get(['prefix'], function (err, input) {
    return getImages(input.prefix);
  });

function callback(err, res) {
  if (err) return;

}

function to130x130(str) {
  var b = '_130x130';
  if (str.indexOf(b, str.length - 14) > -1) return str;
  var output = [str.slice(0, str.indexOf('.', str.length - 5)), b, str.slice(str.indexOf('.', str.length - 5))].join('');
  return output;
}


function to130x1302(str) {
  var b = '130x130';
  if (str.indexOf(b, str.length - 14) > -1) return str;
  var output = [str.slice(0, str.indexOf('.', str.length - 5)), b, str.slice(str.indexOf('.', str.length - 5))].join('');
  return output;
}

var allKeys = [];
function listAllKeys(marker, cb, prefix)
{
  s3Client.listObjects({ Bucket: s3bucket, Marker: marker, prefix: prefix }, function (err, data) {
    allKeys.push(data.Contents);
    if (data.IsTruncated)
      listAllKeys(data.NextMarker, cb);
    else
      cb();
  });
}

function getImages(prefix) {
  var i = 0;
  s3Client2.streamKeys({ prefix: prefix }).on('data', function (key) {
      // Promise.each(data.Contents, function (product) {
      if (i > 0) return;
      var buffer = [];
      if (i == 0) {
        console.log(key);
        // s3Client.get(to130x130(key)).on('response', function (resf) {
        // if (resf.statusCode === 404 || resf.statusCode === 200) {
        s3Client.deleteFile(to130x130(key), function (err, res) {
          if (err) console.log(err);
          s3Client.getFile(key, function (err, resg) {
            if (err) return;
            resg.on('data', function (chunk) {
              buffer.push(chunk);
            });
            resg.on('end', function () {
              var buf = Buffer.concat(buffer);
              imagemagick.convert({
                srcData: buf,
                width: 130,od
                height: 130
              }, function (err, resp) {
                  if (err) return;
                  var headers = {
                    'Content-Type': 'image/jpg'
                  };
                  s3Client.putBuffer(resp, to130x130(key), headers, function (err, res) {
                    if (err) return;
                    console.log('lol');
                  });
                });
            });
          });
          // check `err`, then do `res.pipe(..)` or `res.resume()` or whatever.
        });
        //   } else {
        //     console.log('error');
        //   }
        // }).end();
      }
      i++;
    }).on('error', function (err) {
      console.log(err);
    });
}

var mongoose  = require('mongoose');
var dbHelper = require('../../../helpers/common/database');
dbHelper.connect(dbHelper.databases.main);
var Schema = mongoose.Schema;

var rbizProductSchema = new Schema({
  'No.': { type: String, required: false },
  'Brand': { type: String, required: false },
  'Product Name': { type: String, required: false },
  'Model': { type: String, required: false },
  'Reference SKU': { type: String, required: false },
  'Category': { type: String, required: false },
  'Sub Category': { type: String, required: false },
  'Sub Sub Category': { type: String, required: false },
  'Sub Sub Sub Category': { type: String, required: false },
  'Short Description': { type: String, required: false },
  'Long Description': { type: String, required: false },
  'Height': { type: String, required: false },
  'Width': { type: String, required: false },
  'Length': { type: String, required: false },
  'Weight': { type: String, required: false },
  'Shipping Height': { type: String, required: false },
  'Shipping Width': { type: String, required: false },
  'Shipping Length': { type: String, required: false },
  'Shipping Weight': { type: String, required: false },
  'Stock': { type: String, required: false },
  'Minimum Order': { type: String, required: false },
  'Lead Time ID': { type: String, required: false },
  'Images': [],
  'ImagesAkamized': [],
  'Url': { type: String, required: false },
  'Manual Books': { type: String, required: false },
  'Retail Normal Price': { type: String, required: false },
  'Retail Discounted Price': { type: String, required: false },
  'Disc. Start Date': { type: String, required: false },
  'Disc. End Date': { type: String, required: false },
  'Wholesale Price 1': { type: String, required: false },
  'WP1 Start Quantity': { type: String, required: false },
  'WP1 End Quantity': { type: String, required: false },
  'Wholesale Price 2': { type: String, required: false },
  'WP2 Start Quantity': { type: String, required: false },
  'WP2 End Quantity': { type: String, required: false },
  'Wholesale Price 3': { type: String, required: false },
  'WP3 Start Quantity': { type: String, required: false },
  'WP3 End Quantity': { type: String, required: false },
  'Wholesale Price 4': { type: String, required: false },
  'WP4 Start Quantity': { type: String, required: false },
  'WP4 End Quantity': { type: String, required: false },
  'Unit': { type: String, required: false },
  sellerId: { type: String, required: false },
  uploaded: { type: Boolean, default: false },
  detailAdded: { type: Boolean, default: false },
  type: { type: String },
  isMultiVariant: { type: Boolean, default: false }
},
{
  timestamps: true
},
{
  collection: 'rbizproducts'
});

var RbizProduct = mongoose.model('RbizProduct', rbizProductSchema);

module.exports = RbizProduct;

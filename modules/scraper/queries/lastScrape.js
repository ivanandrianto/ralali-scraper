var LastScrape = require('../models/lastScrape');
var self = this;

exports.getLastScrape = function (website, type) {
  return LastScrape.findOne({ website: website, type: type })
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.setLastScrape = function (website, type, subCategory, page, url) {

  return self.getLastScrape(website, type)
    .then(function (lastScrape) {
      if (lastScrape) {
        lastScrape.subCategory = subCategory;
        lastScrape.page = page;
        lastScrape.url = url;
        return lastScrape.save()
          .catch(function (err) {
            console.log(err);
          });
      } else {
        var lastScrapeObj = {};
        lastScrapeObj.website = website;
        lastScrapeObj.type = type;
        lastScrapeObj.subCategory = subCategory;
        lastScrapeObj.page = page;
        lastScrapeObj.url = url;
        var lastScrapeMongooseObj = new LastScrape(lastScrapeObj);
        return lastScrapeMongooseObj.save()
          .catch(function (err) {
            console.log(err);
          });
      }
    });
};

var fs = require('fs');
var json2csv = require('json2csv');

var Company = require('../models/company');

exports.saveCompany = function (company) {
  return company.save()
    .then(function (company) {
      return company._id;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.removeCompany = function (options) {
  return Company.remove(options).exec();
};

exports.findCompaniesBySourceWebsite = function (source) {
  return Company.find({ sourceWebsite: source })
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.findCompaniesBySourceUrl = function (sourceUrl) {
  return Company.findOne({ sourceUrl: sourceUrl })
    .then(function (res) {
      if (!res) {
        sourceUrl = sourceUrl.replace('http://', '').replace('https://', '');
        var regExpString = '^(http|https):\/\/' + sourceUrl;
        var option = { sourceUrl: new RegExp(regExpString, 'i') };
        return Company.findOne(option)
          .then(function (res) {
            return res;
          });
      } else {
        return res;
      }
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.findCompaniesAll = function () {
  return Company.find()
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.findCompanyById = function (id) {
  return Company.findOne({ _id: id })
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.findCompaniesWithAddressLike = function (addressLike) {
  return Company.find({ address: new RegExp(addressLike, 'g') })
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.findCompaniesByVerifiedStatus = function (status) {
  if (status === '1') {
    return Company.find({ verified: true })
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      console.log(err);
    });
  } else {
    return Company.find({ verified: false })
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      console.log(err);
    });
  }
};

exports.createCompany = function (companyInfo) {
  return Company.create(companyInfo)
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.getCsvFilePath = function () {
  return Company.find()
    .then(function (res) {
      var tableTitleArr = new Array();
      Company.schema.eachPath(function (path) {
        tableTitleArr.push(path);
      });
      console.log(tableTitleArr);
      var csv = json2csv({ data: res, fields: tableTitleArr });
      return csv;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.runConverter = function () {
  return new Promise(function (resolve, reject) {
    console.log('Running converter...');
    var filenames = fs.readdirSync('/shared/ralali/images/');
    var spawn = require('child_process').spawnSync;

    var isPaused = false;
    for (var i in filenames) {
      console.log(filenames[i]);
      var process = spawn('python', ['/shared/ralali/pyscript/scan.py', filenames[i]]);
      var scannedStr = process.stdout.toString();
      console.log(scannedStr);

      // var queryEmail = {};
      // var queryPhone = {};
      // queryEmail['emailImage'] = '~/images/' + filenames[i];
      // queryPhone['phoneImage'] = '~/images/' + filenames[i];
      //
      // var eventEmail = {};
      // var eventPhone = {};
      // eventEmail['email'] = scannedStr;
      // eventPhone['phones'] = [scannedStr];
      //
      // var emailCommand = 'mongo scraper --eval \'db.companies.update({"emailImage": "~/images/' + filenames[i] + '"}, {$set: {"email" : "' + scannedStr.replace(/\r?\n|\r/g, "") + '"}})\';';
      // var phoneCommand = 'mongo scraper --eval \'db.companies.update({"phoneImage": "~/images/' + filenames[i] + '"}, {$set: {"phones" : ["' + scannedStr.replace(/\r?\n|\r/g, "") + '"] }})\';';
      // var processMongoEmail = spawn(emailCommand);
      // var processMongoPhone = spawn(phoneCommand);
      // console.log(emailCommand);
      // console.log(phoneCommand);
      // console.log("Email\n=====\n" + processMongoEmail.stdout.toString());
      // console.log("Phone\n=====\n" + processMongoPhone.stdout.toString());
      console.log('Loop: ' + i);
    }
    resolve('{ status : "success" }');
  });
};

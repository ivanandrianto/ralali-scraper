var Product = require('../models/product');

exports.saveProduct = function (product) {
  return product.save()
    .then(function (product) {
      return product._id;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.findProducts = function (options, sortOptions) {
  return Product.find(options).sort(sortOptions)
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.findProductsBySourceWebsite = function (source) {
  return Product.find({ sourceWebsite: source })
    .then(function (res) {
      return res;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.findProductsBySourceUrl = function (sourceUrl) {
  return Product.findOne({ sourceUrl: sourceUrl })
    .then(function (res) {
      if (!res) {
        sourceUrl = sourceUrl.replace('http://', '').replace('https://', '');
        var regExpString = '^(http|https):\/\/' + sourceUrl;
        var option = { sourceUrl: new RegExp(regExpString, 'i') };
        return Product.findOne(option)
          .then(function (res) {
            return res;
          });
      } else {
        return res;
      }
    })
    .catch(function (err) {
      console.log(err);
    });
};

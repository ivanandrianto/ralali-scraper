var companyQuery = require('../queries/company');
var productQuery = require('../queries/product');
var _ = require('lodash');

exports.filterCompanies = function (companies, sourceWebsite) {
  var filteredCompanies = [];

  companies = _.compact(companies);
  _.forEach(companies, function (company) {
    var inFilteredCompanies = _.find(filteredCompanies, _.matchesProperty('sourceUrl', company.sourceUrl));
    if (!inFilteredCompanies) {
      filteredCompanies.push(company);
    }
  });

  return companyQuery.findCompaniesBySourceWebsite(sourceWebsite)
    .then(function (existingCompanies) {
      existingCompanies = _.keyBy(existingCompanies, 'sourceUrl');
      _.remove(filteredCompanies, function (current) {
        var exist = false;
        var sourceUrl = current.sourceUrl;
        var companyWithSameSourceUrl = existingCompanies[sourceUrl];
        if (companyWithSameSourceUrl) {
          exist = true;
        }
        return exist;
      });
      return filteredCompanies;
    })
    .catch(function (err) {
      if (err) {
        console.log(err);
      }
    });
};

exports.filterProducts = function (products, sourceWebsite) {
  var filteredProducts = [];

  products = _.compact(products);
  _.forEach(products, function (product) {
    var inFilteredProducts = _.find(filteredProducts, _.matchesProperty('sourceUrl', product.sourceUrl));
    if (!inFilteredProducts) {
      filteredProducts.push(product);
    }
  });

  return productQuery.findProductsBySourceWebsite(sourceWebsite)
    .then(function (existingProducts) {
      existingProducts = _.keyBy(existingProducts, 'sourceUrl');
      _.remove(filteredProducts, function (current) {
        var exist = false;
        var sourceUrl = current.sourceUrl;
        var productWithSameSourceUrl = existingProducts[sourceUrl];
        if (productWithSameSourceUrl) {
          exist = true;
        }
        return exist;
      });
      return filteredProducts;
    })
    .catch(function (err) {
      if (err) {
        console.log(err);
      }
    });
};

var Bluebird = require('bluebird');
var companyQuery = require('../queries/company');
var Company = require('../models/company');
var productQuery = require('../queries/product');
var Product = require('../models/product');
var _ = require('lodash');
var self = this;

exports.saveAllCompanies = function (source, companies) {
  var scrapeResult = _.cloneDeep(companies);
  console.log('Saving to database......');
  return companyQuery.findCompaniesBySourceWebsite(source)
    .then(function (existingCompanies) {
      existingCompanies = _.keyBy(existingCompanies, 'sourceUrl');
      _.remove(scrapeResult, function (current) {
        var exist = false;
        var sourceUrl = current.sourceUrl;
        var companyWithSameSourceUrl = existingCompanies[sourceUrl];
        if (companyWithSameSourceUrl) {
          exist = true;
        }
        return exist;
      });
      return scrapeResult;
    })
    .then(function (scrapeResult) {
      var tasks = [];

      _.forEach(scrapeResult, function (company) {
        var companyMongooseObj = new Company({
          name: company.name,
          address: company.address,
          sourceUrl: company.sourceUrl,
          sourceWebsite: company.sourceWebsite
        });
        tasks.push(addCompanyToDatabase(companyMongooseObj));
      });

      return Bluebird.all(tasks)
        .then(function () {
          console.log('Successfully added to database');
        })
        .catch(function (err) {
          console.log(err);
        });
    });
};

exports.saveCompany = function (company) {
  var categories = company.categories || company.category;
  var subCategories = company.subCategories || company.subCategory;

  var companyObj = {};
  companyObj.name = company.name ? company.name : undefined;
  companyObj.address = company.address ? company.address : undefined;
  companyObj.sourceUrl = company.sourceUrl ? company.sourceUrl : undefined;
  companyObj.sourceWebsite = company.sourceWebsite ? company.sourceWebsite : undefined;
  companyObj.phones = company.phones ? company.phones : undefined;
  companyObj.email = company.email ? company.email : undefined;
  companyObj.phoneImage = company.phoneImage ? company.phoneImage : undefined;
  companyObj.emailImage = company.emailImage ? company.emailImage : undefined;
  companyObj.description = company.description ? company.description : undefined;
  companyObj.categories = categories ? categories : undefined;
  companyObj.subCategories = subCategories ? subCategories : undefined;

  var companyMongooseObj = new Company(companyObj);
  return addCompanyToDatabase(companyMongooseObj)
    .then(function (id) {
      if (id) {
        return id;
      }

      // If failed, try to find if the company has already in DB
      return companyQuery.findCompaniesBySourceUrl(company.sourceUrl)
        .then(function (companyInDB) {
          if (companyInDB) {
            return self.addCategoryAndSubCategories(company.sourceUrl, company.category, company.subCategory)
              .then(function () {
                return companyInDB._id;
              });
          }
        });
    });
};

exports.addCategoryAndSubCategories = function (sourceUrl, category, subCategory) {
  return Company.update({ sourceUrl: sourceUrl },
      { $addToSet: { categories: category, subCategories: subCategory } }
    );
};

function addCompanyToDatabase(company) {
  return companyQuery.saveCompany(company)
    .then(function (id) {
      return id;
    });
};

exports.saveProduct = function (product) {
  var productObj = {};
  productObj.name = product.name ? product.name : undefined;
  productObj.description = product.description ? product.description : undefined;
  productObj.sourceUrl = product.sourceUrl ? product.sourceUrl : undefined;
  productObj.sourceWebsite = product.sourceWebsite ? product.sourceWebsite : undefined;
  productObj.category = product.category ? product.category : undefined;
  productObj.subCategory = product.subCategory ? product.subCategory : undefined;
  productObj.prices = product.prices ? product.prices : undefined;
  productObj.companyId = product.companyId ? product.companyId : undefined;
  productObj.images = product.images ? product.images : undefined;
  productObj.minOrder = product.minOrder ? product.minOrder : undefined;
  productObj.lastUpdateBySource = product.lastUpdateBySource ? product.lastUpdateBySource : undefined;

  var productMongooseObj = new Product(productObj);
  return addProductToDatabase(productMongooseObj)
    .then(function (id) {
      if (id) {
        return id;
      }

      // If failed, try to find if the product has already in DB
      return productQuery.findProductsBySourceUrl(product.sourceUrl)
        .then(function (productInDB) {
          if (productInDB) {
            return productInDB._id;
          }
          return null;
        });
    });
};

function addProductToDatabase(company) {
  return productQuery.saveProduct(company)
    .then(function (id) {
      return id;
    });
};

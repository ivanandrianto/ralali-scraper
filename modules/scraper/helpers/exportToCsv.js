/**
 * To run: node modules/scraper/helpers/exportToCsv.js
 */

var _ = require('lodash');
var csv = require('csv-parser');
var fs = require('fs');
var json2csv = require('json2csv');
var commandLineArgs = require('command-line-args');
var productQuery = require('../queries/product');

var optionDefinitions = [
  { name: 'sourceWebsite', alias: 'w', type: String },
  { name: 'fields', alias: 'f', type: String },
  { name: 'output', alias: 'o', type: String }
];

var options = commandLineArgs(optionDefinitions)

var fields = options.fields.split(',');
var outputFileName = options.output;
delete options.fields;
delete options.output;

console.log(options);

productQuery.findProducts(options, null)
  .then(function (products) {
    var csv = json2csv({ data: products, fields: fields }, function (err, csv) {
      if (err) throw err;
      var output = outputFileName.replace(new RegExp('.csv$'), '') + '.csv';
      fs.writeFile(output, csv, function (err) {
        if (err) throw err;
        console.log('file saved');
      });
    });
  });

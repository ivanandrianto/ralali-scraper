var mongoose  = require('mongoose');
var dbHelper = require('../../../helpers/common/database');
dbHelper.connect(dbHelper.databases.main);
var Schema    = mongoose.Schema;

var CompanySchema = new Schema({
    name: { type: String, required: true },
    address: { type: String, required: false },
    description: { type: String, required: false },
    email: { type: String },
    emailImage: { type: String },
    phones: [],
    phoneImage: { type: String },
    sourceUrl: { type: String, required: true, unique: true },
    sourceWebsite: { type: String, required: true },
    categories: [],
    subCategories: [],
    verified: { type: Boolean, default: false }
  },
  {
    timestamps: true
  },
  {
    collection: 'companies'
  });

var Company = mongoose.model('Company', CompanySchema);

module.exports = Company;

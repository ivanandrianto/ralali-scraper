var mongoose = require('mongoose');
var dbHelper = require('../../../helpers/common/database');
dbHelper.connect(dbHelper.databases.main);
var Schema = mongoose.Schema;

var LastScrapeSchema = new Schema({
    website: { type: String },
    type: { type: String },
    subCategory: { type: String },
    page: { type: Number, integer: true },
    url: { type: String }
  },
  {
    timestamps: true
  },
  {
    collection: 'lastScrape'
  });

var LastScrape = mongoose.model('LastScrape', LastScrapeSchema);

module.exports = LastScrape;

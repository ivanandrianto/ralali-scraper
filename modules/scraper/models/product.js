var mongoose = require('mongoose');
var dbHelper = require('../../../helpers/common/database');
dbHelper.connect(dbHelper.databases.main);
var Schema = mongoose.Schema;

var ProductSchema = new Schema({
    name: { type: String, required: true },
    brand: { type: String, required: false },
    sku: { type: String, required: false },
    description: { type: String, required: false },
    shortDescription: { type: String, required: false },
    sourceUrl: { type: String, required: true, unique: true },
    sourceWebsite: { type: String, required: true },
    category: { type: String, required: false },
    subCategory: { type: String, required: false },
    subSubCategory: { type: String, required: false },
    verified: { type: Boolean, default: false },
    prices: [{ type: Number, integer: true }],
    price: { type: Number, integer: true },
    oldPrice: { type: Number, integer: true },
    specialPrice: { type: Number, integer: true },
    priceReductionAmount: { type: Number, integer: true },
    priceReductionPercentage: { type: Number, integer: true },
    weight: { type: String },
    companyId: { type: Schema.Types.ObjectId, required: false },
    images: [],
    minOrder: { type: Number, integer: true },
    lastUpdateBySource: { type: Date }
  },
  {
    timestamps: true
  },
  {
    collection: 'products'
  });

var Product = mongoose.model('Product', ProductSchema);

module.exports = Product;

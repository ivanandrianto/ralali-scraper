/**
 * This script is used to get product list from Depoteknik
 * To run:
 * node modules/scraper/runner/depoteknik/product.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var Promise = require('bluebird');
var url = require('url');
var util = require('util');

var downloader = require('../../../../helpers/file/downloader');
var lastScrapeQuery = require('../../queries/lastScrape');
var request = require('../../../../helpers/common/request');
var Product = require('../../models/product');
var productQueryHelper = require('../../queries/product');

var homeUrl = 'http://www.depoteknik.com/';
var host = 'www.depoteknik.com';
var imageDirectory = '~/images/depoteknik/products/';
var protocol = 'http';
var sourceWebsite = 'depoteknik';

const MAX_CONCURRENCY = 1;
const MAX_ATTEMPT = 3;
const REQUEST_TIMEOUT = 60000;

var lastScrape = null;
var currentSubCategory = null;
var currentPage = 0;

exports.run = function () {
  lastScrapeQuery.getLastScrape(sourceWebsite, 'product')
    .then(function (lastScrapeContext) {
      if (lastScrapeContext) {
        lastScrape = lastScrapeContext;
      }
      return getAllSubSubCategories();
    })
    .then(function (subSubCategories) {
      if (lastScrape) {
        var indexOfSubSubCategory = _.findIndex(subSubCategories, { 'name': lastScrape.subCategory });
        if (lastScrape && indexOfSubSubCategory !== -1) {
          subSubCategories = _.drop(subSubCategories, indexOfSubSubCategory);
        }
      }
      return getAllProducts(subSubCategories);
    })
    .catch(function (err) {
      console.log(err);
    })
    .then(function () {
      process.exit();
    });
};

/**
 * @typedef SubSubCategory
 * @property {String} category - Name of the category
 * @property {String} subCategory - Name of the sub category
 * @property {String} name - Name of the sub sub category
 * @property {String} url - Url of the sub sub category
 */

/**
 * This function is used to get a list of all subcategories
 *
 * @returns {Promise<Array.<SubSubCategory>>}
 */
function getAllSubSubCategories() {
  var allSubSubCategoryList = [];

  var requestSubCategoryListOptions = {
    url: homeUrl,
    method: 'GET',
    timeout: REQUEST_TIMEOUT
  };

  return request.makeRequest(requestSubCategoryListOptions, MAX_ATTEMPT)
    .then(function (html) {
      var body = html.body;
      if (!body) {
        return Promise.reject(null);
      } else {
        var $ = cheerio.load(body);

        $('.menu-wrapper > ul > li').each(function () {
          var category = _.trim($(this).children().children('.cat-name').text());

          $(this).children('.extend').children('.list-wrapper').children('.col-1').children('ul')
            .children('li').children('a').each(function () {
            var subCategory = _.trim($(this).text());

            // If there is no subcategory name, we use category as the subcategory name
            if (!subCategory) {
              subCategory = category;
            }

            $(this).next().children('.list-wrapper2').children('.col-2').children('ul').children('li')
              .children('a').each(function () {
              var name = _.trim($(this).text());
              var url = $(this).attr('href');

              var subSubCategory = {
                category: category,
                subCategory: subCategory,
                name: name,
                url: url
              };
              allSubSubCategoryList.push(subSubCategory);
            });
          });
        });

        return allSubSubCategoryList;
      }
    });
};

/**
 * @typedef Product
 * @property {String} name - Name of the product
 * @property {String} brand - Brand of the product
 * @property {String} description - Description about the product
 * @property {String} sourceUrl - URL to the details page
 * @property {string} sourceWebsite - In this case, the source website is indonetwork
 * @property {String} category - Category of the product
 * @property {String} subCategory - Sub category of the product
 * @property {String} subSubCategory - Sub sub category of the product
 * @property {Number} specialPrice - Special price of the product
 * @property {Number} oldPrice - Old price of the product
 * @property {String} images - path to the product images
 */

/**
 * This function is used to get a list of all products
 *
 * @param {Array<SubSubCategory>}
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProducts(subSubCategories) {
  return Promise.each(subSubCategories, function (subSubCategory) {
    return getProductsOfASubSubCategory(subSubCategory);
  }, { concurrency: MAX_CONCURRENCY })
    .then(function (res) {
      return _.flatten(res);
    });
};

/**
 * This function is used to get a list of all products of a sub sub category
 *
 * @param {SubSubCategory}
 * @returns {Promise<Array.<Product>}>}
 */
function getProductsOfASubSubCategory(subSubCategory) {
  var allProductsList = [];
  var page = 1;

  if (lastScrape && (subSubCategory.url === lastScrape.subCategory)) {
    page = lastScrape.page;
  }

  var pageUrl = util.format(
    subSubCategory.url + '?p=%s',
    page
  );

  var getProductList = function () {
    var requestProductListOptions = {
      url: pageUrl,
      method: 'GET',
      timeout: REQUEST_TIMEOUT
    };

    return request.makeRequest(requestProductListOptions, MAX_ATTEMPT)
      .then(function (html) {
        var body = html.body;
        if (!body) {
          return Promise.reject(null);
        } else {
          var $ = cheerio.load(body);

          var products = [];
          $('.products-grid > li').each(function () {
            var sourceUrl =  $(this).children('a').attr('href');

            if (sourceUrl) {
              var product = {
                sourceUrl: sourceUrl,
                category: subSubCategory.category,
                subCategory: subSubCategory.subCategory,
                subSubCategory: subSubCategory.name
              };
              products.push(product);
            }
          });

          return getAllProductsDetails(products)
            .then(function () {
              allProductsList = _.concat(allProductsList, products);

              var nextUrl = $('.next').attr('href');

              if (products.length > 0 && nextUrl) {
                pageUrl = nextUrl;
                page++;
                currentPage = page;
                currentSubCategory = subSubCategory.url;
                return getProductList();
              } else {
                return allProductsList;
              }
            });
        }
      });
  };

  return getProductList();
};

/**
 * This function is used to get details of all products
 * @param {Array.<Product>} products
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProductsDetails(products) {
  return Promise.each(products, function (product) {
    return getProductDetails(product);
  }, { concurrency: MAX_CONCURRENCY }).then(function (res) {
    return res;
  });
};

/**
 * This function is used to get details of a product from each products' page
 * @param {Product} product
 * @returns {Promise<Product>}
 */
function getProductDetails(product) {
  return productQueryHelper.findProducts({ sourceWebsite: sourceWebsite, sourceUrl: product.sourceUrl })
    .then(function (productsInDb) {
      if (productsInDb.length > 0) {
        return Promise.resolve();
      }

      var requestOptions = {
        url: product.sourceUrl,
        method: 'GET',
        timeout: REQUEST_TIMEOUT
      };

      return request.makeRequest(requestOptions, MAX_ATTEMPT)
        .then(function (html) {
          var body = html.body;
          if (!body) {
            return Promise.reject(null);
          } else {
            var $ = cheerio.load(body);

            product.name = _.trim($('.product-shop .product-name .h1').text());
            product.brand = _.trim($('.product-shop .brand').first().text());
            product.sku = _.trim($('.sku-number').first().text()).replace(/^Item# /, '');
            product.oldPrice = _.trim($('.price-info .old-price .price').first().text().replace(/\D+/g, ''));
            product.specialPrice = _.trim($('.price-info .special-price .price').first().text().replace(/\D+/g, ''));
            product.sourceWebsite = sourceWebsite;
            product.description = _.trim($('.product-desc').first().text());

            $('.data-table tr').each(function () {
              $(this).children('th').children('a').remove();
              $(this).children('td').children('a').remove();
              var key = _.trim($(this).children('th').text());
              var value = _.trim($(this).children('td').text());
              product.description += '\n' + key + ': ';
              product.description += value;
            });
            product.description = _.trim(product.description.replace(/\r/g, '').replace(/\t/g, '').replace(/ {2,}/g, ' ')
              .replace(/\n +/g, '\n').replace(/\n{2,}/g, '\n'));

            product.imageUrl = $('#image-main').attr('src');
            console.log(product);

            if (!product.subSubCategory) {
              var possiblesubSubCategories = [];
              $('.breadcrumbs a').each(function () {
                possiblesubSubCategories.push(_.trim($(this).text()));
              });
              product.subSubCategory = possiblesubSubCategories[possiblesubSubCategories.length - 1];
            }

            return getImage(product.imageUrl)
              .then(function (imgUrl) {
                product.images = [];
                product.images.push(imgUrl);
              })
              .then(function () {
                var productMongooseObj = new Product(product);
                return productQueryHelper.saveProduct(productMongooseObj)
                  .then(function () {
                    return lastScrapeQuery.setLastScrape(sourceWebsite, 'product',
                      product.subSubCategory, currentPage, product.sourceUrl);
                  });
              })
              .catch(function (err) {
                console.log(err);
              });

          }
        });
    });
};

/**
 * This function is used to download image
 * It returns the local file name
 *
 * @params {String}
 * @returns {Promise<String>}
 */
function getImage(imageUrl) {
  if (!imageUrl) {
    return Promise.resolve();
  }

  var paths = url.parse(imageUrl).pathname.split('/');
  var fileName = paths[paths.length - 1];
  var destination = imageDirectory.replace('~', process.env.HOME) + fileName;

  // Download the images
  return downloader.download(imageUrl, destination)
    .then(function (res) {
      if (res === true) {
        return fileName;
      }
    })
    .catch(function (err) {
      console.log(err);
      return null;
    });
};

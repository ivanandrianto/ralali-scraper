/**
 * This script is used to get product list from Mitutoyo
 * To run:
 * node modules/scraper/runner/medicalogy/product.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var Promise = require('bluebird');
var url = require('url');
var util = require('util');

var downloader = require('../../../../helpers/file/downloader');
var lastScrapeQuery = require('../../queries/lastScrape');
var request = require('../../../../helpers/common/request');
var Product = require('../../models/product');
var productQueryHelper = require('../../queries/product');

var homeUrl = 'https://www.medicalogy.com/';
var host = 'www.medicalogy.com';
var imageDirectory = '~/images/medicalogy/products/';
var protocol = 'https';
var sourceWebsite = 'medicalogy';

const MAX_CONCURRENCY = 1;
const MAX_ATTEMPT = 3;
const REQUEST_TIMEOUT = 60000;

var lastScrape = null;
var currentSubSubCategory = null;
var currentPage = 0;

exports.run = function () {
  lastScrapeQuery.getLastScrape(sourceWebsite, 'product')
    .then(function (lastScrapeContext) {
      if (lastScrapeContext) {
        lastScrape = lastScrapeContext;
      }
      return getAllSubSubCategories();
    })
    .then(function (subSubCategories) {
      if (lastScrape) {
        var indexOfSubSubCategory = _.findIndex(subSubCategories, { 'url': lastScrape.subCategory });
        if (lastScrape && indexOfSubSubCategory !== -1) {
          subSubCategories = _.drop(subSubCategories, indexOfSubSubCategory);
        }
      }
      return getAllProducts(subSubCategories);
    })
    .catch(function (err) {
      console.log(err);
    })
    .then(function () {
      process.exit();
    });
};

/**
 * @typedef SubSubCategory
 * @property {String} name - Name of the sub sub category
 * @property {String} category - Name of the category
 * @property {String} subCategory - Name of the sub category
 * @property {String} url - Url of the sub sub category
 */

/**
 * This function is used to get a list of all subcategories
 *
 * @returns {Promise<Array.<SubCategory>}>}
 */
function getAllSubSubCategories() {
  var allSubCategoryList = [];

  var requestSubCategoryListOptions = {
    url: homeUrl,
    method: 'GET',
    timeout: REQUEST_TIMEOUT
  };

  return request.makeRequest(requestSubCategoryListOptions, MAX_ATTEMPT)
    .then(function (html) {
      var body = html.body;
      if (!body) {
        return Promise.reject(null);
      } else {
        var $ = cheerio.load(body);

        $('.megamenu-wrapper').each(function () {
          var categoryName = $(this).children().first().text();
          $(this).children('.subcategory-block-wrapper').children('.category-level-2').each(function () {
            var subCategoryName = _.trim($(this).text());
            $(this).next().children('li').each(function () {
              var subSubCategoryName = _.trim($(this).text());
              var subSubCategoryurl = $(this).children('a').attr('href');
              var parsedUrl = url.parse(subSubCategoryurl);
              if (parsedUrl.host === null) {
                parsedUrl.host = host;
                parsedUrl.protocol = protocol;
                subSubCategoryurl = url.format(parsedUrl);
              }

              var subSubCategory = {
                name: subSubCategoryName,
                category: categoryName,
                subCategory: subCategoryName,
                url: subSubCategoryurl
              };

              allSubCategoryList.push(subSubCategory);
            });
          });
        });

        return allSubCategoryList;
      }
    });
}

/**
 * @typedef Product
 * @property {String} name - Name of the product
 * @property {String} brand - Brand of the product
 * @property {String} description - Description about the product
 * @property {String} sourceUrl - URL to the details page
 * @property {string} sourceWebsite - In this case, the source website is indonetwork
 * @property {String} category - Category of the product
 * @property {String} subCategory - Sub category of the product
 * @property {String} subCubCategory - Sub sub category of the product
 * @property {Number} specialPrice - Special price of the product
 * @property {Number} oldPrice - Old price of the product
 * @property {String} images - path to the product images
 */

/**
 * This function is used to get a list of all products
 *
 * @param {Array<SubSubCategory>}
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProducts(subSubCategories) {
  return Promise.each(subSubCategories, function (subSubCategory) {
    return getProductsOfASubSubCategory(subSubCategory);
  }, { concurrency: MAX_CONCURRENCY })
    .then(function (res) {
      return _.flatten(res);
    });
}

/**
 * This function is used to get a list of all products of a sub sub category
 *
 * @param {SubSubCategory}
 * @returns {Promise<Array.<Product>}>}
 */
function getProductsOfASubSubCategory(subSubCategory) {
  var allProductsList = [];
  var maxPage = 0;
  var page = 1;

  if (lastScrape && (subSubCategory.url === lastScrape.subCategory)) {
    page = lastScrape.page;
  }

  var getProductList = function () {
    var pageUrl = util.format(
      subSubCategory.url + '?page=%s',
      page
    );

    var requestProductListOptions = {
      url: pageUrl,
      method: 'GET',
      timeout: REQUEST_TIMEOUT
    };

    return request.makeRequest(requestProductListOptions, MAX_ATTEMPT)
      .then(function (html) {
        var body = html.body;
        if (!body) {
          return Promise.reject(null);
        } else {
          var $ = cheerio.load(body);

          // Get max page
          if (maxPage === 0) {
            var lastPage = $('.last').children('a').attr('href');
            var lastPageMatches = lastPage ? lastPage.match(/\d+$/) : undefined;
            if (lastPageMatches) {
              maxPage = lastPageMatches[0];
            } else {
              maxPage = -1;
            }
          }

          var products = [];
          $('.card-list .title a').each(function () {
            var sourceUrl = $(this).attr('href');
            var parsedUrl = url.parse(sourceUrl);
            if (parsedUrl.host === null) {
              parsedUrl.host = host;
              parsedUrl.protocol = protocol;
              sourceUrl = url.format(parsedUrl);
            }

            var name = $(this).text();
            var product = {
              name: name,
              sourceUrl: sourceUrl,
              category: subSubCategory.category,
              subCategory: subSubCategory.subCategory,
              subSubCategory: subSubCategory.name
            };
            console.log(product);

            products.push(product);

          });

          return getAllProductsDetails(products)
            .then(function () {
              allProductsList = _.concat(allProductsList, products);
              if (products.length > 0 && page < maxPage) {
                page++;
                currentPage = page;
                currentSubSubCategory = subSubCategory.url;
                return getProductList();
              } else {
                return allProductsList;
              }
            });
        }
      });
  };

  return getProductList();
}

/**
 * This function is used to get details of all products
 * @param {Array.<Product>} products
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProductsDetails(products) {
  return Promise.each(products, function (product) {
    return getProductDetails(product);
  }, { concurrency: MAX_CONCURRENCY }).then(function (res) {
    return res;
  });
}

/**
 * This function is used to get details of a product from each products' page
 * @param {Product} product
 * @returns {Promise<Product>}
 */
function getProductDetails(product) {
  return productQueryHelper.findProducts({ sourceWebsite: sourceWebsite, sourceUrl: product.sourceUrl })
    .then(function (productsInDb) {
      if (productsInDb.length > 0) {
        return Promise.resolve();
      }

      var requestOptions = {
        url: product.sourceUrl,
        method: 'GET',
        timeout: REQUEST_TIMEOUT
      };

      return request.makeRequest(requestOptions, MAX_ATTEMPT)
        .then(function (html) {
          var body = html.body;
          if (!body) {
            return Promise.reject(null);
          } else {
            var $ = cheerio.load(body);

            $('.discount-rate-box').remove();
            var brand = _.trim($('.product-brand-text a').text());
            var description = _.trim($('.card-list-content').text().replace(/∞/g, '°').replace(/¬†/g, ' ')
              .replace(/¬/g, '').replace(/‚Äì/g, '-').replace(/†/g, '').replace(/ +\n/g, '\n').replace(/\n{2,}/g, '\n'));
            var shortDescription = _.trim($('.excerpt').text());
            var oldPrice = _.trim(_.trim($('.product-detail-right-section .price-before-discount').first().text()).replace(/\.00$/, '').replace(/\D+/g, ''));
            var specialPrice = _.trim(_.trim($('.product-detail-right-section .price-current').first().text()).replace(/\.00$/, '').replace(/\D+/g, ''));
            var weight = _.trim($('.product-weight-text').text().replace(/^berat: /i, ''));

            product.brand = brand;
            product.description = description;
            product.shortDescription = shortDescription;
            product.oldPrice = oldPrice;
            product.specialPrice = specialPrice;
            product.sourceWebsite = sourceWebsite;
            product.weight = weight;

            var imgUrl = $('.image-thumb-size').attr('src');
            if (imgUrl) {
              var parsedUrl = url.parse(imgUrl);
              if (parsedUrl.host === null) {
                parsedUrl.host = host;
                parsedUrl.protocol = protocol;
                imgUrl = url.format(parsedUrl);
              }
            }

            return getImage(imgUrl)
              .then(function (imgUrl) {
                product.images = [];
                product.images.push(imgUrl);
              })
              .then(function () {
                var productMongooseObj = new Product(product);
                return productQueryHelper.saveProduct(productMongooseObj)
                  .then(function () {
                    return lastScrapeQuery.setLastScrape(sourceWebsite, 'product',
                      currentSubSubCategory, currentPage, product.sourceUrl);
                  });
              })
              .catch(function (err) {
                console.log(err);
              });
          }
        });
    });
}

/**
 * This function is used to download image
 * It returns the local file name
 *
 * @params {String}
 * @returns {Promise<String>}
 */
function getImage(imageUrl) {

  if (!imageUrl) {
    return Promise.resolve();
  }

  var paths = url.parse(imageUrl).pathname.split('/');
  var fileName = paths[paths.length - 1];
  var destination = imageDirectory.replace('~', process.env.HOME) + fileName;

  // Download the images
  return downloader.download(imageUrl, destination)
    .then(function (res) {
      if (res === true) {
        return fileName;
      }
    })
    .catch(function (err) {
      console.log(err);
      return null;
    });
}

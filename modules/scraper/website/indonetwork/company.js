/**
 * This script is used to get company list from Indonetwork
 * To run:
 * node module/scraper/runner/indonetwork/company.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var Promise = require('bluebird');
var url = require('url');
var util = require('util');

var downloader = require('../../../../helpers/file/downloader');
var lastScrapeQuery = require('../../queries/lastScrape');
var request = require('../../../../helpers/common/request');
var resultFilter = require('../../helpers/filterResult');
var saveHelper = require('../../helpers/saveResult');

var companyListUrl = 'http://www.indonetwork.co.id/companies/perusahaan/?page=%s';
var requestDetailsImageUrl = '%stexttoimage.php?txt=%s=&theme=undefined';
var sourceWebsite = 'indonetwork';
var protocol = 'http';
var imageDirectory = '~/images/';
var requestHeaders = {
  'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.89 Safari/537.36',
  'Connection': 'close'
};
var ocrPhoneUrl = 'http://localhost:5000/telephone/';
var ocrEmailUrl = 'http://localhost:5000/email/';
var self = this;

const MAX_CONCURRENCY = 3;
const MAX_ATTEMPT = 3;
const REQUEST_TIMEOUT = 60000;

var lastScrape = null;
var currentPage = 1;

exports.run = function () {
  lastScrapeQuery.getLastScrape(sourceWebsite, 'company')
    .then(function (lastScrapeContext) {
      if (lastScrapeContext) {
        lastScrape = lastScrapeContext;
      }
      return getAllCompanyList();
    })
    .catch(function (err) {
      console.log(err);
    })
    .then(function () {
      process.exit();
    });
};

/**
 * @typedef Company
 * @property {String} name - The company's name
 * @property {String} sourceUrl - URL to the details page
 * @property {string} address - The company's address
 * @property {string} category - The company's address
 * @property {string} subCategory - The company's address
 * @property {string} sourceWebsite - In this case, the source website is indotrading
 * @property {string} description - Description of the company
 * @property {string} phoneImage - Path to the image containing the company's phone
 * @property {string} emailImage - Path to the image containing the company's email
 */

/**
 * This function is used to get a list of all companies
 *
 * @returns {Promise<Array.<Company>}>}
 */
function getAllCompanyList() {
  var allCompanyList = [];
  var page = 1;

  if (lastScrape) {
    page = lastScrape.page;
  }
  console.log(lastScrape);

  var getCompanyList = function () {

    // Format the URL
    var pageUrl = util.format(
      companyListUrl,
      page
    );

    // HTTP request options
    var requestCompanyListOptions = {
      url: pageUrl,
      method: 'GET',
      timeout: REQUEST_TIMEOUT
    };

    // Perform HTTP request
    return request.makeRequest(requestCompanyListOptions, MAX_ATTEMPT)
      .then(function (html) {
        var body = html.body;
        if (!body) {
          return Promise.reject(null);
        } else {
          var $ = cheerio.load(body);
          var companyList = [];
          $('.list-item a').each(function () {
            var company = {};
            company.name = _.trim($(this).children().first().next()
              .children().first().text());
            company.address = _.trim($(this).children().first().next()
              .children().first().next().text());
            company.sourceWebsite = sourceWebsite;

            var sourceUrl = $(this).attr('href');
            if (sourceUrl) {
              var parsedSourceUrl = url.parse(sourceUrl);
              if (parsedSourceUrl.protocol === null) {
                parsedSourceUrl.protocol = protocol;
              }
              company.sourceUrl = url.format(parsedSourceUrl);
            }

            if (company.name && company.sourceUrl) {
              companyList.push(company);
            }
          });

          if (companyList.length === 0) { // Terminate if there is no company on the last page
            return allCompanyList;
          } else { // Continue if there are any companies on the last page

            allCompanyList = _.concat(allCompanyList, companyList);
            return resultFilter.filterCompanies(companyList, sourceWebsite)
              .then(function (companyList) {
                return getAllCompaniesDetails(companyList);
              })
              .then(function () {
                page++;
                currentPage = page;
                return getCompanyList();
              });
          }
        }
      })
      .catch(function (err) {
        console.log(err);
        return allCompanyList;
      });
  };

  return getCompanyList();
}

/**
 * This function is used to get a details of all companies
 * @param {Array.<Company>} companies
 * @returns {Promise<Array.<Company>}>}
 */
function getAllCompaniesDetails(companies) {
  return Promise.map(companies, function (company) {
    return self.getCompanyDetails(company);
  }, { concurrency: MAX_CONCURRENCY }).then(function (res) {
    return res;
  });
}

/**
 * This function is used to get additional details of a company on each companies' page
 * The additional details include description, phone and email
 * @param {Company} company
 * @returns {Promise<Company>}
 */
exports.getCompanyDetails = function (company) {
  console.log('Get details of ' + company.name);

  // Format the URL
  var sourceUrl = company.sourceUrl;
  var parsedSourceUrl = url.parse(sourceUrl);
  if (parsedSourceUrl.protocol === null) {
    parsedSourceUrl.protocol = protocol;
  }
  if (parsedSourceUrl.host === null) {
    parsedSourceUrl.host = host;
  }
  sourceUrl = url.format(parsedSourceUrl);

  // HTTP request options
  var requestCompanyDetailsOptions = {
    url: sourceUrl,
    method: 'GET',
    timeout: REQUEST_TIMEOUT
  };

  // Perform HTTP request
  return request.makeRequest(requestCompanyDetailsOptions, MAX_ATTEMPT)
    .then(function (html) {
      var body = html.body;
      if (!body) {
        return company;
      }

      var $ = cheerio.load(body);
      var phoneRequestToken = $('.mask-phone-button').attr('id');
      var emailRequestToken = $('.mask-email-button').attr('id');

      // The description can be in either .desc-comp or .cp-desc
      var description = $('.desc-comp').text();
      if (!description) {
        description = $('.cp-desc').text();
      }
      company.description = _.trim(description);

      // Get images of phone and email
      return getDetailsImage(sourceUrl, phoneRequestToken, emailRequestToken)
        .then(function (imagePaths) {
          company.phoneImage = imagePaths.phone ? imagePaths.phone : null;
          company.emailImage = imagePaths.email ? imagePaths.email : null;
          return company;
        })
        .then(function (company) {
          var splittedPhoneImagePath = company.phoneImage.split('/');
          var fileName = splittedPhoneImagePath[splittedPhoneImagePath.length - 1];
          var extractPhoneTextUrl = ocrPhoneUrl + fileName;
          var phoneImageRequestOptions = {
            url: extractPhoneTextUrl,
            method: 'GET',
            timeout: REQUEST_TIMEOUT,
            headers: requestHeaders,
            agent: false
          };
          return request.makeRequest(phoneImageRequestOptions, MAX_ATTEMPT)
            .then(function (res) {
              company.phones = res.body.result;
              return company;
            });
        })
       .then(function (company) {
          var splittedEmailImagePath = company.emailImage.split('/');
          var fileName = splittedEmailImagePath[splittedEmailImagePath.length - 1];
          var extractEmailTextUrl = ocrEmailUrl + fileName;
          var phoneImageRequestOptions = {
            url: extractEmailTextUrl,
            method: 'GET',
            timeout: REQUEST_TIMEOUT,
            headers: requestHeaders,
            agent: false
          };
          return request.makeRequest(phoneImageRequestOptions, MAX_ATTEMPT)
            .then(function (res) {
              company.email = res.body.result;
              return company;
            });
        });
    })
    .then(function (company) {
      // Save the company to db;
      return saveHelper.saveCompany(company)
        .then(function (id) {
          if (id) {
            company._id = id;
            console.log('Saved:\n' + company);
            return lastScrapeQuery.setLastScrape(sourceWebsite, 'company',
              null, currentPage, company.sourceUrl);
          }
          return company;
        });
    })
    .catch(function (err) {
      console.log(err);
      return company;
    });
};

/**
 * This function is used to get images of phone and email
 * and store the images on local computer
 * It returns object containing path of the images
 *
 * @returns {Promise<Object>}
 */
function getDetailsImage(sourceUrl, phoneRequestToken, emailRequestToken) {

  // Format the images URL
  var phoneImageUrl = util.format(
    requestDetailsImageUrl,
    sourceUrl,
    phoneRequestToken
  );

  var emailImageUrl = util.format(
    requestDetailsImageUrl,
    sourceUrl,
    emailRequestToken
  );

  // Format the destination for storing the images
  var directory = imageDirectory.replace('~', process.env.HOME);
  var phoneImageDestination = directory + phoneRequestToken;
  var emailImageDestination = directory + emailRequestToken;

  var imagePaths = {};

  // Download the images
  return downloader.download(phoneImageUrl, phoneImageDestination)
    .then(function (res) {
      if (res === true) {
        imagePaths.phone = imageDirectory + phoneRequestToken;
      }
    })
    .catch(function (err) {
      console.log(err);
    })
    .then(function () {
      return downloader.download(emailImageUrl, emailImageDestination)
        .then(function (res) {
          if (res === true) {
            imagePaths.email = imageDirectory + emailRequestToken;
          }
          return imagePaths;
        });
    })
    .catch(function (err) {
      console.log(err);
      return imagePaths;
    });
}

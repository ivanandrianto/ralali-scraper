/**
 * This script is used to get product list from Indonetwork
 * To run:
 * node module/scraper/website/runner/indonetwork/product.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var moment = require('moment');
var Promise = require('bluebird');
var url = require('url');
var util = require('util');

var downloader = require('../../../../helpers/file/downloader');
var companyScraper = require('./company');
var companyQuery = require('../../queries/company');
var lastScrapeQuery = require('../../queries/lastScrape');
var request = require('../../../../helpers/common/request');
var resultFilter = require('../../helpers/filterResult');
var saveHelper = require('../../helpers/saveResult');

var productCategoryUrl = 'http://www.indonetwork.co.id/categories';
var sourceWebsite = 'indonetwork';
var protocol = 'http';
var host = 'www.indonetwork.co.id';
var requestHeaders = {
  'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.89 Safari/537.36',
  'Connection': 'close'
};
var imageDirectory = '~/images/indonetwork/products/';

const MAX_CONCURRENCY = 1;
const MAX_ATTEMPT = 3;
const REQUEST_TIMEOUT = 60000;

var lastScrape = null;
var currentSubCategory = null;
var currentPage = 0;
var priorityOnly = true;

exports.run = function () {
  lastScrapeQuery.getLastScrape(sourceWebsite, 'product')
    .then(function (lastScrapeContext) {
      if (lastScrapeContext) {
        lastScrape = lastScrapeContext;
      }
      return getAllSubCategories();
    })
    .then(function (subCategories) {
      if (lastScrape) {
        var indexOfSubCategory = _.findIndex(subCategories, { 'url': lastScrape.subCategory });
        if (lastScrape && indexOfSubCategory !== -1) {
          subCategories = _.drop(subCategories, indexOfSubCategory);
        }
      }

      if (priorityOnly) {
        var priorities = [
          'Peralatan Kantor',
          'Elektronik & Peralatan Rumah Tangga',
          'Kesehatan & Kecantikan',
          'Sistem Keamanan',
          'CCTV & Kamera Keamanan Lainnya',
          'Keamanan Penyimpanan',
          'Akses Keamanan',
          'Keamanan Diri',
          'Alat Tes & Pengukuran',
          'Alat Pengukur Lapisan',
          'Pengukur Kedalaman',
          'Pengukur Ketinggian',
          'Pegukur Kelembapan',
          'Timbangan & Alat Ukur Kadar Lemak'
        ];

        // Filter priorities
        var prioritySubCategories = [];
        _.forEach(subCategories, function (subCategory) {
          if ((_.indexOf(priorities, subCategory.category) !== -1) || (_.indexOf(priorities, subCategory.name) !== -1)) {
            prioritySubCategories.push(subCategory);
          }
        });
        return getAllProducts(prioritySubCategories);
      } else {
        return getAllProducts(subCategories);
      }
    })
    .catch(function (err) {
      console.log(err);
    })
    .then(function () {
      process.exit();
    });
}

/**
 * @typedef SubCategory
 * @property {String} name - Name of the subcategory
 * @property {String} category - The catgeory (parent)
 * @property {string} url - URL of the subcategory
 */

/**
 * This function is used to get a list of subcategories
 * @returns {Promise<Array.<SubCategory>}>}
 */
function getAllSubCategories() {
  var allSubCategories = [];

  // HTTP request options
  // User-Agent header is required to avoid being redirected to mobile website
  var requestCategoryListOptions = {
    url: productCategoryUrl,
    method: 'GET',
    timeout: REQUEST_TIMEOUT,
    headers: requestHeaders,
    agent: false
  };

  // Perform HTTP request
  return request.makeRequest(requestCategoryListOptions, MAX_ATTEMPT)
    .then(function (html) {
      var body = html.body;
      if (!body) {
        return Promise.reject(null);
      } else {
        var $ = cheerio.load(body);

        $('.subkat').each(function () {
          var category = $(this).children('h4').text();
          var $subCategories = $(this).children('ul').children('li').children('a');

          var subCategories =  _.reduce($subCategories, function (tempSubCategories, current) {

            var subCategory = {};
            var name = _.trim(current.children[0].data);
            subCategory.name = name;
            subCategory.category = category;
            subCategory.url = current.attribs.href;

            tempSubCategories.push(subCategory);
            return tempSubCategories;

          }, []);

          allSubCategories.push(subCategories);
        });
        allSubCategories = _.flattenDeep(allSubCategories);
        return allSubCategories;
      }
    })
    .catch(function (err) {
      console.log(err);
      return allSubCategories;
    });
}

/**
 * @typedef Product
 * @property {String} name -Name of the product
 * @property {String} description - Description about the product
 * @property {String} sourceUrl - URL to the details page
 * @property {string} sourceWebsite - In this case, the source website is indonetwork
 * @property {String} category - Category of the product
 * @property {String} subCategory - Sub category of the product
 * @property {Array<Number>} subCategory - Prices of the product
 * @property {String} companyId - ID of the company who has the product
 * @property {String} images - path to the product images
 * @property {Integer} minOrder - minimum order of the product
 * @property {Date} lastUpdateBySource - last product detail update by the source
 */

/**
 * This function is used to get a list of all products
 *
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProducts(subCategories) {
  return Promise.each(subCategories, function (subCategory) {
      return getProductsOnASubCategory(subCategory);
    })
    .then(function (res) {
      var flatted = _.flatten(res);
      return flatted;
    });
}

/**
 * This function is used to get a list of all products on a subcategory
 *
 * @returns {Promise<Array.<Product>}>}
 */
function getProductsOnASubCategory(subCategory) {
  var allProductList = [];
  var page = 1;
  currentSubCategory = subCategory.url;

  if (lastScrape && (subCategory.url === lastScrape.subCategory)) {
    page = lastScrape.page;
  }

  // Format URL
  var subCategoryUrl = subCategory.url;
  var parsedSubCategoryUrl = url.parse(subCategoryUrl);
  if (parsedSubCategoryUrl.protocol === null) {
    parsedSubCategoryUrl.protocol = protocol;
  }
  if (parsedSubCategoryUrl.host === null) {
    parsedSubCategoryUrl.host = host;
  }
  subCategoryUrl = url.format(parsedSubCategoryUrl);

  var getProductList = function () {
    var pageUrl = subCategoryUrl + '?page=' + page;

    // HTTP request options
    var requestProductListOptions = {
      url: pageUrl,
      method: 'GET',
      timeout: REQUEST_TIMEOUT,
      headers: requestHeaders,
      agent: false
    };

    // Perform HTTP request
    return request.makeRequest(requestProductListOptions, MAX_ATTEMPT)
      .then(function (html) {
        var body = html.body;
        if (!body) {
          return Promise.reject(null);
        } else {
          var $ = cheerio.load(body);
          var productList = [];

          $('.list-item a').each(function () {
            var product = {};
            product.name = _.trim($(this).children().first().next()
              .children().first().text());
            product.sourceWebsite = sourceWebsite;
            product.category = subCategory.category;
            product.subCategory = subCategory.name;
            product.minOrder = parseInt(_.trim($(this).children().first().next()
              .children('.hargabox').children('.colfix').children('.minorder').text()));

            var sourceUrl = $(this).attr('href').replace('?quickview=true', '');
            if (sourceUrl) {
              var parsedSourceUrl = url.parse(sourceUrl);
              if (parsedSourceUrl.protocol === null) {
                parsedSourceUrl.protocol = protocol;
              }
              product.sourceUrl = url.format(parsedSourceUrl);
            }

            if (product.name && product.sourceUrl) {
              productList.push(product);
            }
          });

          if (productList.length === 0) { // Terminate if there is no product on the last page
            return allProductList;
          } else { // Continue if there are any products on the last page
            allProductList = _.concat(allProductList, productList);

            return resultFilter.filterProducts(productList, sourceWebsite)
              .then(function (productList) {
                return getAllProductsDetails(productList);
              })
              .then(function () {
                page++;
                currentPage = page;
                return getProductList();
              });

          }
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  return getProductList();
}

/**
 * This function is used to get a details of all products
 * @param {Array.<Product>} products
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProductsDetails(products) {
  return Promise.map(products, function (product) {
    return getProductDetails(product);
  }, { concurrency: MAX_CONCURRENCY }).then(function (res) {
    return res;
  });
}

/**
 * This function is used to get additional details of a product on each products' page
 * The additional details include description, price, image, lastUpdateBySource
 * @param {Product} product
 * @returns {Promise<Product>}
 */
function getProductDetails(product) {
  console.log('Get details of ' + product.name);

  // Format the URL
  var sourceUrl = product.sourceUrl;
  var parsedSourceUrl = url.parse(sourceUrl);
  if (parsedSourceUrl.protocol === null) {
    parsedSourceUrl.protocol = protocol;
  }
  sourceUrl = url.format(parsedSourceUrl);

  // HTTP request options
  var requestProductDetailsOptions = {
    url: sourceUrl,
    method: 'GET',
    timeout: REQUEST_TIMEOUT,
    headers: requestHeaders,
    agent: false
  };

  // Perform HTTP request
  return request.makeRequest(requestProductDetailsOptions, MAX_ATTEMPT)
    .then(function (html) {
      var body = html.body;
      if (!body) {
        return product;
      }

      var $ = cheerio.load(body);

      // Get description
      product.description = _.trim($('.tab-content').text());

      // Get last update date
      var possibleLastUpdateDate = [];
      $('.productdata .listdetail dd').each(function () {
        possibleLastUpdateDate.push($(this).text());
      });

      var i = 0;
      while (i < possibleLastUpdateDate.length) {
        var text = _.trim(possibleLastUpdateDate[i]).replace(/\s/g, '');
        var isAValidDate = moment(text, 'DD/MM/YYYY', true).isValid();
        if (isAValidDate) {
          product.lastUpdateBySource = moment(text, 'DD/MM/YYYY', true).format('YYYY-MM-DD');
          break;
        }
        i++;
      }

      // console.log('lud: ' + lastUpdateDate);
      // product.lastUpdate = moment(lastUpdateDate, 'DD/MM/YYYY').format('YYYY-MM-DD');
      // console.log('plu: ' + product.lastUpdate);

      // Get image urls
      var imageUrls = [];
      $('.thumbnails img').each(function () {
        imageUrls.push($(this).attr('src'));
      });

      // Get prices
      var prices = [];
      $('.harga').each(function () {
        prices.push(parseInt($(this).text().replace(/\D/g, '')));
      });
      product.prices = prices;

      // Get and format companyUrl
      var companyUrl = $('.producttitle a').attr('href');
      if (!companyUrl) {
        return Promise.reject(new Error('No company'));
      }
      var parsedCompanyUrl = url.parse(companyUrl);
      if (parsedCompanyUrl.protocol === null) {
        parsedCompanyUrl.protocol = protocol;
      }
      companyUrl = url.format(parsedCompanyUrl);

      // Check whether or not the company has been added to database
      // Add the product's category to the category field
      // If the company has not been added, add the company first
      return companyQuery.findCompaniesBySourceUrl(companyUrl)
        .then(function (company) {
          if (!company) {
            // Add the company
            var company = {};
            company.name = _.trim($('.producttitle').text());
            company.address = _.trim($('.vendorbody p').text());
            company.sourceWebsite = sourceWebsite;
            company.sourceUrl = companyUrl;
            company.category = product.category;
            company.subCategory = product.subCategory;

            return companyScraper.getCompanyDetails(company)
              .then(function (company) {
                return company;
              });
          } else {
            return saveHelper.addCategoryAndSubCategories(company.sourceUrl, product.category, product.subCategory)
              .then(function () {
                return company;
              });
          }
        })
        .then(function (company) {
          if (company) {
            product.companyId = company._id;

            return getImages(imageUrls)
              .then(function (imagePaths) {
                product.images = imagePaths;
              })
              .then(function () {
                return saveHelper.saveProduct(product)
                  .then(function (id) {
                    if (id) {
                      console.log('Product saved:\n' + product.name);
                      return lastScrapeQuery.setLastScrape(sourceWebsite, 'product',
                        currentSubCategory, currentPage, product.sourceUrl);
                    }
                  });
              });

          }
        });
    })
    .catch(function (err) {
      console.log(err);
    });
}

/**
 * This function is used to get images of the product
 * and store the images on local computer
 * It returns path of the images
 *
 * @params {Array<String>}
 * @returns {Promise<Array<String>>}
 */
function getImages(imageUrls) {

  var downloadImage = function (imageUrl) {
    var paths = url.parse(imageUrl).pathname.split('/');
    var fileName = paths[paths.length - 1];
    var destination = imageDirectory.replace('~', process.env.HOME) + fileName;

    // Download the images
    return downloader.download(imageUrl, destination)
      .then(function (res) {
        if (res === true) {
          return imageDirectory + fileName;
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  return Promise.map(imageUrls, function (imageUrl) {
    return downloadImage(imageUrl);
  }, { concurrency: 1 })
    .then(function (res) {
      var flatted = _.flatten(res);
      return flatted;
    });

}

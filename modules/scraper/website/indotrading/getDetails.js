/**
 * This script is used to get additional details of Indotrading products which have been saved
 * The details include price, lastUpdateBySource, and brand
 * To run:
 * node module/scraper/website/indotrading/getDetails.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var Promise = require('bluebird');
var moment = require('moment');
var request = require('../../../../helpers/common/request');
var phantom = require('../../../../helpers/common/phantom');
var url = require('url');

var lastScrapeQuery = require('../../queries/lastScrape');
var productQuery = require('../../queries/product');

var sourceWebsite = 'indotrading';

const MAX_CONCURRENCY = 1;
const MAX_ATTEMPT = 5;

var productOptions = { sourceWebsite: 'indotrading' };
var sortOptions = { _id: 1 };
productQuery.findProducts(productOptions, sortOptions)
  .then(function (products) {

    lastScrapeQuery.getLastScrape(sourceWebsite, 'product-detail')
      .then(function (lastScrapeContext) {
        if (lastScrapeContext) {
          console.log('lsc tidak null');
          var lastScrape = lastScrapeContext;

          var indexOfLastScrapeProduct = _.findIndex(products, { 'sourceUrl': lastScrape.url });
          console.log(indexOfLastScrapeProduct);
          if (indexOfLastScrapeProduct !== -1) {
            products = _.drop(products, indexOfLastScrapeProduct);
          }
          console.log('Products length: ' + products.length);
        }

        return getAllProductPrice(products);
      });
  });

/**
 * This function is used to get price of all Products
 *
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProductPrice(products) {
  return Promise.each(products, function (product) {
    return getProductPrice(product);
  }, { concurrency: MAX_CONCURRENCY })
    .then(function (res) {
      var flatted = _.flatten(res);
      return flatted;
    });
}

/**
 * This function is used to get details of a product from each products' page
 * which include description, phone, and email
 * @param {Product} product
 * @returns {Promise<Product>}
 */
function getProductPrice(product) {
  console.log('Get details of ' + product.name + ' ' + product.sourceUrl);
  var sourceUrl = product.sourceUrl;
  var changed = false;

  var attempt = 0;
  var getDetails = function () {
    // Perform HTTPs request with phantom
    return phantom.makeRequest(sourceUrl, MAX_ATTEMPT)
      .then(function (html) {
        attempt++;
        if (!html) {
          return null;
        } else {
          var $ = cheerio.load(html);

          var details = [];
          $('.jm-table-desc ul li span').each(function () {
            details.push($(this).text());
          });

          if (product.prices.length === 0) {
            var prices = [];
            var i = 0;
            while (i < details.length) {
              var text = _.trim(details[i]).replace(/\s/g, '');
              var regexp = new RegExp('^rp');
              if (text.toLowerCase().match(regexp)) {
                prices.push(text.replace(/\D/g, ''));
                product.prices = prices;
                console.log(product.prices);
                changed = true;
                break;
              }
              i++;
            }

            // Most likely the last update by source is incorrect
            var i = 0;
            while (i < details.length) {
              var text = _.trim(details[i]).replace('Agu', 'Agustus');
              var isAValidDate = moment(text, 'DD MMM YYYY', 'id', true).isValid();
              if (isAValidDate) {
                product.lastUpdateBySource = moment(text, 'DD MMM YYYY', 'id').format('YYYY-MM-DD');
                console.log(product.lastUpdateBySource);
                changed = true;
                break;
              }
              i++;
            }
            console.log(product.lastUpdateBySource);
          }

          if (!product.brand) {
            var brand = _.trim($('.jm-table-desc ul li a').text());
            if (brand) {
              product.brand = brand;
              console.log(product.brand);
              changed = true;
            }
          }

          if (changed) {
            return product.save()
              .then(function () {
                return lastScrapeQuery.setLastScrape(sourceWebsite, 'product-detail', '', '', product.sourceUrl);
              });
          } else {
            return lastScrapeQuery.setLastScrape(sourceWebsite, 'product-detail', '', '', product.sourceUrl);
          }
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  return getDetails();
}

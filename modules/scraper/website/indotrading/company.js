/**
 * This script is used to get company list from Indotrading
 * To run:
 * node module/scraper/runner/indotrading/company.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var Promise = require('bluebird');
var url = require('url');

var lastScrapeQuery = require('../../queries/lastScrape');
var phantom = require('../../../../helpers/common/phantom');
var request = require('../../../../helpers/common/request');
var resultFilter = require('../../helpers/filterResult');
var saveHelper = require('../../helpers/saveResult');

var companyListUrl = 'https://www.indotrading.com/companycatalog/';
var requestPhoneUrl = 'https://www.indotrading.com/AjaxMethod.asmx/UpdateCompanyPhoneLeads';
var protocol = 'https';
var host = 'www.indotrading.com';
var sourceWebsite = 'indotrading';

const MAX_CONCURRENCY = 1;
const MAX_ATTEMPT = 5;

var lastScrape = null;
var currentSubCategory = null;
var currentPage = 0;
var priorityOnly = true;

exports.run = function () {
  lastScrapeQuery.getLastScrape(sourceWebsite, 'company')
    .then(function (lastScrapeContext) {
      if (lastScrapeContext) {
        console.log('lsc tidak null');
        lastScrape = lastScrapeContext;
      }
      return getSubCategories();
    })
    .then(function (subCategories) {

      if (lastScrape) {
        var indexOfSubCategory = _.findIndex(subCategories, { 'url': lastScrape.subCategory });
        if (lastScrape && indexOfSubCategory !== -1) {
          subCategories = _.drop(subCategories, indexOfSubCategory);
        }
      }
      console.log('subcat length: ' + subCategories.length);

      if (priorityOnly) {
        var priorities = [
          'Alat Elektronik dan Elektrik',
          'Alat Ukur dan Survey',
          'Alat Proteksi Diri dan Keamanan',
          'Kesehatan dan Medis',
          'Peralatan Kantor Sekolah dan Alat Tulis',
          'Lingkungan'
        ];

        // Filter priorities
        var prioritySubCategories = [];
        _.forEach(subCategories, function (subCategory) {
          if ((_.indexOf(priorities, subCategory.category) !== -1) || (_.indexOf(priorities, subCategory.name) !== -1)) {
            console.log(subCategory);
            prioritySubCategories.push(subCategory);
          }
        });
        console.log('priorty length: ' + prioritySubCategories.length);
        return getAllCompanies(prioritySubCategories);
      } else {
        return getAllCompanies(subCategories);
      }
    })
    .catch(function (err) {
      console.log(err);
    })
    .finally(function () {
      process.exit();
    });
};

/**
 * @typedef SubCategory
 * @property {String} name - Name of the subcategory
 * @property {String} category - The catgeory (parent)
 * @property {string} url - URL of the subcategory
 */

/**
 * This function is used to get a list of subcategories given the HTML of the page
 * containing list of subcategories
 * @param {String} html
 * @returns {Promise<Array.<SubCategory>}>}
 */
function getSubCategories() {
  var allSubCategories = [];

  return phantom.makeRequest(companyListUrl, MAX_ATTEMPT)
    .then(function (html) {
      if (!html) {
        return Promise.reject(null);
      } else {
        var $ = cheerio.load(html);

        $('#cphBody_CatWWW_rptCategory tbody tr td').each(function () {
          var category = _.trim($(this).children('.idt-head-catalog').text());
          var categoryUrl = $(this).children('.idt-head-catalog').children('a').attr('href');

          // Remove count on subcategory name
          var lastIndexOfSpace = category.lastIndexOf(' ');
          if (lastIndexOfSpace !== -1) {
            category = category.substr(0, lastIndexOfSpace);
          }

          if (!_.isEmpty(category) && _.indexOf(priorities, categoryUrl) !== -1) {
            var $subCategories = $(this).children('.companyCategory').children('li').children('a');
            var subCategories =  _.reduce($subCategories, function (tempSubCategories, current) {

              var subCategory = {};
              subCategory.url = current.attribs.href;
              subCategory.category = category;

              // Remove count on subcategory name
              var name = _.trim(current.children[0].data);
              lastIndexOfSpace = name.lastIndexOf(' ');
              if (lastIndexOfSpace !== -1) {
                name = name.substr(0, lastIndexOfSpace);
              }
              subCategory.name = name;

              tempSubCategories.push(subCategory);
              return tempSubCategories;
            }, []);
            allSubCategories.push(subCategories);
          }

        });
        allSubCategories = _.flattenDeep(allSubCategories);
        return allSubCategories;
      }
    })
    .catch(function (err) {
      console.log(err);
    });
}

/**
 * @typedef Company
 * @property {String} name - The company's name
 * @property {String} sourceUrl - URL to the details page
 * @property {string} address - The company's address
 * @property {string} category - The company's address
 * @property {string} subCategory - The company's address
 * @property {string} sourceWebsite - In this case, the source website is indotrading
 * @property {string} description - Description of the company
 * @property {string} phone - The company's phone numbers
 */

/**
 * This function is used to get a list of all companies
 *
 * @returns {Promise<Array.<Company>}>}
 */
function getAllCompanies(subCategories) {
  return Promise.each(subCategories, function (subCategory) {
    return getCompaniesOnASubCategory(subCategory);
  }, { concurrency: MAX_CONCURRENCY })
    .then(function (res) {
      return _.flatten(res);
    });
}

/**
 * This function is used to get a list of all companies on a subcategory
 *
 * @returns {Promise<Array.<Company>}>}
 */
function getCompaniesOnASubCategory(subCategory) {
  var companyList = [];
  var page = 1;
  currentSubCategory = subCategory.url;

  // Format URL
  var subCategoryUrl = subCategory.url;
  var parsedSubCategoryUrl = url.parse(subCategoryUrl);
  if (parsedSubCategoryUrl.protocol === null) {
    parsedSubCategoryUrl.protocol = protocol;
  }
  if (parsedSubCategoryUrl.host === null) {
    parsedSubCategoryUrl.host = host;
  }
  subCategoryUrl = url.format(parsedSubCategoryUrl);

  var getCompanyList = function () {
    var subCategoryUrlWithPage = subCategoryUrl + page;

    // Perform HTTPS request with phantom
    return phantom.makeRequest(subCategoryUrlWithPage, MAX_ATTEMPT)
      .then(function (html) {
        if (!html) {
          return Promise.reject(new Error('Can\'t load page: ' + subCategoryUrlWithPage));
        }
        var companiesListKeyBySourceUrl = _.keyBy(companyList, 'sourceUrl');
        var companies = getCompanies(html, subCategory);

        // Check each companies on the current page whether or not it has appeared on the previous pages
        // Because  the same company may appear again on the same sub categories listing
        // Not sure if it guarantees to get all companies
        // But if we don't use it, sometimes we may have to check more than 1000 pages on a sub category
        // only to get the same companies
        // I'll check it later
        companies = _.remove(companies, function (company) {
          return (!company) || (!(companiesListKeyBySourceUrl[company.sourceUrl]));
        });

        if (companies.length === 0) { // Terminate if there is no company on the last page
          return companyList;
        } else { // Continue if there are any companies on the last page
          companyList = _.concat(companyList, companies);
          console.log('before filter: ' + companyList.length);
          return resultFilter.filterCompanies(companyList, sourceWebsite)
            .then(function (companyList) {
              console.log('after filter: ' + companyList.length);
              return getAllCompaniesDetails(productList);
            })
            .then(function () {
              page++;
              currentPage = page;
              return getCompanyList();
            });
        }
      });
  };

  return getCompanyList()
    .catch(function (err) {
      console.log(err);
    });
}

/**
 * This function is used to get list of companies on a page given the HTML
 * @param {String} html
 * @returns {Promise<Array.<Company>}>}
 */
function getCompanies(html, subCategory) {
  var companies = [];
  var $ = cheerio.load(html);
  $('.warp-companylist1 .nopad-right').each(function () {
    var name = $(this).children('.clearfix').children('.nopad')
      .children('.pull-left').children('a').text();
    var sourceUrl = $(this).children('.clearfix').children('.nopad')
      .children('.pull-left').children('a').attr('href');
    var address = $(this).children('.three-line').text();

    var company = {};
    company.name = _.trim(name);
    company.sourceUrl = sourceUrl;
    company.address = _.trim(address);
    company.subCategory = subCategory.name;
    company.category = subCategory.category;
    company.sourceWebsite = sourceWebsite;
    if (company.name && company.sourceUrl) {
      companies.push(company);
    }
  });
  return companies;
}

/**
 * This function is used to get details of all companies
 * @param {Array.<Company>} companies
 * @returns {Promise<Array.<Company>}>}
 */
function getAllCompaniesDetails(companies) {
  return Promise.map(companies, function (company) {
    return this.getCompanyDetails(company);
  }, { concurrency: MAX_CONCURRENCY }).then(function (res) {
    return res;
  });
}

/**
 * This function is used to get details of a company from each companies' page
 * which include description, phone, and email
 * @param {Company} company
 * @returns {Promise<Company>}
 */
exports.getCompanyDetails = function (company) {
  console.log('Get details of ' + company.name);

  // Format the URL
  var sourceUrl = company.sourceUrl;
  var parsedSourceUrl = url.parse(sourceUrl);
  if (parsedSourceUrl.protocol === null) {
    parsedSourceUrl.protocol = protocol;
  }
  if (parsedSourceUrl.host === null) {
    parsedSourceUrl.host = host;
  }
  sourceUrl = url.format(parsedSourceUrl);

  var attempt = 0;
  var getDetails = function () {
    // Perform HTTPS request with phantom
    return phantom.makeRequest(sourceUrl, MAX_ATTEMPT)
      .then(function (html) {
        attempt++;
        if (!html) {
          company.description = '';
          company.phones = '';
          return null;
        } else {
          var $ = cheerio.load(html);
          company.description = _.trim($('#cphBody_idDivProfile2').text());
          var token = $('#hdToken').attr('value');

          // The link to product image contains company ID
          // which is need for requesting phone numbers
          var productImg = $('.tab-content img').first().attr('src');
          var companyId = getCompanyId(productImg);

          // If there is no product images on the page
          // Try to get company ID from company logo image
          if (!companyId) {
            var companyLogoImg = $('.logo-img-servis img').first().attr('src');
            companyId = getCompanyId(companyLogoImg);
          }

          if (!companyId) {
            if (attempt < MAX_ATTEMPT) {
              return getDetails();
            }
            return null;
          }

          if (token && companyId && sourceUrl) {
            return getPhone(token, companyId, sourceUrl)
              .then(function (res) {
                company.phones = res;
              })
              .catch(function (err) {
                console.log(err);

                // Retry if it can't get the phone because of Invalid Token
                // It needs to refresh the entire page to get a new token
                return getDetails();
              });
          } else {
            return null;
          }
        }
      });
  };

  return getDetails()
    .then(function () {
      // Save the company to db;
      return saveHelper.saveCompany(company)
        .then(function (id) {
          if (id) {
            console.log('Company saved:\n' + company);
            return lastScrapeQuery.setLastScrape(sourceWebsite, 'company',
              currentSubCategory, currentPage, company.sourceUrl);
          }
          return company;
        });
    })
    .catch(function (err) {
      console.log(err);
    });
};

/**
 * This function is used to extract companyID given an imagePath
 * The imagePath usually contains companyID
 * On the pathnames, it will try to find a path matching the regex
 * @param {String} imagePath
 * @returns {int}
 */
function getCompanyId(imagePath) {
  var companyId = null;
  var companyIdRegex = /^co\d+$/i;
  var productImagePathname = imagePath ?
    (url.parse(imagePath).pathname).split('/') : [];

  var i = 0;
  while (i < productImagePathname.length) {
    var path = productImagePathname[i];
    if (path.match(companyIdRegex)) {
      companyId = 'C' + path.substring(1, path.length);
      break;
    }
    i++;
  }

  return companyId;
}

/**
 * This function is used to get phone numbers of a company
 * It makes HTTP request with a json object containing token and companyId
 * @param {String} token
 * @param {String} companyId
 * @param {String} sourceUrl
 * @returns {Promise<Object>}
 */
function getPhone(token, companyId, sourceUrl) {

  // Request options
  var options = {
    url: requestPhoneUrl,
    headers: {
      'jar': false,
      'Referrer': sourceUrl
    },
    body: {
      'Token': token,
      'EncCompanyID': companyId,
      'ProductID': ''
    },
    json: true,
    method: 'POST'
  };

  // Perform HTTPS request
  return request.makeRequest(options, MAX_ATTEMPT)
    .then(function (res) {

      var response = res.body.d;
      if (response === 'Invalid Token') {
        return Promise.reject(response);
      }

      var phonesJson = JSON.parse(response);

      // There can be more than one phone numbers, stored on the following keys
      var phoneKeys = [
        'Phone',
        'Phone2',
        'Phone3',
        'Phone4',
        'Phone5',
        'MobilePhone',
        'MobilePhone2',
        'MobilePhone3',
        'Fax'
      ];

      var phones = [];
      _.forEach(phoneKeys, function (phoneKey) {
        var phone = phonesJson[phoneKey];
        if (phone) {
          phones.push(phone);
        }
      });

      return phones;
    })
    .catch(function (err) {
      if (err === 'Invalid Token') { // Sometimes it returns invalid token
        return Promise.reject(err);
      }
      return [];
    });
}

/**
 * This script is used to get product list from Indotrading
 * To run:
 * node module/scraper/runner/indotrading/product.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var Promise = require('bluebird');
var moment = require('moment');
var url = require('url');

var companyQuery = require('../../queries/company');
var companyScraper = require('./company');
var downloader = require('../../../../helpers/file/downloader');
var lastScrapeQuery = require('../../queries/lastScrape');
var phantom = require('../../../../helpers/common/phantom');
var request = require('../../../../helpers/common/request');
var resultFilter = require('../../helpers/filterResult');
var saveHelper = require('../../helpers/saveResult');

var productListUrl = 'https://www.indotrading.com/productcatalog/';
var protocol = 'https';
var host = 'www.indotrading.com';
var sourceWebsite = 'indotrading';
var imageDirectory = '~/images/indotrading/products/';

const MAX_CONCURRENCY = 1;
const MAX_ATTEMPT = 5;

var lastScrape = null;
var currentSubCategory = null;
var currentPage = 0;
var priorityOnly = true;

exports.run = function () {
  lastScrapeQuery.getLastScrape(sourceWebsite, 'product')
    .then(function (lastScrapeContext) {
      if (lastScrapeContext) {
        console.log('lsc tidak null');
        lastScrape = lastScrapeContext;
      }
      return getSubCategories();
    })
    .then(function (subCategories) {
      if (lastScrape) {
        var indexOfSubCategory = _.findIndex(subCategories, {'url': lastScrape.subCategory});
        if (lastScrape && indexOfSubCategory !== -1) {
          subCategories = _.drop(subCategories, indexOfSubCategory);
        }
      }
      console.log('subcat length: ' + subCategories.length);

      if (priorityOnly) {
        var priorities = [
          'Alat Elektronik dan Elektrik',
          'Alat Ukur dan Survey',
          'Alat Proteksi Diri dan Keamanan',
          'Kesehatan dan Medis',
          'Peralatan Kantor Sekolah dan Alat Tulis',
          'Lingkungan'
        ];

        // Filter priorities
        var prioritySubCategories = [];
        _.forEach(subCategories, function (subCategory) {
          if ((_.indexOf(priorities, subCategory.category) !== -1) || (_.indexOf(priorities, subCategory.name) !== -1)) {
            console.log(subCategory);
            prioritySubCategories.push(subCategory);
          }
        });
        console.log('priorty length: ' + prioritySubCategories.length);
        return getAllProducts(prioritySubCategories);
      } else {
        return getAllProducts(subCategories);
      }
    })
    .catch(function (err) {
      console.log(err);
    })
    .then(function () {
      process.exit();
    });
};

/**
 * @typedef SubCategory
 * @property {String} name - Name of the subcategory
 * @property {String} category - The catgeory (parent)
 * @property {string} url - URL of the subcategory
 */

/**
 * This function is used to get a list of subcategories
 * @returns {Promise<Array.<SubCategory>}>}
 */
function getSubCategories() {
  var allSubCategories = [];

  return phantom.makeRequest(productListUrl, MAX_ATTEMPT)
    .then(function (html) {
      if (!html) {
        return Promise.reject(null);
      } else {
        var $ = cheerio.load(html);

        $('#rptCategory tbody tr td').each(function () {
          var category = _.trim($(this).children('.idt-head-catalog').text());
          var categoryUrl = $(this).children('.idt-head-catalog').children('a').attr('href');

          // Remove count on subcategory name
          var lastIndexOfSpace = category.lastIndexOf(' ');
          if (lastIndexOfSpace !== -1) {
            category = category.substr(0, lastIndexOfSpace);
          }

          if (!_.isEmpty(category)) {
            var $subCategories = $(this).children('.companyProduct').children('li').children('a');

            var subCategories =  _.reduce($subCategories, function (tempSubCategories, current) {
              var subCategory = {};
              subCategory.url = current.attribs.href;
              subCategory.category = category;

              // Remove count on subcategory name
              var name = _.trim(current.children[0].data);
              lastIndexOfSpace = name.lastIndexOf(' ');
              if (lastIndexOfSpace !== -1) {
                name = name.substr(0, lastIndexOfSpace);
              }
              subCategory.name = name;

              tempSubCategories.push(subCategory);
              return tempSubCategories;
            }, []);

            allSubCategories.push(subCategories);
          }

        });
        allSubCategories = _.flattenDeep(allSubCategories);
        return allSubCategories;
      }
    })
    .catch(function (err) {
      console.log(err);
    });
}

/**
 * @typedef Product
 * @property {String} name -Name of the product
 * @property {String} description - Description about the product
 * @property {String} sourceUrl - URL to the details page
 * @property {string} sourceWebsite - In this case, the source website is indonetwork
 * @property {String} category - Category of the product
 * @property {String} subCategory - Sub category of the product
 * @property {Array<Number>} subCategory - Prices of the product
 * @property {String} companyId - ID of the company who has the product
 * @property {String} images - path to the product images
 * @property {Integer} minOrder - minimum order of the product
 * @property {Date} lastUpdateBySource - last product detail update by the source
 */

/**
 * This function is used to get a list of all products
 *
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProducts(subCategories) {
  return Promise.each(subCategories, function (subCategory) {
    return getProductsOnASubCategory(subCategory);
  }, { concurrency: MAX_CONCURRENCY })
    .then(function (res) {
      return _.flatten(res);
    });
}

/**
 * This function is used to get a list of all products on a subcategory
 *
 * @returns {Promise<Array.<Product>}>}
 */
function getProductsOnASubCategory(subCategory) {
  var allProductsList = [];
  var page = 1;
  currentSubCategory = subCategory.url;

  if (lastScrape && (subCategory.url === lastScrape.subCategory)) {
    page = lastScrape.page;
  }

  // Format URL
  var subCategoryUrl = subCategory.url;
  var parsedSubCategoryUrl = url.parse(subCategoryUrl);
  if (parsedSubCategoryUrl.protocol === null) {
    parsedSubCategoryUrl.protocol = protocol;
  }
  if (parsedSubCategoryUrl.host === null) {
    parsedSubCategoryUrl.host = host;
  }
  subCategoryUrl = url.format(parsedSubCategoryUrl);

  var getProductsList = function () {
    var subCategoryUrlWithPage = subCategoryUrl + page;

    // Perform HTTPS request with phantom
    return phantom.makeRequest(subCategoryUrlWithPage, MAX_ATTEMPT)
      .then(function (html) {
        if (!html) {
          return Promise.reject(new Error('Can\'t load page: ' + subCategoryUrlWithPage));
        }
        var productsListKeyBySourceUrl = _.keyBy(allProductsList, 'sourceUrl');
        var productList = getProducts(html, subCategory);

        // Check each products on the current page whether or not it has appeared on the previous pages
        // Because  the same product may appear again on the same sub categories listing
        // Not sure if it guarantees to get all products
        // But if we don't use it, sometimes we may have to check more than 1000 pages on a sub category
        // only to get the same products
        // I'll check it later
        productList = _.remove(productList, function (product) {
          return (!product) || (!(productsListKeyBySourceUrl[product.sourceUrl]));
        });

        if (productList.length === 0) { // Terminate if there is no product on the last page
          return allProductsList;
        } else { // Continue if there are any products on the last page
          allProductsList = _.concat(allProductsList, productList);
          console.log('before filter: ' + productList.length);
          return resultFilter.filterProducts(productList, sourceWebsite)
            .then(function (productList) {
              console.log('after filter: ' + productList.length);
              return getAllProductsDetails(productList);
            })
            .then(function () {
              page++;
              currentPage = page;
              return getProductsList();
            });
        }
      });
  };

  return getProductsList()
    .catch(function (err) {
      console.log(err);
    });
}

/**
 * This function is used to get list of products on a page given the HTML
 * @param {String} html
 * @returns {Promise<Array.<Company>}>}
 */
function getProducts(html, subCategory) {
  var products = [];
  var $ = cheerio.load(html);
  $('.productlist1 .nopad-right').each(function () {
    var name = $(this).children('.clearfix').children('.nopad')
      .children('.two-lines-elipsis').text();
    var sourceUrl = $(this).children('.clearfix').children('.nopad')
      .children('.two-lines-elipsis').children('span').children('a').attr('href');

    var product = {};
    product.name = _.trim(name);
    product.sourceUrl = sourceUrl;
    product.subCategory = subCategory.name;
    product.category = subCategory.category;
    product.sourceWebsite = sourceWebsite;
    if (product.name && product.sourceUrl) {
      products.push(product);
    }
  });
  return products;
}

/**
 * This function is used to get details of all products
 * @param {Array.<Product>} products
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProductsDetails(products) {
  return Promise.map(products, function (product) {
    return getProductDetails(product);
  }, { concurrency: MAX_CONCURRENCY }).then(function (res) {
    return res;
  });
}

/**
 * This function is used to get details of a product from each products' page
 * which include description, phone, and email
 * @param {Product} product
 * @returns {Promise<Product>}
 */
function getProductDetails(product) {
  console.log('Get details of ' + product.name);

  // Format the URL
  var sourceUrl = product.sourceUrl;
  var parsedSourceUrl = url.parse(sourceUrl);
  if (parsedSourceUrl.protocol === null) {
    parsedSourceUrl.protocol = protocol;
  }
  if (parsedSourceUrl.host === null) {
    parsedSourceUrl.host = host;
  }
  sourceUrl = url.format(parsedSourceUrl);

  var attempt = 0;
  var getDetails = function () {
    // Perform HTTPs request with phantom
    return phantom.makeRequest(sourceUrl, MAX_ATTEMPT)
      .then(function (html) {
        attempt++;
        if (!html) {
          return null;
        } else {
          var $ = cheerio.load(html);

          // Get image urls
          var imageUrls = [];
          $('.jm-product-preview-new img').each(function () {
            imageUrls.push($(this).attr('src').replace('w200-h200', 'w500-h500'));
          });

          // Get description, minOrder, and brand
          product.description = _.trim($('.jm-wrap-content-tab-product1').text());
          var minOrderText = _.trim($('.jm-table-desc ul li span').first().text());
          product.minOrder = _.isInteger(minOrderText) ? parseInt(minOrderText) : undefined;
          product.brand = _.trim($('.jm-table-desc ul li').first().next().next().next().next().children('span').text());

          // Get last update date
          // var lastUpdateDate = _.trim($('.jm-table-desc ul li').first().next().next().next().children('span').text())
          //   .replace('Agu', 'Agustus');
          // var isAValidDate = moment(lastUpdateDate, 'DD MMM YYYY', 'id').isValid();
          // if (isAValidDate) {
          //   product.lastUpdateBySource = moment(lastUpdateDate, 'DD MMM YYYY', 'id').format('YYYY-MM-DD');
          // }

          var details = [];
          $('.jm-table-desc ul li span').each(function () {
            details.push($(this).text());
          });

          var i = 0;
          while (i < details.length) {
            var text = _.trim(details[i]).replace('Agu', 'Agustus');
            var isAValidDate = moment(text, 'DD MMM YYYY', 'id').isValid();
            if (isAValidDate) {
              product.lastUpdateBySource = moment(text, 'DD MMM YYYY', 'id').format('YYYY-MM-DD');
              break;
            }
            i++;
          }
          console.log(product.lastUpdateBySource);

          var prices = [];
          var i = 0;
          while (i < details.length) {
            var text = _.trim(details[i]).replace(/\s/g, '');
            var regexp = new RegExp('^rp');
            if (text.toLowerCase().match(regexp)) {
              prices.push(text.replace(/\D/g, ''));
              product.prices = prices;
              break;
            }
            i++;
          }
          console.log(product.prices);

          // Get and format companyUrl
          var companyUrl = $('#cphBody_ctl00_company_info .two-lines-elipsis a').attr('href');
          if (!companyUrl) {
            return Promise.reject(new Error('No company'));
          }
          var parsedCompanyUrl = url.parse(companyUrl);
          if (parsedCompanyUrl.protocol === null) {
            parsedCompanyUrl.protocol = protocol;
          }
          companyUrl = url.format(parsedCompanyUrl);

          // Check whether or not the company has been added to database
          // Add the product's category to the category field
          // If the company has not been added, add the company first
          console.log('companyUrl: ' + companyUrl);
          return companyQuery.findCompaniesBySourceUrl(companyUrl)
            .then(function (company) {
              if (!company) {

                // Add the company
                var company = {};
                company.name = _.trim($('#cphBody_ctl00_company_info .two-lines-elipsis').text());
                company.address = _.trim($('.custom-media-list-product').first().next().text())
                  .replace('Alamat', '');
                company.sourceWebsite = sourceWebsite;
                company.sourceUrl = companyUrl;
                company.category = _.trim($('.btn-breadcrumb li').first().next().text());
                company.subCategory = _.trim($('.btn-breadcrumb li').first().next().next().text());

                return companyScraper.getCompanyDetails(company)
                  .then(function (company) {
                    return company;
                  });
              } else {
                return saveHelper.addCategoryAndSubCategories(company.sourceUrl, product.category, product.subCategory)
                  .then(function () {
                    return company;
                  });
              }
            })
            .then(function (company) {
              if (company) {
                product.companyId = company._id;
                return getImages(imageUrls)
                  .then(function (imagePaths) {
                    product.images = imagePaths;
                  })
                  .then(function () {
                    return saveHelper.saveProduct(product)
                      .then(function (id) {
                        if (id) {
                          console.log('Product saved:\n' + product.name);
                          return lastScrapeQuery.setLastScrape(sourceWebsite, 'product',
                            currentSubCategory, currentPage, product.sourceUrl);
                        }
                      });
                  });
              }
            });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  return getDetails();
}

/**
 * This function is used to get images of the product
 * and store the images on local computer
 * It returns path of the images
 *
 * @params {Array<String>}
 * @returns {Promise<Array<String>>}
 */
function getImages(imageUrls) {

  var downloadImage = function (imageUrl) {
    var paths = url.parse(imageUrl).pathname.split('/');
    var fileName = paths[paths.length - 1];
    var destination = imageDirectory.replace('~', process.env.HOME) + fileName;

    // Download the images
    return downloader.download(imageUrl, destination)
      .then(function (res) {
        if (res === true) {
          return imageDirectory + fileName;
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  return Promise.map(imageUrls, function (imageUrl) {
    return downloadImage(imageUrl);
  }, { concurrency: 1 })
    .then(function (res) {
      var flatted = _.flatten(res);
      return flatted;
    });

}

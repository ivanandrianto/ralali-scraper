var AlibabaProduct = require('../models/alibabaProduct');

exports.saveProduct = function (product) {
  return product.save()
    .then(function (product) {
      return product._id;
    })
    .catch(function (err) {
      console.log(err);
    });
};

exports.getProduct = function (options) {
  return AlibabaProduct.find(options)
    .catch(function (err) {
      console.log(err);
    });
};

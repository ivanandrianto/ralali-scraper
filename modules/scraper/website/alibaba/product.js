/**
 * This script is used to get product list from Alibaba
 * To run:
 * node module/scraper/website/alibaba/product.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var Promise = require('bluebird');
var moment = require('moment');
var request = require('../../../../helpers/common/request');
var phantom = require('../../../../helpers/common/phantom');
var url = require('url');

var downloader = require('../../../../helpers/file/downloader');

var homeUrl = 'http://www.alibaba.com/';
var protocol = 'http';
var host = 'www.alibaba.com';
var sourceWebsite = 'alibaba';
var imageDirectory = '~/images/alibaba/products/';

const MAX_CONCURRENCY = 1;
const MAX_ATTEMPT = 5;

phantom.makeRequest(homeUrl, MAX_ATTEMPT)
  .then(function (html) {
    var categories = getCategories(html);
    return getSubCategories(categories);
  })
  .then(function (subCategories) {
    return getAllProducts(subCategories);
  })
  .then(function (products) {
    return getAllProductsDetails(products);
  });

/**
 * @typedef Category
 * @property {String} name - Name of the category
 * @property {string} url - URL of the category
 */

/**
 * @typedef SubCategory
 * @property {String} name - Name of the subcategory
 * @property {String} category - The catgeory (parent)
 * @property {string} url - URL of the subcategory
 */

/**
 * This function is used to get a list of subcategories given the HTML of the page
 * containing list of subcategories
 * @param {String} html
 * @returns {Promise<Array.<SubCategory>}>}
 */
function getCategories(html) {
  var $ = cheerio.load(html);
  var categories = [];

  $('.menu-tree-trigger').each(function () {
    var categoryName = _.trim($(this).children('.tree-name-warp').text());
    var categoryUrl = $(this).children('.tree-name-warp').children('a').attr('href');

    if (!_.isEmpty(categoryName) && !_.isEmpty(categoryUrl)) {
      var category = { name: categoryName, url: categoryUrl };
      categories.push(category);
    }

  });
  return categories;
}

/**
 * This function is used to get a list of subcategories given from each categories
 * @param {Array<Category>} categories
 * @returns {Promise<Array.<SubCategory>}>}
 */
function getSubCategories(categories) {
  return Promise.map(categories, function (category) {
    return getSubCategoriesOnASubCategory(category);
  }, { concurrency: MAX_CONCURRENCY })
    .then(function (res) {
      var flatted = _.flatten(res);
      return flatted;
    });
}

/**
 * This function is used to get a list of subcategories from a category
 * containing list of subcategories
 * @param {Category} category
 * @returns {Promise<Array.<SubCategory>}>}
 */
function getSubCategoriesOnASubCategory(category) {
  // Format URL
  var categoryUrl = category.url;
  var parsedCategoryUrl = url.parse(categoryUrl);
  if (parsedCategoryUrl.protocol === null) {
    parsedCategoryUrl.protocol = protocol;
  }
  if (parsedCategoryUrl.host === null) {
    parsedCategoryUrl.host = host;
  }
  categoryUrl = url.format(parsedCategoryUrl);

  // Perform HTTP request
  return phantom.makeRequest(categoryUrl, MAX_ATTEMPT)
    .then(function (html) {
      var $ = cheerio.load(html);
      var subCategories = [];

      $('.item .title a').each(function () {
        var subCategoryName = _.trim($(this).text());
        var subCategoryUrl = $(this).attr('href');

        if (!_.isEmpty(subCategoryName) && !_.isEmpty(categoryUrl)) {
          var subCategory = { name: subCategoryName, category: category.name, url: subCategoryUrl };
          subCategories.push(subCategory);
        }

      });
      return subCategories;
    });

}

/**
 * This function is used to get a list of all products from each sub categories
 * @param {Array<SubCategory>} subCategories
 * @returns {Promise<Array.<Product>>}
 */
function getAllProducts(subCategories) {
  return Promise.map(subCategories, function (subCategory) {
    return getProductsOnASubCategory(subCategory);
  }, { concurrency: MAX_CONCURRENCY })
    .then(function (res) {
      var flatted = _.flatten(res);
      return flatted;
    });
}

/**
 * This function is used to get a list of all products on a subcategory
 * @param {SubCategory} subCategory
 * @returns {Promise<Array.<Product>>>}
 */
function getProductsOnASubCategory(subCategory) {
  var allProductsList = [];
  var page = 1;

  // Format URL
  var subCategoryUrl = subCategory.url;
  var parsedSubCategoryUrl = url.parse(subCategoryUrl);
  if (parsedSubCategoryUrl.protocol === null) {
    parsedSubCategoryUrl.protocol = protocol;
  }
  subCategoryUrl = url.format(parsedSubCategoryUrl);

  var getProductsList = function () {
    var pageUrl = subCategoryUrl + '/' + page;

    // Perform HTTP request
    return phantom.makeRequest(pageUrl, MAX_ATTEMPT)
      .then(function (html) {
        if (!html) {
          return Promise.reject(null);
        } else {
          var $ = cheerio.load(html);
          var productList = [];

          $('.m-product-item .col-main').each(function () {
            var product = {};
            product.name = _.trim($(this).children('.main-wrap').children('.title').text());
            product.sourceWebsite = sourceWebsite;
            product.category = subCategory.category;
            product.subCategory = subCategory.name;
            product.price = _.trim($(this).children('.main-wrap').children('.pmo')
              .children('.price').text());
            product.minOrder = parseInt($(this).children('.main-wrap').children('.pmo')
              .children('.min-order').children('b').text().replace('Piece', ''));

            var sourceUrl = _.trim($(this).children('.main-wrap').children('.title').children('a').attr('href'));
            if (sourceUrl) {
              var parsedSourceUrl = url.parse(sourceUrl);
              if (parsedSourceUrl.protocol === null) {
                parsedSourceUrl.protocol = protocol;
              }
              product.sourceUrl = url.format(parsedSourceUrl);
            }

            if (product.name && product.sourceUrl) {
              productList.push(product);
            }
          });

          if (productList.length === 0 | page > 2) { // Terminate if there is no product on the last page
            return allProductsList;
          } else { // Continue if there are any products on the last page
            allProductsList = _.concat(allProductsList, productList);
            page++;
            return getProductsList();
          }
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  return getProductsList();
}

/**
 * This function is used to get details of all products
 * @param {Array.<Product>} products
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProductsDetails(products) {
  return Promise.map(products, function (product) {
    return getProductDetails(product);
  }, { concurrency: MAX_CONCURRENCY }).then(function (res) {
    return res;
  });
}

/**
 * This function is used to get details of a product from each products' page
 * which include description, phone, and email
 * @param {Product} product
 * @returns {Promise<Product>}
 */
function getProductDetails(product) {
  console.log('Get details of ' + product.name);

  // Format the URL
  var sourceUrl = product.sourceUrl;
  var parsedSourceUrl = url.parse(sourceUrl);
  if (parsedSourceUrl.protocol === null) {
    parsedSourceUrl.protocol = protocol;
  }
  sourceUrl = url.format(parsedSourceUrl);

  // Perform HTTP request
  return phantom.makeRequest(sourceUrl, MAX_ATTEMPT)
    .then(function (html) {
      if (!html) {
        return product;
      }

      var $ = cheerio.load(html);

      // Get image urls
      var imageUrls = [];
      $('.item img').each(function () {
        imageUrls.push($(this).attr('src').replace('.png_50x50', ''));
      });

      // Get product description
      product.description = _.trim($('#J-product-detail').text());

    })
    .catch(function (err) {
      console.log(err);
    });
}

/**
 * This function is used to get images of the product
 * and store the images on local computer
 * It returns path of the images
 *
 * @params {Array<String>}
 * @returns {Promise<Array<String>>}
 */
function getImages(imageUrls) {

  var downloadImage = function (imageUrl) {
    var paths = url.parse(imageUrl).pathname.split('/');
    var fileName = paths[paths.length - 1];
    var destination = imageDirectory.replace('~', process.env.HOME) + fileName;

    // Download the images
    return downloader.download(imageUrl, destination)
      .then(function (res) {
        if (res === true) {
          return imageDirectory + fileName;
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  return Promise.map(imageUrls, function (imageUrl) {
    return downloadImage(imageUrl);
  }, { concurrency: 1 })
    .then(function (res) {
      var flatted = _.flatten(res);
      return flatted;
    });

}

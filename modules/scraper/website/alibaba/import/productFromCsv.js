/**
 * This script is used to get import Alibaba scraper data in CSV to database
 * To run:
 * node modules/scraper/website/alibaba/import/productFromCsv.js
 * @author: ivan
 */

'use strict';

var csv = require('csv-parser');
var fs = require('fs');
var AlibabaProduct = require('../models/alibabaProduct');
var AlibabaProductQuery = require('../queries/alibabaProduct');
var _ = require('lodash');
var Promise = require('bluebird');
var prompt = require('prompt');

prompt.start();
prompt.get(['category', 'subCategory', 'csvPath'], function (err, input) {
  return readAndSaveProducts(input.category, input.subCategory, input.csvPath);
});

function readAndSaveProducts(category, subCategory, csvPath) {
  var products = [];

  return new Promise(function (resolve) {
    fs.createReadStream(csvPath)
      .pipe(csv())
      .on('data', function (data) {

        // Get price
        if (data.price) {
          var priceText = _.trim(data.price);
          var firstDigitIndex = priceText.search(/\d/);
          var currency = priceText.substr(0, firstDigitIndex);
          priceText = priceText.substr(firstDigitIndex, firstDigitIndex.length);
          var prices = priceText.split('-');
          var minPriceStr = prices[0];
          if (!isNaN(minPriceStr)) {
            var minPrice = minPriceStr;
          }
          if (prices.length > 1) {
            var maxPriceStr = prices[1];
            if (!isNaN(maxPriceStr)) {
              var maxPrice = maxPriceStr;
            }
          } else {
            var maxPrice = minPrice ? minPrice : undefined;
          }
        }

        // Get price unit
        if (data.price_unit) {
          var priceUnitText = _.trim(data.price_unit);
          var priceUnitTextSplitBySlash = priceUnitText.split('/');
          if (priceUnitTextSplitBySlash.length > 1) {
            var priceUnit = _.trim(priceUnitTextSplitBySlash[1]);
            var priceUnitIndexOfSpace = priceUnit.indexOf(' ');
            if (priceUnitIndexOfSpace !== -1) {
              priceUnit = priceUnit.substr(0, priceUnitIndexOfSpace);
            }
          }
        }

        // Get min order
        if (data['min-order']) {
          var minOrderText = _.trim(data['min-order']);
          var minOrderTextSplitBySpace = minOrderText.split(' ');
          var minOrderTextQty = minOrderTextSplitBySpace[0];
          if (!isNaN(minOrderTextQty)) {
            var minOrderQty = minOrderTextQty;
            console.log(minOrderQty);
          }
          if (minOrderTextSplitBySpace.length > 1) {
            var minOrderUnit = minOrderTextSplitBySpace[1];
          }
        }

        var productMoongoseObj = new AlibabaProduct({
          title: data.title,
          category: category,
          subCategory: subCategory,
          minPrice: minPrice ? minPrice : undefined,
          maxPrice: maxPrice ? maxPrice : undefined,
          priceUnit: priceUnit ? priceUnit : undefined,
          priceCurrency: currency,
          minOrderQuantity: minOrderQty ? minOrderQty : undefined,
          minOrderUnit: minOrderUnit ? minOrderUnit : undefined,
          estProfit: data.est_profit,
          seller: data.seller,
          sellerUrl: data.seller_url,
          productUrl: data.product_url,
          imageUrl: data.image_url,
          imageAlt: data.image_alt,
          properties: data.properties
        });
        return AlibabaProductQuery.saveProduct(productMoongoseObj)
          .then(function (res) {
            console.log(res);
          })
          .catch(function (err) {
            console.log(err);
          });
      })
      .on('end', function () {
        console.log('resolved');
        resolve(products);
      });
    });
}

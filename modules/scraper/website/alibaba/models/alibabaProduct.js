var mongoose = require('mongoose');
var dbHelper = require('../../../../../helpers/common/database');
dbHelper.connect(dbHelper.databases.main);
var Schema = mongoose.Schema;

var AlibabaProductSchema = new Schema({
    title: { type: String, required: true },
    category: { type: String, required: true },
    subCategory: { type: String, required: true },
    priceCurrency: { type: String, required: false },
    minPrice: { type: Number, required: false },
    maxPrice: { type: Number, required: false },
    priceUnit: { type: String },
    minOrderQuantity: { type: Number, required: false },
    minOrderUnit: { type: String, required: false },
    estProfit: { type: String, required: false },
    seller: { type: String, required: false },
    productUrl: { type: String, required: true, unique: true },
    imageUrl: { type: String, required: false },
    imageAlt: { type: String, required: false },
    properties: { type: String, required: false },
  },
  {
    timestamps: true
  },
  {
    collection: 'alibabaProducts'
  });

var AlibabaProduct = mongoose.model('AlibabaProduct', AlibabaProductSchema);

module.exports = AlibabaProduct;

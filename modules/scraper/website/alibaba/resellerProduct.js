/**
 * This script is used to get product list from Alibaba
 * To run:
 * node module/scraper/website/alibaba/product.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var Promise = require('bluebird');
var moment = require('moment');
var request = require('../../../../helpers/common/request');
var phantom = require('../../../../helpers/common/phantom');
var url = require('url');
var util = require('util');

var downloader = require('../../../../helpers/file/downloader');

var homeUrl = 'http://reseller.alibaba.com/Products';
var productSubCategoryUrl = 'https://www.alibaba.com/catalogs/products/CID%s----------------------------G--------------------------%s/';
var protocol = 'http';
var sourceWebsite = 'alibaba';
var imageDirectory = '~/images/alibaba/products/';

const MAX_CONCURRENCY = 1;
const MAX_ATTEMPT = 5;

phantom.makeRequest(homeUrl, MAX_ATTEMPT)
  .then(function (html) {
    return getSubCategories(html);
  })
  .then(function (subCategories) {
    return getAllProducts(subCategories);
  })
  .then(function (products) {
    return getAllProductsDetails(products);
  });;

/**
 * @typedef Category
 * @property {String} name - Name of the category
 * @property {string} url - URL of the category
 */

/**
 * @typedef SubCategory
 * @property {String} name - Name of the subcategory
 * @property {String} category - The catgeory (parent)
 * @property {string} url - URL of the subcategory
 */

/**
 * This function is used to get a list of subcategories given the HTML of the page
 * @param {Strinng} html
 * @ {Array.<SubCategory>}
 */
function getSubCategories(html) {
  var allSubCategories = [];
  var $ = cheerio.load(html);

  $('.sub-item').each(function () {
    // Get category name
    var category = $(this).children('.sub-title').text();
    var lastIndexOfOpenBracket = category.lastIndexOf('(');
    if (lastIndexOfOpenBracket !== -1) {
      category = _.trim(category.substr(0, lastIndexOfOpenBracket));
    }

    var $subCategories = $(this).children('.sub-item-cont-wrapper').children('ul').children('li').children('a');
    var subCategories =  _.reduce($subCategories, function (tempSubCategories, current) {
      var subCategory = {};
      var url = current.attribs.href.replace('http://www.alibaba.com/', '');
      var indexOfPid = url.lastIndexOf('_pid');
      var subCategoryUrlName = url.substr(0, indexOfPid);
      var subCategoryUrlId = url.substr(indexOfPid, url.length).replace('_pid', '');
      subCategory.url = util.format(productSubCategoryUrl, subCategoryUrlId, subCategoryUrlName);
      subCategory.category = category;
      subCategory.name = _.trim(current.children[0].data);
      tempSubCategories.push(subCategory);
      return tempSubCategories;
    }, []);
    allSubCategories.push(subCategories);
  });
  return _.flattenDeep(allSubCategories);

}

/**
 * This function is used to get a list of all products from each sub categories
 * @param {Array<SubCategory>} subCategories
 * @returns {Promise<Array.<Product>>}
 */
function getAllProducts(subCategories) {
  return Promise.map(subCategories, function (subCategory) {
    return getProductsOnASubCategory(subCategory);
  }, { concurrency: MAX_CONCURRENCY })
    .then(function (res) {
      var flatted = _.flatten(res);
      return flatted;
    });
}

/**
 * This function is used to get a list of all products on a subcategory
 * @param {SubCategory} subCategory
 * @returns {Promise<Array.<Product>>>}
 */
function getProductsOnASubCategory(subCategory) {
  var allProductsList = [];
  var page = 1;

  // Format URL
  var subCategoryUrl = subCategory.url;
  var parsedSubCategoryUrl = url.parse(subCategoryUrl);
  if (parsedSubCategoryUrl.protocol === null) {
    parsedSubCategoryUrl.protocol = protocol;
  }
  subCategoryUrl = url.format(parsedSubCategoryUrl);

  var getProductsList = function () {
    var pageUrl = subCategoryUrl + '/' + page;

    // Perform HTTP request
    return phantom.makeRequest(pageUrl, MAX_ATTEMPT)
      .then(function (html) {
        if (!html) {
          return Promise.reject(null);
        } else {
          var $ = cheerio.load(html);
          var productList = [];

          $('.item-info').each(function () {
            var product = {};
            product.name = _.trim($(this).children('.title').text());
            product.sourceWebsite = sourceWebsite;
            product.category = subCategory.category;
            product.subCategory = subCategory.name;
            product.price = _.trim($(this).children('.pmo')
              .children('.price').text());
            product.minOrder = parseInt($(this).children('.pmo')
              .children('.min-order').children('b').text().replace('Piece', ''));

            var sourceUrl = _.trim($(this).children('.title').children('a').attr('href'));
            if (sourceUrl) {
              var parsedSourceUrl = url.parse(sourceUrl);
              if (parsedSourceUrl.protocol === null) {
                parsedSourceUrl.protocol = protocol;
              }
              product.sourceUrl = url.format(parsedSourceUrl);
            }

            if (product.name && product.sourceUrl) {
              productList.push(product);
            }
          });

          if (productList.length === 0 | page > 1) { // Terminate if there is no product on the last page
            return allProductsList;
          } else { // Continue if there are any products on the last page
            allProductsList = _.concat(allProductsList, productList);
            page++;
            return getProductsList();
          }
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  return getProductsList();
}

/**
 * This function is used to get details of all products
 * @param {Array.<Product>} products
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProductsDetails(products) {
  return Promise.map(products, function (product) {
    return getProductDetails(product);
  }, { concurrency: MAX_CONCURRENCY }).then(function (res) {
    return res;
  });
}

/**
 * This function is used to get details of a product from each products' page
 * which include description, phone, and email
 * @param {Product} product
 * @returns {Promise<Product>}
 */
function getProductDetails(product) {
  console.log('Get details of ' + product.name);

  // Format the URL
  var sourceUrl = product.sourceUrl;
  var parsedSourceUrl = url.parse(sourceUrl);
  if (parsedSourceUrl.protocol === null) {
    parsedSourceUrl.protocol = protocol;
  }
  sourceUrl = url.format(parsedSourceUrl);

  // Perform HTTP request
  return phantom.makeRequest(sourceUrl, MAX_ATTEMPT)
    .then(function (html) {
      if (!html) {
        return product;
      }

      var $ = cheerio.load(html);

      // Get image urls
      var imageUrls = [];
      $('.thumb img').each(function () {
        imageUrls.push($(this).attr('src').replace('.jpg_50x50', ''));
      });

      // Get product description
      product.description = _.trim($('#J-product-detail').text());

    })
    .catch(function (err) {
      console.log(err);
    });
}

/**
 * This function is used to get images of the product
 * and store the images on local computer
 * It returns path of the images
 *
 * @params {Array<String>}
 * @returns {Promise<Array<String>>}
 */
function getImages(imageUrls) {

  var downloadImage = function (imageUrl) {
    var paths = url.parse(imageUrl).pathname.split('/');
    var fileName = paths[paths.length - 1];
    var destination = imageDirectory.replace('~', process.env.HOME) + fileName;

    // Download the images
    return downloader.download(imageUrl, destination)
      .then(function (res) {
        if (res === true) {
          return imageDirectory + fileName;
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  return Promise.map(imageUrls, function (imageUrl) {
    return downloadImage(imageUrl);
  }, { concurrency: 1 })
    .then(function (res) {
      var flatted = _.flatten(res);
      return flatted;
    });

}

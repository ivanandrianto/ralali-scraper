/**
 * This script is used to get product list from Mitutoyo
 * To run:
 * node modules/scraper/runner/medicalogy/product.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var Promise = require('bluebird');
var url = require('url');
var util = require('util');

var downloader = require('../../../../helpers/file/downloader');
var lastScrapeQuery = require('../../queries/lastScrape');
var request = require('../../../../helpers/common/request');
var Product = require('../../models/product');
var productQueryHelper = require('../../queries/product');

var homeUrl = 'http://www.alatkesehatan.id/';
var productHomeUrl = 'http://www.alatkesehatan.id/toko/';
var host = 'www.alatkesehatan.id';
var imageDirectory = '~/images/alatkesehatan/products/';
var protocol = 'http';
var sourceWebsite = 'alatkesehatan';

const MAX_CONCURRENCY = 1;
const MAX_ATTEMPT = 3;
const REQUEST_TIMEOUT = 60000;

var lastScrape = null;
var currentSubCategory = null;
var currentPage = 0;

exports.run = function () {
  lastScrapeQuery.getLastScrape(sourceWebsite, 'product')
    .then(function (lastScrapeContext) {
      if (lastScrapeContext) {
        lastScrape = lastScrapeContext;
      }
      return getAllSubSubCategories();
    })
    .then(function (subCategories) {
      if (lastScrape) {
        var indexOfSubCategory = _.findIndex(subCategories, { 'url': lastScrape.subCategory });
        if (lastScrape && indexOfSubCategory !== -1) {
          subCategories = _.drop(subCategories, indexOfSubCategory);
        }
      }
      return getAllProducts(subCategories);
    })
    .catch(function (err) {
      console.log(err);
    })
    .then(function () {
      process.exit();
    });
};

/**
 * @typedef SubSubCategory
 * @property {String} name - Name of the sub sub category
 * @property {String} url - Url of the sub sub category
 */

/**
 * This function is used to get a list of all subcategories
 *
 * @returns {Promise<Array.<SubSubCategory>>}
 */

function getAllSubSubCategories() {
  var allSubSubCategoryList = [];

  var requestSubCategoryListOptions = {
    url: homeUrl,
    method: 'GET',
    timeout: REQUEST_TIMEOUT
  };

  return request.makeRequest(requestSubCategoryListOptions, MAX_ATTEMPT)
    .then(function (html) {
      var body = html.body;
      if (!body) {
        return Promise.reject(null);
      } else {
        var $ = cheerio.load(body);

        // $('.menu-item').each(function () {
        //   var category = _.trim($(this).children('a').text());
        //   $(this).children('ul').children('li').children('a').each(function () {
        //     var name = _.trim($(this).text());
        //     var url = $(this).attr('href');
        //     var subCategory = { name: name, category: category, url: url };
        //     allSubCategoryList.push(subCategory);
        //   });
        // });

        $('.ubermenu-target').each(function () {
          var url = $(this).attr('href');
          if (url && url.match(new RegExp('kategori-produk'))) {
            var name = _.trim($(this).text());
            var subSubCategory = {
              name: name,
              url: url
            };
            allSubSubCategoryList.push(subSubCategory);
          }
        });

        return allSubSubCategoryList;
      }
    });
}

/**
 * @typedef Product
 * @property {String} name - Name of the product
 * @property {String} brand - Brand of the product
 * @property {String} description - Description about the product
 * @property {String} sourceUrl - URL to the details page
 * @property {string} sourceWebsite - In this case, the source website is indonetwork
 * @property {String} category - Category of the product
 * @property {String} subCategory - Sub category of the product
 * @property {String} subCubCategory - Sub sub category of the product
 * @property {Number} specialPrice - Special price of the product
 * @property {Number} oldPrice - Old price of the product
 * @property {String} images - path to the product images
 */

/**
 * This function is used to get a list of all products
 *
 * @param {Array<SubSubCategory>}
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProducts(subSubCategories) {
  return Promise.each(subSubCategories, function (subSubCategory) {
    return getProductsOfASubCategory(subSubCategory);
  }, { concurrency: MAX_CONCURRENCY })
    .then(function (res) {
      return _.flatten(res);
    });
}

/**
 * This function is used to get a list of all products of a sub sub category
 *
 * @param {SubSubCategory}
 * @returns {Promise<Array.<Product>}>}
 */
function getProductsOfASubCategory(subCategory) {
  var allProductsList = [];
  var page = 1;

  if (lastScrape && (subCategory.url === lastScrape.subCategory)) {
    page = lastScrape.page;
  }

  var getProductList = function () {
    var pageUrl = util.format(
      subCategory.url + 'page/%s/',
      page
    );

    var requestProductListOptions = {
      url: pageUrl,
      method: 'GET',
      timeout: REQUEST_TIMEOUT
    };

    return request.makeRequest(requestProductListOptions, MAX_ATTEMPT)
      .then(function (html) {
        var body = html.body;
        if (!body) {
          return Promise.reject(null);
        } else {
          var $ = cheerio.load(body);

          var products = [];
          $('.product-thumbnail').each(function () {
            var sourceUrl =  $(this).children().children().children().attr('href');
            var name =  _.trim($(this).children().first().children().first().next().children().first().text());

            if (sourceUrl) {
              var product = {
                name: name,
                sourceUrl: sourceUrl
              };
              console.log(product);

              products.push(product);
            }
          });

          return getAllProductsDetails(products)
            .then(function () {
              allProductsList = _.concat(allProductsList, products);
              if (products.length > 0) {
                page++;
                currentPage = page;
                currentSubCategory = subCategory.url;
                return getProductList();
              } else {
                return allProductsList;
              }
            });
        }
      });
  };

  return getProductList();
}

/**
 * This function is used to get details of all products
 * @param {Array.<Product>} products
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProductsDetails(products) {
  return Promise.each(products, function (product) {
    return getProductDetails(product);
  }, { concurrency: MAX_CONCURRENCY }).then(function (res) {
    return res;
  });
}

/**
 * This function is used to get details of a product from each products' page
 * @param {Product} product
 * @returns {Promise<Product>}
 */
function getProductDetails(product) {
  return productQueryHelper.findProducts({ sourceWebsite: sourceWebsite, sourceUrl: product.sourceUrl })
    .then(function (productsInDb) {
      if (productsInDb.length > 0) {
        return Promise.resolve();
      }

      var requestOptions = {
        url: product.sourceUrl,
        method: 'GET',
        timeout: REQUEST_TIMEOUT
      };

      return request.makeRequest(requestOptions, MAX_ATTEMPT)
        .then(function (html) {
          var body = html.body;
          if (!body) {
            return Promise.reject(null);
          } else {
            var $ = cheerio.load(body);
            $('.woocommerce-Price-currencySymbol').remove();
            $('h2').remove();
            $('h3').remove();

            var price = _.trim($('.entry-summary .woocommerce-Price-amount').first().text().replace(/\D+/g, ''));
            var shortDescription = _.trim($('.entry-summary .product-description').first().text());
            var description = _.trim($('#tab-description').text());

            $('.shop_attributes tr').each(function () {
              var key = _.trim($(this).children('th').text());
              var value = _.trim($(this).children('td').text());
              description += '\n' + key + ': ';
              description += value;
            });

            var possibleCategories = [];
            $('.woocommerce-breadcrumb a').each(function () {
              possibleCategories.push(_.trim($(this).text()));
            });

            if (possibleCategories.length > 4) {
              product.category = possibleCategories[2];
              product.subCategory = possibleCategories[3];
              product.subSubCategory = possibleCategories[4];
            } else if (possibleCategories.length === 4) {
              product.category = possibleCategories[2];
              product.subCategory = possibleCategories[3];
            }

            product.price = price;
            product.shortDescription = shortDescription;
            product.description = description;
            product.sourceWebsite = sourceWebsite;

            var imageUrls = [];
            $('.images a').each(function () {
              var imageUrl = $(this).attr('href');
              imageUrls.push(imageUrl);
            });

            return getImages(imageUrls)
              .then(function (imagePaths) {
                product.images = imagePaths;
              })
              .then(function () {
                var productMongooseObj = new Product(product);
                return productQueryHelper.saveProduct(productMongooseObj)
                  .then(function () {
                    return lastScrapeQuery.setLastScrape(sourceWebsite, 'product',
                      currentSubCategory, currentPage, product.sourceUrl);
                  });
              });
          }
        });
    });
}

/**
 * This function is used to get images of the product
 * and store the images on local computer
 * It returns path of the images
 *
 * @params {Array<String>}
 * @returns {Promise<Array<String>>}
 */
function getImages(imageUrls) {

  var downloadImage = function (imageUrl) {
    var paths = url.parse(imageUrl).pathname.split('/');
    var fileName = paths[paths.length - 1];
    var destination = imageDirectory.replace('~', process.env.HOME) + fileName;

    // Download the images
    return downloader.download(imageUrl, destination)
      .then(function (res) {
        if (res === true) {
          return fileName;
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  return Promise.map(imageUrls, function (imageUrl) {
    return downloadImage(imageUrl);
  }, { concurrency: 1 })
    .then(function (res) {
      var flatted = _.flatten(res);
      return flatted;
    });

}


/**
 * This script is used to get product list from Toyskingdom
 * To run:
 * node modules/scraper/runner/toyskingdom/product.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var Promise = require('bluebird');
var url = require('url');
var util = require('util');

var downloader = require('../../../../helpers/file/downloader');
var lastScrapeQuery = require('../../queries/lastScrape');
var request = require('../../../../helpers/common/request');
var Product = require('../../models/product');
var productQueryHelper = require('../../queries/product');

var homeUrl = 'http://www.toyskingdom.co.id/';
var host = 'www.toyskingdom.co.id';
var imageDirectory = '~/images/toyskingdom/products/';
var protocol = 'http';
var sourceWebsite = 'toyskingdom';

const MAX_CONCURRENCY = 1;
const MAX_ATTEMPT = 3;
const REQUEST_TIMEOUT = 60000;

var lastScrape = null;
var currentSubCategory = null;
var currentPage = 0;

exports.run = function () {
  lastScrapeQuery.getLastScrape(sourceWebsite, 'product')
    .then(function (lastScrapeContext) {
      if (lastScrapeContext) {
        lastScrape = lastScrapeContext;
      }
      return getAllSubCategories();
    })
    .then(function (subCategories) {
      if (lastScrape) {
        var indexOfSubCategory = _.findIndex(subCategories, { 'name': lastScrape.subCategory });
        if (lastScrape && subCategories !== -1) {
          subCategories = _.drop(subCategories, indexOfSubCategory);
        }
      }
      return getAllProducts(subCategories);
    })
    .catch(function (err) {
      console.log(err);
    })
    .then(function () {
      process.exit();
    });
};

/**
 * @typedef SubCategory
 * @property {String} category - Name of the category
 * @property {String} name - Name of the sub sub category
 * @property {String} url - Url of the sub sub category
 */

/**
 * This function is used to get a list of all subcategories
 *
 * @returns {Promise<Array.<SubCategory>>}
 */
function getAllSubCategories() {
  var allSubCategoryList = [];

  var requestSubCategoryListOptions = {
    url: homeUrl,
    method: 'GET',
    timeout: REQUEST_TIMEOUT
  };

  return request.makeRequest(requestSubCategoryListOptions, MAX_ATTEMPT)
    .then(function (html) {
      var body = html.body;
      if (!body) {
        return Promise.reject(null);
      } else {
        var $ = cheerio.load(body);

        $('.category-left .menu-content .level-1').each(function () {
          var category = _.trim($(this).children('a').text());

          $(this).children('.wt-sub-menu').children('.col-xs-12').children('.wt-menu-row').children('.wt-menu-col')
            .first().children('ul').children('li .item-line').children('a').each(function () {
            var name = _.trim($(this).text());
            var url = $(this).attr('href');

            if (name && url && (category !== 'Exclusive Brands')) {
              var subCategory = {
                category: category,
                name: name,
                url: url
              };
              allSubCategoryList.push(subCategory);
            }
          });
        });

        return allSubCategoryList;
      }
    });
};

/**
 * @typedef Product
 * @property {String} name - Name of the product
 * @property {String} brand - Brand of the product
 * @property {String} description - Description about the product
 * @property {String} sourceUrl - URL to the details page
 * @property {string} sourceWebsite - In this case, the source website is indonetwork
 * @property {String} category - Category of the product
 * @property {String} subCategory - Sub category of the product
 * @property {Number} price - Price of the product
 * @property {String} images - path to the product images
 */

/**
 * This function is used to get a list of all products
 *
 * @param {Array<SubSubCategory>}
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProducts(subCategories) {
  return Promise.each(subCategories, function (subCategory) {
    return getProductsOfASubCategory(subCategory);
  }, { concurrency: MAX_CONCURRENCY })
    .then(function (res) {
      return _.flatten(res);
    });
};

/**
 * This function is used to get a list of all products of a sub category
 *
 * @param {SubSubCategory}
 * @returns {Promise<Array.<Product>}>}
 */
function getProductsOfASubCategory(subCategory) {
  var allProductsList = [];
  var page = 1;

  if (lastScrape && (subCategory.url === lastScrape.subCategory)) {
    page = lastScrape.page;
  }
  currentPage = page;

  var pageUrl = util.format(
    subCategory.url + '?p=%s',
    page
  );

  var getProductList = function () {
    var requestProductListOptions = {
      url: pageUrl,
      method: 'GET',
      timeout: REQUEST_TIMEOUT
    };

    return request.makeRequest(requestProductListOptions, MAX_ATTEMPT)
      .then(function (html) {
        var body = html.body;
        if (!body) {
          return Promise.reject(null);
        } else {
          var $ = cheerio.load(body);

          var products = [];
          $('.product_list > li').each(function () {
            var sourceUrl =  $(this).children('.product-block').children('.product-container')
              .children('.right-block').children('h5').children('.product-name').attr('href');

            if (sourceUrl) {
              var product = {
                sourceUrl: sourceUrl,
                category: subCategory.category,
                subCategory: subCategory.name
              };
              products.push(product);
            }
          });

          return getAllProductsDetails(products)
            .then(function () {
              allProductsList = _.concat(allProductsList, products);

              var nextUrl = $('#pagination_next_bottom').children('a').attr('href');

              if (products.length > 0 && nextUrl) {
                page++;
                currentPage = page;
                currentSubCategory = subCategory.url;
                pageUrl = util.format(
                  subCategory.url + '?p=%s',
                  page
                );
                return getProductList();
              } else {
                return allProductsList;
              }
            });
        }
      });
  };

  return getProductList();
};

/**
 * This function is used to get details of all products
 * @param {Array.<Product>} products
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProductsDetails(products) {
  return Promise.each(products, function (product) {
    return getProductDetails(product);
  }, { concurrency: MAX_CONCURRENCY }).then(function (res) {
    return res;
  });
};

/**
 * This function is used to get details of a product from each products' page
 * @param {Product} product
 * @returns {Promise<Product>}
 */
function getProductDetails(product) {
  return productQueryHelper.findProducts({ sourceWebsite: sourceWebsite, sourceUrl: product.sourceUrl })
    .then(function (productsInDb) {
      if (productsInDb.length > 0) {
        return Promise.resolve();
      }

      var requestOptions = {
        url: product.sourceUrl,
        method: 'GET',
        timeout: REQUEST_TIMEOUT
      };

      return request.makeRequest(requestOptions, MAX_ATTEMPT)
        .then(function (html) {
          var body = html.body;
          if (!body) {
            return Promise.reject(null);
          } else {
            var $ = cheerio.load(body);

            product.name = _.trim($('.page-heading').text());
            product.price = _.trim($('#our_price_display').first().text().replace(/\D+/g, ''));
            product.oldPrice = _.trim($('#old_price_display').first().text().replace(/\D+/g, ''));
            product.priceReductionAmount = _.trim($('#reduction_amount_display').first().text().replace(/\D+/g, ''));
            product.priceReductionPercentage = _.trim($('#reduction_percent_display').first().text().replace(/\D+/g, ''));
            product.sourceWebsite = sourceWebsite;
            product.shortDescription = _.trim($('#short_description_content').text());
            var possibleLinkToBrand = $('.rte a').first().attr('href');
            $('.rte a').remove();
            product.description = '';
            $('.page-product-box .rte ul li').each(function () {
              product.description += _.trim($(this).text()) + '\n';
            });

            $('.table-data-sheet tr').each(function () {
              var key = _.trim($(this).children().first().text());
              var value = _.trim($(this).children().first().next().text());
              product.description += '\n' + key + ' ';
              product.description += value;
            });

            product.shortDescription = _.trim(product.shortDescription.replace(/\r/g, '').replace(/\t/g, '')
              .replace(/ {2,}/g, ' ').replace(/\n +/g, '\n').replace(/\n{2,}/g, '\n'));
            product.description = _.trim(product.description.replace(/\r/g, '').replace(/\t/g, '').replace(/ {2,}/g, ' ')
              .replace(/\n +/g, '\n').replace(/\n{2,}/g, '\n'));

            var imageUrls = [];
            $('#thumbs_list_frame li a').each(function () {
              var imageUrl = $(this).attr('href');
              if (imageUrl) {
                imageUrls.push(imageUrl);
              }
            });
            console.log(product);

            // Get Brand
            var brand;
            if (possibleLinkToBrand) {
              var parsedPossibleLinkToBrand = url.parse(possibleLinkToBrand, true);
              brand = parsedPossibleLinkToBrand.query.search_query;
            }

            return getBrand(product, brand, possibleLinkToBrand)
              .then(function () {
                return getImages(imageUrls)
                  .then(function (imagePaths) {
                    product.images = imagePaths;
                  })
                  .then(function () {
                    var productMongooseObj = new Product(product);
                    return productQueryHelper.saveProduct(productMongooseObj)
                      .then(function () {
                        return lastScrapeQuery.setLastScrape(sourceWebsite, 'product',
                          product.subCategory, currentPage, product.sourceUrl);
                      });
                  });
              })
              .catch(function (err) {
                console.log(err);
              });
          }
        });
    });
};

/**
 * This function is used to get brand
 * It update the product.brand property
 *
 * @params {Product} product
 * @params {String} brand
 * @params {String} parsedPossibleLinkToBrand
 * @returns {Promise>}
 */
function getBrand(product, brand, possibleLinkToBrand) {
  if (brand) {
    product.brand = brand;
    return Promise.resolve();
  }

  var requestOptions = {
    url: possibleLinkToBrand,
    method: 'GET',
    timeout: REQUEST_TIMEOUT
  };

  return request.makeRequest(requestOptions, MAX_ATTEMPT)
    .then(function (html) {
      var body = html.body;
      if (!body) {
        return Promise.reject(null);
      } else {
        var $ = cheerio.load(body);
        $('.breadcrumb a').remove();
        $('.breadcrumb .navigation-pipe').remove();
        var brand = _.trim($('.breadcrumb').text());
        product.brand = brand ? brand : undefined;
        return Promise.resolve();
      }
    });
}

/**
 * This function is used to get images of the product
 * and store the images on local computer
 * It returns path of the images
 *
 * @params {Array<String>}
 * @returns {Promise<Array<String>>}
 */
function getImages(imageUrls) {

  var downloadImage = function (imageUrl) {
    var paths = url.parse(imageUrl).pathname.split('/');
    var fileName = paths[paths.length - 1];
    var fileId = paths[paths.length - 2].split('-')[0];
    fileName = fileId + '_' + fileName;
    var destination = imageDirectory.replace('~', process.env.HOME) + fileName;

    // Download the images
    return downloader.download(imageUrl, destination)
      .then(function (res) {
        if (res === true) {
          return fileName;
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  return Promise.map(imageUrls, function (imageUrl) {
    return downloadImage(imageUrl);
  }, { concurrency: 1 })
    .then(function (res) {
      var flatted = _.flatten(res);
      return flatted;
    });

}

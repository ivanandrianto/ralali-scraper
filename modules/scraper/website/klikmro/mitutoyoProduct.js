/**
 * This script is used to get product list from Mitutoyo
 * To run:
 * node modules/scraper/runner/klikmro/mitutoyoProduct.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var Promise = require('bluebird');
var url = require('url');
var util = require('util');

var downloader = require('../../../../helpers/file/downloader');
var lastScrapeQuery = require('../../queries/lastScrape');
var request = require('../../../../helpers/common/request');
var Product = require('../../models/product');
var productQueryHelper = require('../../queries/product');

var homeUrl = 'https://www.klikmro.com/brands/mitutoyo.html';
var host = 'www.klikmro.com';
var imageDirectory = '~/images/klikmro/mitutoyo/products/';
var protocol = 'https';
var productListUrl = 'https://www.klikmro.com/brands/mitutoyo.html?limit=360&p=%s';
var sourceWebsite = 'klikmro';

const MAX_CONCURRENCY = 3;
const MAX_ATTEMPT = 3;
const REQUEST_TIMEOUT = 60000;

var lastScrape = null;
var currentPage = 0;

exports.run = function () {
  lastScrapeQuery.getLastScrape(sourceWebsite, 'mitutoyoProduct')
    .then(function (lastScrapeContext) {
      if (lastScrapeContext) {
        lastScrape = lastScrapeContext;
      }
      return getAllProducts();
    })
    .then(getProductDetails)
    .catch(function (err) {
      console.log(err);
    })
    .then(function () {
      process.exit();
    });
};

/**
 * This function is used to get a list of all products
 *
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProducts() {
  var allProductList = [];
  var page = 1;

  if (lastScrape) {
    page = lastScrape.page;
  }

  var url = util.format(
    productListUrl,
    page
  );

  var getProductList = function () {
    var requestOptions = {
      url: url,
      method: 'GET',
      timeout: REQUEST_TIMEOUT
    };

    // Perform HTTP request
    return request.makeRequest(requestOptions, MAX_ATTEMPT)
      .then(function (html) {
        var body = html.body;
        if (!body) {
          return Promise.reject(null);
        } else {
          var $ = cheerio.load(body);
          var count = 0;
          var products = [];
          $('.item > a').each(function () {
            var sourceUrl = $(this).attr('href');
            var product = { sourceUrl: sourceUrl };
            products.push(product);
          });

          return getProductsDetails(products)
            .then(function () {
              var nextPage = $('.i-next').attr('href');
              if (nextPage) {
                page++;
                currentPage = page;
                url = nextPage;
                return getProductList();
              }
            });
        }
      });

  };

  return getProductList();
}

/**
 * This function is used to get details of all products
 * @param {Array.<Product>} products
 * @returns {Promise<Array.<Product>}>}
 */
function getProductsDetails(products) {
  return Promise.map(products, function (product) {
    return getProductDetails(product);
  }, { concurrency: MAX_CONCURRENCY }).then(function (res) {
    return res;
  });
}

/**
 * This function is used to get details of a product from each products' page
 *
 * @param {Product} product
 * @returns {Promise<Product>}
 */
function getProductDetails(product) {
  return productQueryHelper.findProducts({ sourceWebsite: sourceWebsite, sourceUrl: product.sourceUrl })
    .then(function (productsInDb) {
      if (productsInDb.length > 0) {
        return Promise.resolve();
      }

      var requestOptions = {
        url: product.sourceUrl,
        method: 'GET',
        timeout: REQUEST_TIMEOUT
      };

      return request.makeRequest(requestOptions, MAX_ATTEMPT)
        .then(function (html) {
          var body = html.body;
          if (!body) {
            return Promise.reject(null);
          } else {
            var $ = cheerio.load(body);
            $('script').remove();
            $('#review-form').remove();
            $('.no-rating').remove();
            $('.form-add').remove();
            $('.product-desc h2').remove();

            product.brand = _.trim($('.product-essential .brand').first().text());
            product.name = _.trim($('.product-essential .product-name').first().text());
            product.oldPrice = _.trim($('.product-essential .old-price .price').first().text().replace(/\D+/g, ''));
            product.specialPrice = _.trim($('.product-essential .special-price .price').first().text().replace(/\D+/g, ''));
            product.sourceWebsite = sourceWebsite;

            product.description = _.trim($('.product-desc').text());

            $('#product-attribute-specs-table tr').each(function () {
              var key = _.trim($(this).children('th').text());
              var value = _.trim($(this).children('td').text());
              product.description += '\n' + key + ': ';
              product.description += value;

              if (key.toLowerCase() === 'type') {
                product.category = value;
              }

            });
            product.description = _.trim(product.description.replace(/\r/g, '').replace(/\t/g, '').replace(/ {2,}/g, ' ')
              .replace(/\n +/g, '\n').replace(/\n{2,}/g, '\n'));

            product.imageUrl = $('#image-main').attr('src');
            console.log(product);

            return getImage(product.imageUrl)
              .then(function (imgUrl) {
                product.images = [];
                product.images.push(imgUrl);
              })
              .then(function () {
                var productMongooseObj = new Product(product);
                return productQueryHelper.saveProduct(productMongooseObj)
                  .then(function () {
                    return lastScrapeQuery.setLastScrape(sourceWebsite, 'mitutoyoProduct',
                      null, currentPage, product.sourceUrl);
                  });
              })
              .catch(function (err) {
                console.log(err);
              });
          }
        });
    });
}

/**
 * This function is used to download image
 * It returns the local file name
 *
 * @params {String}
 * @returns {Promise<String>}
 */
function getImage(imageUrl) {
  var paths = url.parse(imageUrl).pathname.split('/');
  var fileName = paths[paths.length - 1];
  var destination = imageDirectory.replace('~', process.env.HOME) + fileName;

  // Download the images
  return downloader.download(imageUrl, destination)
    .then(function (res) {
      if (res === true) {
        return fileName;
      }
    })
    .catch(function (err) {
      console.log(err);
      return null;
    });
}

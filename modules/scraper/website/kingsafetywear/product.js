/**
 * This script is used to get product list from Kingsafetywear
 * To run:
 * node modules/scraper/runner/kingsafetywear/product.js
 * @author: ivan
 */

'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var Promise = require('bluebird');
var url = require('url');
var util = require('util');

var downloader = require('../../../../helpers/file/downloader');
var request = require('../../../../helpers/common/request');
var Product = require('../../models/product');
var productQueryHelper = require('../../queries/product');

var homeUrl = 'http://www.kingsafetywear.com/index.html';
var host = 'www.kingsafetywear.com';
var imageDirectory = '~/images/kingsafetywear/products/';
var protocol = 'http';
var sourceWebsite = 'kingsafetywear';

const MAX_CONCURRENCY = 3;
const MAX_ATTEMPT = 3;
const REQUEST_TIMEOUT = 60000;

exports.run = function () {
  return getAllBrandList()
    .then(function (brands) {
      return getAllProducts(brands);
    })
    .then(function (products) {
      console.log('Length: ' + products.length);
      return getAllProductsDetails(products);
    });
};

/**
 * @typedef Brand
 * @property {String} name -Name of the brand
 * @property {String} url - Url of the brand
 */

/**
 * This function is used to get a list of all brands
 *
 * @returns {Promise<Array.<Brand>}>}
 */
function getAllBrandList() {
  var allBrandsList = [];

  var requestAllBrandOptions = {
    url: homeUrl,
    method: 'GET',
    timeout: REQUEST_TIMEOUT
  };

  // Perform HTTP request
  return request.makeRequest(requestAllBrandOptions, MAX_ATTEMPT)
    .then(function (html) {
      var body = html.body;
      if (!body) {
        return Promise.reject(null);
      } else {
        var $ = cheerio.load(body);

        $('.box_image a').each(function () {
          var urlToBrandPage = $(this).attr('href');
          var parsedUrl = url.parse(urlToBrandPage);
          if (parsedUrl.host === null) {
            parsedUrl.host = host;
            parsedUrl.protocol = protocol;
            var brand = { url: url.format(parsedUrl) };
            allBrandsList.push(brand);
          }
        });
        return allBrandsList;
      }
    });
}

/**
 * This function is used to get a list of all products
 *
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProducts(brands) {
  return Promise.map(brands, function (brand) {
    return getProductsOfABrand(brand);
  }, { concurrency: MAX_CONCURRENCY })
    .then(function (res) {
      return _.flatten(res);
    });
}

/**
 * @typedef Product
 * @property {String} name - Name of the product
 * @property {String} brand - Brand of the product
 * @property {String} description - Description about the product
 * @property {String} sourceUrl - URL to the details page
 * @property {string} sourceWebsite - In this case, the source website is indonetwork
 * @property {String} category - Category of the product
 * @property {String} subCategory - Sub category of the product
 * @property {String} images - path to the product images
 */

/**
 * This function is used to get a list of all products of a brand
 *
 * @returns {Promise<Array.<Product>}>}
 */
function getProductsOfABrand(brand) {
  var allProductsList = [];

  var requestProductListOptions = {
    url: brand.url,
    method: 'GET',
    timeout: REQUEST_TIMEOUT
  };

  return request.makeRequest(requestProductListOptions, MAX_ATTEMPT)
    .then(function (html) {
      var body = html.body;
      if (!body) {
        return Promise.reject(null);
      } else {
        var $ = cheerio.load(body);

        brand.name = $('.subnavhead').text().replace(/\W/g, '').toLowerCase();
        $('.productList').each(function () {

          var $productElmt = $(this).children();
          $productElmt.each(function () {
            var product = {};

            var productUrl = $(this).attr('href');
            if (productUrl) {
              productUrl = 'brands/' + brand.name + '/' + productUrl;
              var parsedUrl = url.parse(productUrl);
              if (parsedUrl.host === null) {
                parsedUrl.host = host;
                parsedUrl.protocol = protocol;
                productUrl = url.format(parsedUrl);
              }
              product.sourceUrl = productUrl;
              product.brand = brand.name;
              allProductsList.push(product);
            }
          });
        });
        return Promise.resolve(allProductsList);
      }
    });
}

/**
 * This function is used to get details of all products
 * @param {Array.<Product>} products
 * @returns {Promise<Array.<Product>}>}
 */
function getAllProductsDetails(products) {
  return Promise.map(products, function (product) {
    return getProductDetails(product);
  }, { concurrency: MAX_CONCURRENCY }).then(function (res) {
    return res;
  });
}

/**
 * This function is used to get details of a product from each products' page
 * @param {Product} product
 * @returns {Promise<Product>}
 */
function getProductDetails(product) {
  var requestProductDetailsOptions = {
    url: product.sourceUrl,
    method: 'GET',
    timeout: REQUEST_TIMEOUT
  };

  return request.makeRequest(requestProductDetailsOptions, MAX_ATTEMPT)
    .then(function (html) {
      var body = html.body;
      if (!body) {
        return Promise.reject(null);
      } else {
        var $ = cheerio.load(body);
        $('#viewsize').remove();

        var name = $('h2').text();
        $('h2').remove();
        var description = _.trim($('.productInfo').text().replace(/\r/g, '').replace(/\t/g, '').replace(/ {2,}/g, ' ')
          .replace(/\n +/g, '\n').replace(/\n{2,}/g, '\n'));
        description = description.replace(/sole construction/i, '\nSOLE CONSTRUCTION');
        description = description.replace(/upper construction/i, '\nUPPER CONSTRUCTION');

        product.name = name;
        product.description = description;
        product.sourceWebsite = sourceWebsite;

        var possibleCategories = [];
        $('.breadcrumb a').each(function () {
          possibleCategories.push($(this).text());
        });

        if (possibleCategories.length >= 3) {
          product.category = possibleCategories[possibleCategories.length - 2];
          product.subCategory = possibleCategories[possibleCategories.length - 1];
        }

        var imgUrl = $('#ex1 img').attr('src').replace(/^..\/../i, protocol + '://' + host + '/brands');
        return getImage(imgUrl)
          .then(function (imgUrl) {
            product.images = [];
            product.images.push(imgUrl);
          })
          .then(function () {
            var productMongooseObj = new Product(product);
            return productQueryHelper.saveProduct(productMongooseObj);
          })
          .catch(function (err) {
            console.log(err);
          });
      }
    });
}

/**
 * This function is used to download image
 * It returns the local file name
 *
 * @params {String}
 * @returns {Promise<String>}
 */
function getImage(imageUrl) {
  var paths = url.parse(imageUrl).pathname.split('/');
  var fileName = paths[paths.length - 1];
  var destination = imageDirectory.replace('~', process.env.HOME) + fileName;

  // Download the images
  return downloader.download(imageUrl, destination)
    .then(function (res) {
      if (res === true) {
        return fileName;
      }
    })
    .catch(function (err) {
      console.log(err);
      return null;
    });
}

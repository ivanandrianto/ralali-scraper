var _ = require('lodash');
var Browser = require('zombie');

exports.makeRequest = function (url, maxAttempt) {
  console.log('Requesting ' + url);

  var i = 0;

  var getHtml = function () {
    var browser = new Browser();
    return new Promise(function (resolve) {
      browser.visit(url, function () {
        browser.wait(function () {
          var html = _.trim(browser.html());
          clearBrowser(browser);
          i++;
          if ((!html || html.length === 0) && i < maxAttempt) {
            resolve(getHtml());
          } else {
            resolve(html);
          }
        });
      });
    });
  };

  return getHtml();
};

function clearBrowser(browser) {
  delete browser.cookies;
  browser.window.close();
}

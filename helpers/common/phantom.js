var phantom = require('phantom');

exports.makeRequest = function (url, maxAttempt) {
  console.log('Requesting ' + url);
  var sitepage = null;
  var phInstance = null;
  var attempt = 0;

  var getResponse = function () {
    return new Promise(function (resolve, reject) {
      phantom.create()
        .then(instance => {
          attempt++;
          console.log('attempt' + attempt);
          phInstance = instance;
          return instance.createPage();
        })
        .then(page => {
          sitepage = page;
          return page.open(url);
        })
        .then(status => {
          return sitepage.property('content');
        })
        .then(content => {
          sitepage.close();
          phInstance.exit();
          resolve(content);
        })
        .catch(error => {
          reject(new Error('Unable to load' + url));
          phInstance.exit();
	  if (attempt < maxAttempt) {
            return getResponse();
          }
       });
    });
  };

  return getResponse();

};

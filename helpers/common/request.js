var _ = require('lodash');
var Promise = require('bluebird');
var requestPromise = Promise.promisify(require('request'));

exports.makeRequest = function (options, maxAttempt) {
  console.log('Requesting ' + options.url);

  var attempt = 0;

  var getResponse = function () {
    return requestPromise(options)
      .then(function (res) {
        attempt++;
        if (!res && attempt < maxAttempt) {
          return getResponse();
        } else {
          return res;
        }
      });
  };

  return getResponse();
};

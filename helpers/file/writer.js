var fs = require('fs');
var Promise = require('bluebird');
var writeFilePromise = Promise.promisify(fs.writeFile);

exports.write = function (filePath, content) {
  return writeFilePromise(filePath, content)
    .catch(function (err) {
      console.log(err);
    });
};

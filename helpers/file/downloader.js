/**
 * This helper is used to download file from a url
 * and save it to a destination in local computer
 *
 * @author: ivan
 */

var Promise = require('bluebird');
var request = Promise.promisifyAll(require('superagent'));
var fs = require('fs');

function binaryParser(response, callback) {
  response.setEncoding('binary');
  response.data = '';
  response.on('data', function (chunk) {
    response.data += chunk;
  });
  response.on('end', function () {
    callback(null, new Buffer(response.data, 'binary'));
  });
}

exports.download = function (url, destination) {
  if (!url || !destination) {
    return false;
  }
  return request
    .get(url)
    .buffer()
    .parse(binaryParser)
    .endAsync()
    .then(function (response) {
      fs.writeFileSync(destination, response.body);
      return true;
    })
    .catch(function (error) {
      console.log(error);
      return false;
    });
};
